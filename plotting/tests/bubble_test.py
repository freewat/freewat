import os
import unittest

from plotting import BubblePlot


class TestBubbleMethods(unittest.TestCase):
    path = "data" + os.path.sep + "t_syn_out"
    pathHob = path + '.hob'
    pathR = path + '._r'
    pathW = path + '._w'
    pathSO = path + '._so'

    def test_merge_csv(self):
        v = BubblePlot.VectorLayer(self.path)
        g = v.merge_csv(self.pathHob, self.pathR, self.pathW, self.pathSO, 'mean')
        #print(g)


if __name__ == '__main__':
    unittest.main()