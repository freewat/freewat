#-------------------------------------------------------------------------------
# Name:
# Purpose:
#
# Author:      iacopo.borsi
#
# Created:     28/01/2016
# Copyright:   (c) Iacopo Borsi (iacopo.borsi@tea-group.com) 2016
# Licence:     GNU General Public License
#-------------------------------------------------------------------------------

import matplotlib.pyplot as plt
import flopy
from flopy.modflow import *
from flopy.seawat import *
import sys
# add path
mylistfile = 'C:\\Users\\iacopo.borsi\\ex_iah\\working_folder\\iah.list'
namefile = 'C:\\Users\\iacopo.borsi\\ex_iah\\working_folder\\iah.nam'
disfile = 'C:\\Users\\iacopo.borsi\\ex_iah\\working_folder\\iah.dis'

# 'C:\Users\iacopo.borsi\ex_t0\\t0.lst'
mysecondfile = 'C:\Users\iacopo.borsi\ex_mt\\pr3.list'
secondname = mysecondfile = 'C:\\Users\\iacopo.borsi\\ex_mt\\pr3.nam'


ml = Modflow.load(namefile, version='mf2005', model_ws='C:\\Users\\iacopo.borsi\\ex_iah\\working_folder')

#msw = Seawat.load(namefile, version='mf2005', model_ws='C:\\Users\\iacopo.borsi\\ex_mt')

dis = ModflowDis.load(disfile, ml)
nper = dis.nper
nstp = dis.nstp




mf_list = flopy.utils.MfListBudget(mylistfile)

#sw_list = flopy.utils.SwtListBudget(mysecondfile)
# Select stress period and time step
listaKstpKper = mf_list.get_kstpkper()

# Select desired kper and kstp
iper = 2
kstpkper = listaKstpKper[iper - 1]

#mf_ucn = flopy.utils.SwtListBudget(mysecondfile)

incremental, cumulative = mf_list.get_budget()

data = mf_list.get_data(kstpkper= kstpkper)

plt.bar(data['index'], data['value'])
plt.xticks(data['index'], data['name'], rotation=45, size=6)
plt.ylabel('Water Volume')
plt.grid()

##plt.plot(incremental[:]['totim'], incremental[:]['STORAGE_IN'] )
##plt.plot(incremental[:]['totim'], -1*incremental[:]['STORAGE_OUT'])
##plt.xticks(incremental[:]['totim'], incremental[:]['totim'])

plt.show()
