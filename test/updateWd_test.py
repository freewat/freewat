# -*- coding: utf-8 -*-

import sys
import os
from tempfile import gettempdir
from qgis.core import *
from qgis.gui import *
from PyQt4.QtGui import QDialogButtonBox
from ..logger import Logger
from ..dialogs.updateWd import UpdateWorkingDirectory
from ..dialogs.createModel_dialog import CreateModelDialog

Logger.TESTING = True

app = QgsApplication(sys.argv, True)
QgsApplication.initQgis()

try:
    dlg = UpdateWorkingDirectory(iface=None)
    assert(False) # should trigger an exception because model is missiing
except RuntimeError as e:
    assert(str(e).find('create a MODEL before') != -1)

dbfile = os.path.join(gettempdir(), 'mymodel.sqlite')
if os.path.exists(dbfile):
    os.remove(dbfile)
dlg = CreateModelDialog(iface=None)
dlg.bxDBname.setText('mymodel')
dlg.txtDirectory.setText(gettempdir())
dlg.srText.setText('EPSG:2154')
dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)
app.processEvents()

dlg = UpdateWorkingDirectory(iface=None)
dlg.workingDirPath.setText(gettempdir())

if len(sys.argv) > 1:
    dlg.show()
    app.exec_()
else:
    dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)
    app.processEvents()
