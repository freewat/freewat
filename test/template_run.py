# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QMenu


# Initialize Qt resources from file resources.py
import sys, os
# add path
sys.path.append(os.path.expanduser('~/.qgis2/python/plugins/freewat'))

# Import the code for the dialog
import createEVTLayer_dialog as cDialog

# Input parameters
#qd = QDialog()
dlg = cDialog.CreateEVTLayerDialog(iface)


# show the dialog
dlg.show()
# Run the dialog event loop
dlg.exec_()
