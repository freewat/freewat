import unittest
from modpath import inputCreator


class TestModelFilesCreation(unittest.TestCase):
    testParams = {
        'SimulationType': 'Pathlines',
        'ReferenceTime': '6666',
        'TrackingDirection': 'Forward',
        'OutputInterval': '',
        'Retardation': 'True',
        'ParticelSource': '',
        'ReleaseTime': '',
        'SelectedFeatures': ''
    }

    def test_CreatInputFiles(self):
        t = inputCreator.InputCreator('testmodel', 'testModel', self.testParams)
        self.assertNotEqual(t, None)

if __name__ == '__main__':
    unittest.main()
