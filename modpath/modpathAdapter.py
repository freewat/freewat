import inputCreator as inputFiles
from exeRunner import ExeRunner as runner

'''
Responsible for providing an interface to pass through selected options to modpath files and run the model
'''


class ModpathAdapter:

    def __init__(self, modelPath, modelName, exePath, param_dict):
        '''
        Parameters
        ----------
        modelPath
        modelName
        exePath:  path to the modpath executable
        param_dict: parameters selected in the gui which ought to be passed to modpath

        Returns
        -------
        '''

        self.modpathEXE = exePath
        self.inputFiles = inputFiles.InputCreator(modelPath, modelName)

    def run(self, args):
        runner.run(self.modpathEXE, params=args)