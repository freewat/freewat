# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2014 - 2015 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************
import math
import numpy as np
import datetime
#from datetime import datetime, date

def lerp(min, max, coeff):
    return (1-coeff)*min + (coeff*max)

def cond(K,L,w,th):
    c = (K*L*w)/th
    return c

def timeoffsetCalc(t0, date0, tnow, datenow, nper, perlen, timeString ):
    '''
    # t0, datetime.time object for initial time
    # date0, datetime.date object for initial time
    # tnow, datetime.time object for current time
    # datenow, datetime.date object for current time
    # nper, number of stress period (integer)
    # perlen, list of stress period length (list)
    # timeString, string of Time Unit: 'sec' OR 'hour' OR 'day'
    '''

    # Delta dates in Days
    deltaDate = (datenow - date0)
    deltaDate = deltaDate.total_seconds()/86400

    if timeString == 'sec':
        deltaDate = deltaDate*86400
        tnow = datetime.datetime.combine(datetime.date.today(), tnow)
        t0 = datetime.datetime.combine(datetime.date.today(), t0)
        deltaTime = tnow - t0
        deltaTime = deltaTime.total_seconds()
    elif timeString == 'hour':
        deltaDate = deltaDate*24
        tnow = tnow.hour + tnow.minute/60.0 + tnow.second/3600.0
        t0   = t0.hour + t0.minute/60.0 + t0.second/3600.0
        deltaTime = tnow - t0
    elif timeString == 'day':
        tnow = tnow.hour/24.0 + tnow.minute/1440.0 + tnow.second/86400.0
        t0   = t0.hour/24.0 + t0.minute/1440.0 + t0.second/86400.0
        deltaTime = tnow - t0

    totDelta = deltaTime + deltaDate

    tsp = np.ones(nper+1 )

    tsp[0] = 0
    for i in range(nper):
        tsp[i+1] = tsp[i] + perlen[i]

    for j in range(nper):
        if totDelta == tsp[j]:
            iref = j + 1
            offset = 0
        elif totDelta > tsp[j] and totDelta <= tsp[j+1]:
            iref = j + 1
            offset = totDelta - tsp[j]

    return [iref, offset, tsp]


def timeoffsetCalcIsodate(t0, date0, tnow, datenow, nper, perlen, timeString):
    '''
    # t0, datetime.time object for initial time
    # date0, datetime.date object for initial time
    # tnow, datetime.time object for current time
    # datenow, datetime.date object for current time
    # nper, number of stress period (integer)
    # perlen, list of stress period length (list)
    # timeString, string of Time Unit: 'sec' OR 'hour' OR 'day'
    '''

    # Delta dates in Days
    #deltaDate = (datenow - date0)
    totDelta = 0
    deltaDate = (datenow - date0).total_seconds() / 86400

    deltaTime = datetime.combine(datetime(1, 1, 1, 0, 0, 0), tnow) - datetime.combine(datetime(1, 1, 1, 0, 0, 0), t0)

    if timeString == 'hour':
        deltaDate = deltaDate * 24
        totDelta = (deltaTime.total_seconds() / 3600) + deltaDate
        #tnow = tnow.hour + tnow.minute / 60.0 + tnow.second / 3600.0
        #t0 = t0.hour + t0.minute / 60.0 + t0.second / 3600.0
    elif timeString == 'day':
        totDelta = (deltaTime.total_seconds() / 86400) + deltaDate
        #tnow = tnow.hour / 24.0 + tnow.minute / 1440.0 + tnow.second / 86400.0
        #t0 = t0.hour / 24.0 + t0.minute / 1440.0 + t0.second / 86400.0
    else:
        deltaDate = deltaDate * 86400
        totDelta = deltaTime.total_seconds() + deltaDate

    #deltaTime = tnow - t0
    #totDelta = deltaTime.total_seconds() + deltaDate

    tsp = np.ones(nper + 1)

    tsp[0] = 0
    for i in range(nper):
        tsp[i + 1] = tsp[i] + perlen[i]

    iref = 0
    offset = totDelta

    for j in range(nper):
        if totDelta == tsp[j]:
            iref = j + 1
            offset = 0
        elif totDelta > tsp[j] and totDelta <= tsp[j + 1]:
            iref = j + 1
            offset = totDelta - tsp[j]

    return [iref, offset, tsp]
