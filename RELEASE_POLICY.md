# FREEWAT Release process

## Definition of "release"

A release is anything that is published beyond the FREEWAT group. This means any publication outside the development community, defined as individuals actively participating in development or following the dev list or the GitLab repository.

## Release Team

The FREEWAT release team coordinates the release process, packaging and announcing new releases to the world at large.

The release team is currently composed of : 
- Iacopo Borsi
- Massimiliano Cannata
- Matteo Ghetta
- Mirko Cardoso

The release manager is in charge of the decisions related to the release process.
The current release manager is *Iacopo Borsi*

## Versions

Odd version numbers (2.1, 2.3 etc) are development versions.

Even version numbers (2.2, 2.4 etc) are release versions.

## Strategy of release

This document lays out the strategy for releases of FREEWAT. The Release Roadmap is intended to provide a 'big picture' overview of where we are going and how we intend to get there.

### Minor releases

The FREEWAT project aims to make at least two minor releases every year in addition to the major release, on a predefined schedule. If it becomes necessary due to an important bugfix or security issue, more releases will be made between these dates, so this list should be considered a minimum.

The target date for these minor releases are, unless otherwise stated, the first tuesday of November and first tuesday of March.

### Major releases

The FREEWAT project aims to make one major release per year. This release should be aligned with the latest QGIS LTR release.
Therefore, the target date for the major release is the first tuesday of July, but can be modified according to QGIS LTR schedule.

## Release process

Release will therefore happen every four month. In the first three month new development is taking place. Then a feature freeze is invoked and the final month is used for testing, bugfixing, translation and release preparations. 

### Open Development

In the development phase developers work on adding new features for the next release. Early adopters can use the source code or development releases to see the development progress, do preliminary testing and provide bug reports and their thoughts to help with development.

### Feature freeze

In the feature freeze phase new features are not allowed in anymore and the focus of everyone moves from enhancing FREEWAT to stablizing it. 

Users should start extensive testing of these prereleases in their environment to verify that there are no issues, they wouldn’t want to see in the upcoming release. All such issues should be reported (see Bugs, Features and Issues). Everything that goes unnoticed, will also end up in the next release. Only in case of serious problems backports to a latest release will occur. Therefore testing of the prereleases and reporting issues is very important.

In the feature freeze developers monitor the issue repository and start working on fixing the reported issues.

With the begin of the feature freeze the translation files will be updated so that translators can start their work. Note that this might be an incremental process as although the features are frozen, bug fixes might still introduce translation string changes.

### String freeze and call for translations

First we update the translation files, and update the release history. Then we make a 'call for translations' announcement. During this period bug fixes can continue but changes to translatable strings in the source should be avoided. At the end of the string freeze cycle we call on translators to submit their work prior to branching.

### Release

When the release happens, a branch with a even release number is created and the master branch advances to the next odd version. After the release a call for packaging is issued.

### Packaging & announcement

The packagers build the various packages needed for different platforms, particularly the QGIS FREEWAT plugin. These artefacts are published on the Gitlab platform.

### Documentation

Documentation release will happen for Major versions. The documentation will be updated in the master branch and the documentation release process will follow the major release schedule.

### Howto create a release : steps to follow

The RELEASE.md file shows the different technical steps to follow in order to build, verify and publish a release.
