<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT" sourcelanguage="en_US">
<context>
    <name>CloneSensor</name>
    <message>
        <location filename="cloneSensor_dialog.py" line="76"/>
        <source>Sensor name not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cloneSensor_dialog.py" line="107"/>
        <source>Please check time interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cloneSensor_dialog.py" line="117"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cloneSensor_dialog.py" line="117"/>
        <source>Sensor {} exist, overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cloneSensor_dialog.py" line="126"/>
        <source>Save</source>
        <translation type="unfinished">Salva</translation>
    </message>
    <message>
        <location filename="cloneSensor_dialog.py" line="126"/>
        <source>Sensor saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cloneSensor_dialog.py" line="135"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputeUSBDialog</name>
    <message>
        <location filename="runUSBcalculus_dialog.py" line="252"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="runUSBcalculus_dialog.py" line="252"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CopyFromVectorDialog</name>
    <message>
        <location filename="ui_copyFromVector.ui" line="14"/>
        <source>Copy fields from vector to vector</source>
        <translation>Copia campo da vettore a vettore</translation>
    </message>
    <message>
        <location filename="ui_copyFromVector.ui" line="127"/>
        <source>FROM:</source>
        <translation>Da:</translation>
    </message>
    <message>
        <location filename="ui_copyFromVector.ui" line="71"/>
        <source>TO:</source>
        <translation>A:</translation>
    </message>
    <message>
        <location filename="ui_copyFromVector.ui" line="104"/>
        <source>Target fields</source>
        <translation>Campo di destinazione</translation>
    </message>
    <message>
        <location filename="ui_copyFromVector.ui" line="48"/>
        <source>Origin fields</source>
        <translation>Campo di origine</translation>
    </message>
    <message>
        <location filename="ui_copyRasterToFields.ui" line="88"/>
        <source>Raster</source>
        <translation>Raster</translation>
    </message>
    <message>
        <location filename="ui_copyRasterToFields.ui" line="110"/>
        <source>Vector</source>
        <translation>Vettore</translation>
    </message>
    <message>
        <location filename="ui_copyRasterToFields.ui" line="161"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_copyRasterToFields.ui" line="14"/>
        <source>Copy raster values to vector</source>
        <translation>Copia valori da raster a vettore </translation>
    </message>
    <message>
        <location filename="ui_copyRasterToFields.ui" line="27"/>
        <source>Copy from raster to vector</source>
        <translation>Copia da raster a vettore</translation>
    </message>
    <message>
        <location filename="ui_copyRasterToFields.ui" line="128"/>
        <source>Target field</source>
        <translation>Campo di destinazione</translation>
    </message>
    <message>
        <location filename="ui_copyRasterToFields.ui" line="167"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;FROM&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: origin &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Raster&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; layer&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;TO&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: taget &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Vector&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; layer&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;As a &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Target field&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, the User must select the field from the target &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Vector&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; layer involved in the copy&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to FREEWAT User Manual (Volume 1, section 5.3). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateAddSPDialog</name>
    <message>
        <location filename="addStressPeriod_dialog.py" line="64"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
</context>
<context>
    <name>CreateCHDLayerDialog</name>
    <message>
        <location filename="ui_createCHDLayer.ui" line="14"/>
        <source>Create a layer for CHD package</source>
        <translation>Crea layer per il pacchetto CHD</translation>
    </message>
    <message>
        <location filename="ui_createCHDLayer.ui" line="34"/>
        <source>Create CHD Layer</source>
        <translation>Crea Layer CHD</translation>
    </message>
    <message>
        <location filename="ui_createCHDLayer.ui" line="59"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createCHDLayer.ui" line="69"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createCHDLayer.ui" line="102"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="createCHDLayer_dialog.py" line="101"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createCHDLayer_dialog.py" line="101"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="ui_createCHDLayer.ui" line="79"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createCHDLayer.ui" line="108"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the chd MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package requires selecting cells from the grid MDO to which &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;such boundary condition has to be assigned.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Time-Variant Specified-Head&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;CHD&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Time-Variant Specified-Head Package&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;CHD&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;) allows to simulate a specified-head boundary condition.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The groundwater flow equation is not solved within specified-head cells, as hydraulic head stays constant in these cells throughout the simulation, according to values defined by the user for each Stress Period.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must specify also to which model layer(s) this condition applies.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.1). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateCropTableDialog</name>
    <message>
        <location filename="ui_createCropTable.ui" line="14"/>
        <source>Create Table for Crops Parameters </source>
        <translation>Crea tabella per parametri di Crop</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="63"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="40"/>
        <source>Insert information on Crop Type:</source>
        <translation>Inserisci informazioni dei tipi di Crop:</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="106"/>
        <source>Load from CSV</source>
        <translation>Carica da CSV</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="120"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="140"/>
        <source>CSV Parameters Table</source>
        <translation>File CSV dei parametri</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="130"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="274"/>
        <source>Add the csv file to the Legend</source>
        <translation>Aggiungi il file CSV alla legenda</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="252"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="297"/>
        <source>Enter crop information</source>
        <translation>Inserisci informazioni dei tipi di Crop</translation>
    </message>
    <message>
        <location filename="createCropTable_dialog.py" line="85"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="562"/>
        <source>0.02</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="647"/>
        <source>0.28</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui_createCropTable.ui" line="812"/>
        <source>0.65</source>
        <translation></translation>
    </message>
    <message>
        <location filename="createCropTable_dialog.py" line="85"/>
        <source>Yuo have already created a Crop Table 
 for model %s !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateDRNLayerDialog</name>
    <message>
        <location filename="createDRNLayer_dialog.py" line="159"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createDRNLayer_dialog.py" line="159"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CreateEVTLayerDialog</name>
    <message>
        <location filename="ui_createEVTLayer.ui" line="14"/>
        <source>Create a layer for EVT package</source>
        <translation>Crea layer per il pacchetto EVT</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="27"/>
        <source>Create EVT Layer</source>
        <translation>Crea Layer EVT</translation>
    </message>
    <message>
        <location filename="ui_createUZFLayer.ui" line="62"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createUZFLayer.ui" line="88"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="45"/>
        <source>Enter evt parameters</source>
        <translation>Inserisci parametri evt</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="328"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="311"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="288"/>
        <source>CSV Parameters Table</source>
        <translation>File CSV dei parametri</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="281"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="343"/>
        <source>Add the table to the Legend</source>
        <translation>Aggiungi la tabella alla legenda</translation>
    </message>
    <message>
        <location filename="ui_createUZFLayer.ui" line="108"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createSMLLayer.ui" line="14"/>
        <source>Create Surface Model Layer</source>
        <translation>Crea Surface Model Layer</translation>
    </message>
    <message>
        <location filename="ui_createSMLLayer.ui" line="27"/>
        <source>Create SML Layer</source>
        <translation>Crea Layer SML</translation>
    </message>
    <message>
        <location filename="ui_createUZFLayer.ui" line="14"/>
        <source>Create a layer for UZF package</source>
        <translation>Crea layer per il pacchetto UZF</translation>
    </message>
    <message>
        <location filename="ui_createUZFLayer.ui" line="34"/>
        <source>Create UZF Layer</source>
        <translation>Crea Layer UZF</translation>
    </message>
    <message>
        <location filename="createEVTLayer_dialog.py" line="113"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createEVTLayer_dialog.py" line="113"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="ui_createUZFLayer.ui" line="42"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="192"/>
        <source>Load evt parameters from CSV</source>
        <translation>Carica parametri di evt da CSV</translation>
    </message>
    <message>
        <location filename="ui_createEVTLayer.ui" line="427"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the evt MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Additional parameters are required to calculate evapotranspiration loss from the water table [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/T] within each cell, at each Stress Period. The user may assign such parameters by filling a table (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Enter evt parameters&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;is checked) or by loading a csv file (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Load evt parameters from CSV&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;is checked). In this case, the user must define the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Decimal separator&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Column separator&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; used in the csv file loaded. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;In both cases, the parameters to be assigned are the following:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;sp&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: Stress Period number (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;surf&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: elevation [L] of the evapotranspiration surface (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;evtr&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: maximum evapotranspiration flux [L/T] (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;exdp&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: evapotranspiration extintion depth [L] (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;If &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the Legend&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; is checked, a table containing the evt parameters assigned is loaded in the Layers Panel. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package does not require prior processing of a polygon shapefile, as this condition can be applied to all grid cells and, eventually, it is possible to deactivate evapotranspiration at some grid cells, by using QGIS selection and editing tools.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;EVAPOTRANSPIRATION&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;EVT&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;EVAPOTRANSPIRATION Package&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;EVT&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;) allows to simulate areally-distributed evapotranspiration.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Evapotranspiration loss from the water table:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;- occurs at a maximum rate, if the water table is above a certain elevation (ET surface);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;- is null, if the depth of the water table below the ET surface exceeds a specified interval (extintion depth);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;- varies linearly with water-table elevation between these limits.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Evapotranspiration loss from the water table can be calculated in cell &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; as:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Q&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;EVT&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;=EVTR     if h&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt; &amp;gt; SURF&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Q&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;EVT&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;=EVTR [h&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;-(SURF-EXDP)/EXDP]     if SURF-EXDP ≤ h&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt; ≤ SURF&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Q&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;EVT&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;=0     if h&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt; &amp;lt; SURF-EXDP&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;where:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;EVTR&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; is the maximum possible value of evapotranspiration loss [L/T] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;SURF&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; is the ET surface elevation [L] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;EXDP&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; is the extintion depth [L] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;h&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; is the head at the node of the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;-th cell [L].&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must specify, for each Stress Period, maximum evapotranspiration rate [L/T], which will be then multiplied by the area of the grid cell [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;] to which this condition is applied, to get a volumetric flow rate [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/T].&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must also specify, for each Stress Period, to which model layer this term applies.&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.8). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createSMLLayer.ui" line="113"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the sml MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Creating the Surface Model Layer is necessary &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;only&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; if the UZF Package has to be coupled with the SFR to address run off. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Of course, if the SFR Package is activated, but the Surface Model Layer is not implemented, no run off will be relocated to the SFR segments.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must specify, within each grid cell, Manning’s roughness coefficient, slope of the bottom surface and the aspect value.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must also specify the stream segment to which run off will be addressed.&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to FREEWAT User Manual (Volume 1, section 6.9).&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createUZFLayer.ui" line="114"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the uzf MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package does not require prior processing of a polygon shapefile, as this condition can be applied to all grid cells and, eventually, it is possible to deactivate evapotranspiration and recharge at some grid cells, by using QGIS selection and editing tools.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Unsaturated-Zone Flow&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;UZF&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Unsaturated-Zone Flow Package&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;UZF&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;) allows to simulate vertical flow of water through the unsaturated zone, by etimating the evapotransporation and effective infiltration to groundwater using the land surface precipitation. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This Package also allows the estimation of direct runoff to surface waterways. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;If the UZF Package is used, RCH and EVT Packages should not be activated.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The vertical flow through the unsaturated zone is estimated by solving a kinematic wave equation, which is an approximation of the Richards’ equation. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The approach assumes that unsaturated flow occurs in response to gravity potential gradients only and ignores negative potential gradients; the approach further assumes uniform hydraulic properties in the unsaturated zone for each vertical column of model cells. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The Brooks-Corey function is used to define the relation between unsaturated hydraulic conductivity and water content. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must specify initial and saturated water contents, saturated vertical hydraulic conductivity, and an exponent in the Brooks-Corey function. Residual water content is calculated internally on the basis of the difference between saturated water content and specific yield.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must also specify, for each Stress Period, the infiltration rate [L/T] at land surface, the ET demand rate [L/T] within the ET extinction depth interval, the ET extintion depth [L] and the extinction water content below which ET cannot be removed from the unsaturated zone.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must specify as well to which model layer this condition applies.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2006/tm6a19/pdf/tm6a19.pdf&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#0000ff;&quot;&gt;Niswonger et al. (2006)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.9).&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateFarmCropSoil</name>
    <message>
        <location filename="ui_createFarmCropSoil.ui" line="14"/>
        <source>Create a layer for Crops and Soils</source>
        <translation>Crea layer per Crop e Soils</translation>
    </message>
    <message>
        <location filename="ui_createFarmCropSoil.ui" line="28"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createFarmCropSoil.ui" line="38"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createFarmCropSoil.ui" line="48"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
</context>
<context>
    <name>CreateFarmId</name>
    <message>
        <location filename="ui_createFarmId.ui" line="14"/>
        <source>Create a layer for Water Units ID (Farm ID)</source>
        <translation>Crea layer per Water Units ID (Farm ID)</translation>
    </message>
    <message>
        <location filename="ui_createFarmId.ui" line="25"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createFarmId.ui" line="48"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createFarmId.ui" line="58"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createFarmPipeline.ui" line="14"/>
        <source>Create a layer for Water Units Pipelines</source>
        <translation>Crea layer per Water Units Pipelines</translation>
    </message>
</context>
<context>
    <name>CreateFarmWELayerDialog</name>
    <message>
        <location filename="createFarmWells_dialog.py" line="105"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createFarmWells_dialog.py" line="105"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CreateFlowOBLayerDialog</name>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="20"/>
        <source>Create Flow Observations layer</source>
        <translation>Crea Flow Observation layer</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="33"/>
        <source> Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="59"/>
        <source>Gage Segment Layer (line)</source>
        <translation>Segmento layer Gage (lineare)</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="89"/>
        <source>Field of Gage Group Name</source>
        <translation>Campo del nome gruppo gage</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="113"/>
        <source>Package Type</source>
        <translation>Tipo pacchetto</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="136"/>
        <source>Select Package ...</source>
        <translation>Seleziona pacchetto...</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="164"/>
        <source>Model Data Object </source>
        <translation>Model Data Object </translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="190"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="216"/>
        <source>Use OAT sensors</source>
        <translation>Usa sensori OAT</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="246"/>
        <source>Upstream sensor</source>
        <translation>Sensore a monte</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="263"/>
        <source>Downstream sensor</source>
        <translation>Sensore a valle</translation>
    </message>
    <message>
        <location filename="ui_createRVOBLayer.ui" line="280"/>
        <source>Adapt timeseries</source>
        <translation>Adatta serie temporali</translation>
    </message>
</context>
<context>
    <name>CreateGHBLayerDialog</name>
    <message>
        <location filename="createGHBLayer_dialog.py" line="162"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createGHBLayer_dialog.py" line="162"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CreateHOBLayerDialog</name>
    <message>
        <location filename="ui_createHOBLayer.ui" line="20"/>
        <source>Create layer for HOB package</source>
        <translation>Crea layer per il pacchetto HOB</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="56"/>
        <source>Grid Layer</source>
        <translation>Reticolo</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="78"/>
        <source>Use selected OAT sensor</source>
        <translation>Usa sensori OAT selezionati</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="123"/>
        <source>Field of Observation Well Name</source>
        <translation>Campo del nome punti di osservazione</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="173"/>
        <source>Field of Top of the Screen</source>
        <translation>Campo del Top della fenestratura</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="190"/>
        <source>Name of new layer</source>
        <translation>Nome del nuovo layer</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="163"/>
        <source>Field of Bottom of the Screen</source>
        <translation>Campo del Bottom della fenestratura</translation>
    </message>
    <message>
        <location filename="createHOBLayer_dialog.py" line="120"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createHOBLayer_dialog.py" line="120"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="36"/>
        <source> Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_createHOBLayer.ui" line="99"/>
        <source>Point Layer</source>
        <translation>Vettore di punti</translation>
    </message>
</context>
<context>
    <name>CreateLAKLayerDialog</name>
    <message>
        <location filename="createLAKLayer_dialog.py" line="139"/>
        <source>Select input file</source>
        <translation>Seleziona file di input</translation>
    </message>
</context>
<context>
    <name>CreateMNWLayerDialog</name>
    <message>
        <location filename="ui_createMNWLayer.ui" line="14"/>
        <source>Create a layer for MNW package</source>
        <translation>Crea layer per il pacchetto MNW</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="27"/>
        <source>Create MNW Layer</source>
        <translation>Crea Layer MNW</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="33"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="105"/>
        <source>Create a Multi Node Layer (Model Data Object) </source>
        <translation>Crea layer MNW (Model Data Object)</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="115"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="128"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="203"/>
        <source>Create or Update Table for Well Properties</source>
        <translation>Crea o aggiorna tabella delle proprietà del pozzo</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="228"/>
        <source>Note: the well equation is
hw = hn + AQn + BQn + CQn^P
Coefficient A is determined by the model</source>
        <translation>Nota: l&apos;equazione usata è hw = hn + AQn + BQn + CQn^P. Il coefficiente A è determinato dal modello</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="612"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="createMNWLayer_dialog.py" line="190"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createMNWLayer_dialog.py" line="190"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="211"/>
        <source>MNW Model Data Object:</source>
        <translation>Model Data Object MNW:</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="509"/>
        <source>Well Radius (Rw):</source>
        <translation>Raggio del pozzo (Rw):</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="479"/>
        <source>Select Well Name:</source>
        <translation>Seleziona nome pozzo:</translation>
    </message>
    <message>
        <location filename="ui_createMNWLayer.ui" line="618"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Create a Multi Node Layer (Model Data Object)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; section:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the mnw MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Create or Update Table for Well Properties&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; section (to be filled once the mnw MDO has been created):&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;MNW Model Data Object&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the mnw MDO created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;for each multi-node well:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;   - the name assigned to the multi-node well (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;WELLID&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; field in the attribute table of the &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;   mnw MDO) must be selected from the drop-dowm menu near to &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Select Well Name&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;   - &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;the number of model layer(s) to which the multi-node well is screened must be &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;   checked in the drop-down menu &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Select Layer(s) where Well is active&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;   - Well Radius (Rw) &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;is the radius of the multi-node well&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;   - &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;B&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;P&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; are coefficients used in the general well-loss equation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;https://pubs.usgs.gov/tm/tm6a30/pdf/TM-6A30_hirez.pdf&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Konikow et al. (2009)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.3). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateModelDialog</name>
    <message>
        <location filename="ui_createModel.ui" line="17"/>
        <source>Create a model </source>
        <translation>Crea un modello</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="36"/>
        <source>Create Model</source>
        <translation>Crea modello</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="52"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="128"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="150"/>
        <source>Length Unit:</source>
        <translation>Unità di lunghezza:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="173"/>
        <source>Time Unit:</source>
        <translation>Unità di tempo:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="207"/>
        <source>Define first Stress Period</source>
        <translation>Definisci il primo Stress Period</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="242"/>
        <source>Time Steps:</source>
        <translation>Time Steps:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="256"/>
        <source>Multiplier:</source>
        <translation>Moltiplicatore:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="270"/>
        <source>State:</source>
        <translation>Stato:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="369"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="92"/>
        <source>Working Folder:</source>
        <translation>Cartella di lavoro:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="228"/>
        <source>Length:</source>
        <translation>Lunghezza:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="294"/>
        <source>Initial date and time of simulation:</source>
        <translation>Data e orario dell&apos;inizio della simulazione:</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="329"/>
        <source>Select CRS</source>
        <translation>Seleziona SR</translation>
    </message>
    <message>
        <location filename="ui_createModel.ui" line="375"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Working Folder&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: folder within which the geodatabase, the MODFLOW input files and the output files will be stored&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Length unit&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: length unit to be used. Four options are available (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;m&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;cm&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;ft&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;undefined&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Time unit&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: time unit to be used. Seven options are available (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;sec&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;min&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;hour&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;day&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;month&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;year&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;undefined&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Define first Stress Period&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; section:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Length&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: length of the first Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;to be expressed in time units&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Time Steps&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of Time Steps within the first Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Multiplier&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: multiplier to calculate the length of Time Steps within the first Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;State&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: option needed to define if the first Stress Period will be run in &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Steady State&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Transient &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;conditions&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Initial date and time of simulation&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: starting date (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;gg/mm/yy&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) and time (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;hh:mm:ss&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) of the simulation &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;By clicking on &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Select CRS&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, it is possible to select the Coordinate Reference System to be assigned to the model through the Coordinate Reference System Selector&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to FREEWAT User Manual (Volume 1, chapter 3). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateModelLayerDialog</name>
    <message>
        <location filename="ui_createModelLayer.ui" line="27"/>
        <source>Create Model Layer</source>
        <translation>Crea Model Layer</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="78"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="136"/>
        <source>Wetting Capability
 (LAYWET):</source>
        <translation>Opzione di rewetting
 (LAYWET):</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="150"/>
        <source>BOTTOM:</source>
        <translation>BOTTOM:</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="38"/>
        <source>TOP:</source>
        <translation>TOP:</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="166"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="186"/>
        <source>Model Layer Type:</source>
        <translation>Tipo model layer:</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="230"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="60"/>
        <source>Interblock Transmissivity 
(LAYAVG):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="111"/>
        <source>Model Layer Name:</source>
        <translation>Nome Model Layer:</translation>
    </message>
    <message>
        <location filename="ui_createModelLayer.ui" line="236"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Layer Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the model layer which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;TOP&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: elevation of the top surface of the model layer [L] above a reference datum &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;BOTTOM&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: elevation of the bottom surface of the model layer [L] above a reference datum&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Layer Type&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: option needed to state if the model layer is &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;confined &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;convertible&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Wetting Capability (LAYWET)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: option needed just for convertible layers to state if cells which become dry during the simulation can be rewetted (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Yes&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) or not (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;No&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Interblock Transmissivity (LAYAVG)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: method used to calculate the horizontal interblock transmissivity (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;harmonic&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;logarithmic&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;arithmetic-mean&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, subsection 4.1.1). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateModelTableDialog</name>
    <message>
        <location filename="ui_createModelTable.ui" line="14"/>
        <source>Create a Model Table</source>
        <translation>Crea Model Table</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="32"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="39"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="55"/>
        <source>Model Type:</source>
        <translation>Tipo Modello:</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="84"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="131"/>
        <source>Length Unit:</source>
        <translation>Unità di lunghezza:</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="166"/>
        <source>Time Unit:</source>
        <translation>Unità di tempo:</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="192"/>
        <source>Working Directory:</source>
        <translation>Cartella di lavoro:</translation>
    </message>
    <message>
        <location filename="ui_createModelTable.ui" line="223"/>
        <source>Browse</source>
        <translation>Cerca</translation>
    </message>
</context>
<context>
    <name>CreateMultipleSensor</name>
    <message>
        <location filename="createMultipleSensor_dialog.py" line="71"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
</context>
<context>
    <name>CreateOatAddTs</name>
    <message>
        <location filename="createSensor_dialog.py" line="170"/>
        <source>No data Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="214"/>
        <source>No observation found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="223"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="223"/>
        <source>Sensor {} exist, overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="245"/>
        <source>Save</source>
        <translation type="unfinished">Salva</translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="245"/>
        <source>Sensor saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="260"/>
        <source>Sensor name not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="263"/>
        <source>Plese define a observed property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="266"/>
        <source>Plese define a unit of measure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="293"/>
        <source>Please define a valid sensor name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createSensor_dialog.py" line="316"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateParams2DLayerDialog</name>
    <message>
        <location filename="createParams2dArray_dialog.py" line="299"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createParams2dArray_dialog.py" line="299"/>
        <source>Yuo have already created a Parameters Table 
 for model %s 
 named &quot;%s&quot;! 
 Please, change the name.</source>
        <translation>Hai già creato una tabella dei parametri per il modello %s con il nome &quot;%s&quot;! Modifica il nome.</translation>
    </message>
</context>
<context>
    <name>CreateParams3DLayerDialog</name>
    <message>
        <location filename="createParams3dArray_dialog.py" line="319"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createParams3dArray_dialog.py" line="319"/>
        <source>Yuo have already created a Parameters Table 
 for model %s 
 named &quot;%s&quot;! 
 Please, change the name.</source>
        <translation>Hai già creato una tabella dei parametri per il modello %s con il nome &quot;%s&quot;! Modifica il nome.</translation>
    </message>
</context>
<context>
    <name>CreatePartikelTracking</name>
    <message>
        <location filename="ui_createParticleTracking.ui" line="22"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="14"/>
        <source>Particle Tracking (MODPATH)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="35"/>
        <source>Package:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="59"/>
        <source>Well (WEL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="64"/>
        <source>Recharge (RCH)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="87"/>
        <source>Run MODPATH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="99"/>
        <source>Particle tracking options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="108"/>
        <source>Simulationtype </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="116"/>
        <source>Pathlines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="121"/>
        <source>Endpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="129"/>
        <source>Tracking direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="143"/>
        <source>Backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="148"/>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="156"/>
        <source>Reference time </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="180"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="170"/>
        <source>Release time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="191"/>
        <source>Time interval for
output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="202"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="213"/>
        <source>Use retardation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="233"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="255"/>
        <source>Apply Time-based Style to Pathlines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="270"/>
        <source>Load and Select a pathlines layer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="277"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createParticleTracking.ui" line="300"/>
        <source>Appy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateRCHLayerDialog</name>
    <message>
        <location filename="ui_createRCHLayer.ui" line="14"/>
        <source>Create a layer for RCH package</source>
        <translation>Crea layer per il pacchetto RCH</translation>
    </message>
    <message>
        <location filename="ui_createRCHLayer.ui" line="34"/>
        <source>Create RCH Layer</source>
        <translation>Crea Layer RCH</translation>
    </message>
    <message>
        <location filename="ui_createRCHLayer.ui" line="42"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createRCHLayer.ui" line="62"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createRCHLayer.ui" line="72"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createRCHLayer.ui" line="88"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="createRCHLayer_dialog.py" line="94"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createRCHLayer_dialog.py" line="94"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="ui_createRCHLayer.ui" line="94"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the rch MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package does not require prior processing of a polygon shapefile, as this condition can be applied to all grid cells and, eventually, it is possible to deactivate recharge at some grid cells, by using QGIS selection and editing tools.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;RECHARGE&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;RCH&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;RECHARGE Package&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;RCH&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;) allows to simulate areally-distributed recharge.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must specify, for each Stress Period, the recharge flux [L/T], which will be then internally multiplied by the area of the grid cell [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;] to which this condition is applied, to get a volumetric flow rate [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/T]. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must also specify, for each Stress Period, to which model layer this term applies.&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.4).  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateRIVLayerDialog</name>
    <message>
        <location filename="ui_createDRNLayer.ui" line="26"/>
        <source>Create a layer for DRN package</source>
        <translation>Crea layer per il pacchetto DRN</translation>
    </message>
    <message>
        <location filename="ui_createDRNLayer.ui" line="39"/>
        <source>Create DRN Layer</source>
        <translation>Crea Layer DRN</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="74"/>
        <source> Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="100"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createDRNLayer.ui" line="121"/>
        <source>Line Layer (drain segment):</source>
        <translation>Layer lineare (segmento del dreno):</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="184"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createDRNLayer.ui" line="490"/>
        <source>Enter drain parameters</source>
        <translation>Inserisci parametri del dreno</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="266"/>
        <source>Load river parameters from CSV</source>
        <translation>Carica parametri del fiume da CSV</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="387"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="438"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="358"/>
        <source>CSV Parameters Table</source>
        <translation>File CSV dei parametri</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="332"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="668"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="26"/>
        <source>Create a layer for GHB package</source>
        <translation>Crea layer per il pacchetto GHB</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="39"/>
        <source>Create GHB Layer</source>
        <translation>Crea Layer GHB</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="141"/>
        <source>Line Layer (ghb segment):</source>
        <translation>Layer lineare (segmento ghb):</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="308"/>
        <source>Enter ghb parameters</source>
        <translation>Inserisci parametri ghb</translation>
    </message>
    <message>
        <location filename="ui_createDRNLayer.ui" line="276"/>
        <source>Load drain parameters from CSV</source>
        <translation>Carica parametri del dreno da CSV</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="453"/>
        <source>Add the table to the Legend</source>
        <translation>Aggiungi la tabella alla legenda</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="26"/>
        <source>Create a layer for RIV package</source>
        <translation>Crea layer per il pacchetto RIV</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="45"/>
        <source>Create RIV Layer</source>
        <translation>Crea Layer RIV</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="496"/>
        <source>Enter river parameters</source>
        <translation>Inserisci parametri del fiume</translation>
    </message>
    <message>
        <location filename="ui_createSFRLayer.ui" line="20"/>
        <source>Create a layer for SFR package</source>
        <translation>Crea layer per il pacchetto SFR</translation>
    </message>
    <message>
        <location filename="ui_createSFRLayer.ui" line="33"/>
        <source>Create SFR Layer</source>
        <translation>Crea Layer SFR</translation>
    </message>
    <message>
        <location filename="ui_createSFRLayer.ui" line="416"/>
        <source>Enter sfr parameters</source>
        <translation>Inserisci parametri sfr</translation>
    </message>
    <message>
        <location filename="ui_createSFRLayer.ui" line="51"/>
        <source>Load sfr parameters from CSV</source>
        <translation>Carica parametri di sfr da CSV</translation>
    </message>
    <message>
        <location filename="createRIVLayer_dialog.py" line="162"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createRIVLayer_dialog.py" line="162"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="180"/>
        <source>Ghb segment (xyz):</source>
        <translation>Segmento ghb (xyz):</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="157"/>
        <source>Boundary dist:</source>
        <translation>Distanza confine:</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="206"/>
        <source>From layer:</source>
        <translation>Da layer:</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="109"/>
        <source>To layer:</source>
        <translation>A layer:</translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="458"/>
        <source>Load ghb parameters from CSV</source>
        <translation>Carica parametri di ghb da CSV</translation>
    </message>
    <message>
        <location filename="ui_createSFRLayer.ui" line="277"/>
        <source>Line Layer (stream segment):</source>
        <translation>Layer lineare (segmento del fiume):</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="207"/>
        <source>Layer number:</source>
        <translation>Numero del layer:</translation>
    </message>
    <message>
        <location filename="ui_createSFRLayer.ui" line="309"/>
        <source>Stream segment (xyz):</source>
        <translation>Segmento del fiume (xyz):</translation>
    </message>
    <message>
        <location filename="ui_createDRNLayer.ui" line="153"/>
        <source>Drain segment (xyz):</source>
        <translation>Segmento del dreno (xyz):</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="148"/>
        <source>Width:</source>
        <translation>Larghezza:</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="116"/>
        <source>River segment (xyz):</source>
        <translation>Segmento del fiume (xyz):</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="465"/>
        <source>Single Segment</source>
        <translation>Segmento singolo</translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="475"/>
        <source>Multi Segment</source>
        <translation>Segmenti multipli</translation>
    </message>
    <message>
        <location filename="ui_createDRNLayer.ui" line="636"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Line Layer (drain segment)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: line shapefile containing the profile of the drain segment previously created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the drn MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Drain segment (xyz)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of the drain segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Width&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: width [L] of the drain (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Layer number&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of the model layer to which the drain is in contact (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Additional parameters are required to calculate the hydraulic conductance of the drain bed sediments [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T] within each cell, at each Stress Period. The user may assign such parameters by filling a table (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Enter drain parameters&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;is checked) or by loading a csv file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Load drain parameters from CSV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;is checked). In this case, the user must define the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Decimal separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Column separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; used in the csv file loaded. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In both cases, the parameters to be assigned are the following:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;sp&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Stress Period number (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;elev_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: drain elevation [L] at the upstream cell of the drain segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;elev_out&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: drain elevation [L] at the downstream cell of the drain segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hc_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: hydraulic conductivity [L/T] of the drain bed sediments at the upstream cell of the drain segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hc_out&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: hydraulic conductivity [L/T] of the drain bed sediments at the downstream cell of the drain segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thick_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: thickness [L] of the drain bed sediments at the upstream cell of the drain segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thick_out&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: thickness [L] of the drain bed sediments at the downstream cell of the drain segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;If &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the Legend&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is checked, a table containing the drain parameters assigned is loaded in the Layers Panel. If &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the DB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is checked, such table is stored also in the model DB.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Activating this Package requires prior processing of a line shapefile, containing the profile of the drain within the study area.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;DRAIN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;DRAIN Package&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) allows to simulate groundwater exchange between a drainage system and an aquifer system, depending on the difference between the drain elevation and the hydraulic head of the aquifer itself.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Groundwater outflow occurs through the drainage system if the head in the aquifer is above a certain fixed elevation, called the drain elevation.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The outflow from the aquifer in cell &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is given by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Q&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;=C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;(h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;-H&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;)      if h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &amp;gt; H&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Q&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;=0      if h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; ≤ H&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;where:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the is the drain conductance [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;H&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the drain elevation [L] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the head at the node of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;-th cell [L].&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; depends on the hydraulic conductivity of the drain bed and on its geometry, according to the following equation:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;DRN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; = K&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; W&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; / M&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;where:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;K&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the hydraulic conductivity of the drain bed material [L/T] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the length of cell [L];&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;W&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the width of the drain [L] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;M&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the thickness of the drain bed [L] (user-defined).&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createGHBLayer.ui" line="656"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Line Layer (ghb segment)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: line shapefile containing the profile of the ghb segment previously created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the ghb MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Ghb segment (xyz)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of the ghb segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Boundary dist&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: distance [L] between the external source and the general-head boundary (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;From layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of the uppermost model layer to which the ghb condition is applied (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;To layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of the deepest model layer to which the ghb condition is applied (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Additional parameters are required to calculate the hydraulic conductance [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T] between the external source and each grid cell, at each Stress Period. The user may assign such parameters by filling a table (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Enter ghb parameters&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;is checked) or by loading a csv file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Load ghb parameters from CSV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;is checked). In this case, the user must define the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Decimal separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Column separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; used in the csv file loaded. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In both cases, the parameters to be assigned are the following:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;sp&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Stress Period number (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;bhe_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: head assigned to the external source [L] (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hc_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: hydraulic conductivity [L/T] between the external source and the grid cell (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thick_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: thickness [L] of the sediments between the external source and the grid cell (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;If &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the Legend&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is checked, a table containing the ghb parameters assigned is loaded in the Layers Panel. If &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the DB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is checked, such table is stored also in the model DB.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Activating this Package requires prior processing of a line shapefile, containing the profile of the general-head boundary within the study area.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;General-Head Boundary&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;GHB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;General-Head Boundary Package&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;GHB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) allows to simulate groundwater exchange between an external source and an aquifer system, depending on the head gradient between the source itself and the groundwater system.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The flow between the external source and the aquifer in cell &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is given by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;QB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;=CB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;(HB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;-h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;where:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;CB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the hydraulic conductance [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T] between the external source and the grid cell (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;HB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the head [L] assigned to the external source (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the head at the node of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;-th cell [L].&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;CB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; depends on the the cell geometry, its distance from the external source and the hydraulic conductivity between the external source and the grid cells, according to the following equation:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;CB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; = KB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; A&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; / L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;where:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;KB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the hydraulic conductivity between the external source and the grid cell [L/T] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;A&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the area of the boundary cell perpendicular to the flow [L];&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the distance between the external source and the boundary cell [L] (user-defined).&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createSFRLayer.ui" line="734"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Line Layer (stream segment)&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: line shapefile containing the profile of the stream segment previously created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the sfr MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Stream segment (xyz)&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: number of the stream segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Layer number&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: number of the model layer to which the stream is in contact (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Additional parameters are required to activate this Package. The user may assign such parameters by filling a table (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Enter sfr parameters&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;is checked) or by loading a csv file (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Load sfr parameters from CSV&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;is checked). In this case, the user must define the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Decimal separator&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Column separator&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; used in the csv file loaded. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;In both cases, the parameters to be assigned are the following:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;sp&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: SP number (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;seg_id&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: stream segment ID (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;out_seg&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ID of the stream segment located downstream with respect to the current stream segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;up_seg&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ID of the stream segment located upstream with respect to the current stream segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;iprior&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: priority flag of the diversion (used only if &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;up_seg&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; is not 0) (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;flow&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: inflow [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/T] at the upstream reach of the current stream segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;runoff&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: incoming runoff [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/T] at each reach of the current stream segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;etsw&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: volumetric rate per unit area of water removed by evapotranspiration directly from the stream channel [L/T] (positive value) (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;pptsw&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: volumetric rate per unit area of water added by precipitation directly on the stream channel [L/T] (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;roughch&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: Manning’s roughness coefficient (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hcond1&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: hydraulic conductivity [L/T] of the streambed at the upstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thickm1&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: thickness [L] of streambed material at the upstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;elevup&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: elevation [L] of the top of the streambed at the upstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;width1&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: average width [L] of the stream channel at the upstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thts1&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: saturated volumetric water content in the unsaturated zone beneath the upstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thti1&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: initial volumetric water content beneath the upstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;eps1&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: Brooks-Corey exponent used in the relation between water content and hydraulic conductivity within the unsaturated zone beneath the upstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hcond2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: hydraulic conductivity [L/T] of the streambed at the downstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thickm2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: thickness [L] of streambed material at the downstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;elevdn&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: elevation [L] of the top of the streambed at the downstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;width2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: average width [L] of the stream channel at the downstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thts2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: saturated volumetric water content in the unsaturated zone beneath the downstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thti2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: initial volumetric water content beneath the downstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;eps2&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: Brooks-Corey exponent used in the relation between water content and hydraulic conductivity within the unsaturated zone beneath the downstream reach of the current segment (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;).&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;If &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the Legend&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; is checked, a table containing the river parameters assigned is loaded in the Layers Panel. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package requires prior processing of a line shapefile, containing the profile of the stream within the study area.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Streamflow-Routing&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;SFR&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Streamflow-Routing Package&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;SFR&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;) allows to simulate stream-flow to downstream streams using a kinematic wave equation. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Unsaturated flow beneath streams can be simulated as well if the UZF Package is used.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Besides the sfr parameters listed above, the user must also specify to which model layer this condition applies.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2006/tm6A13/pdf/tm6a13.pdf&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Niswonger and Prudic (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.10).&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="132"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Line Layer:&lt;br&gt;(river segment)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createRIVLayer.ui" line="683"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Line Layer (river segment)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: line shapefile containing the profile of the river segment previously created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the riv MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;River segment (xyz)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Width&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: width [L] of the river (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Layer number&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of the model layer to which the river is in contact (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Additional parameters are required to calculate the hydraulic conductance of the river bed sediments [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T] within each cell, at each Stress Period. The user may assign such parameters by filling a table (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Enter river parameters&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;is checked) or by loading a csv file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Load river parameters from CSV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;is checked). In this case, the user must define the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Decimal separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Column separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; used in the csv file loaded. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In both cases, the parameters to be assigned are the following:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;sp&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Stress Period number (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;rs_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: river stage [L] at the upstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;rs_out&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: river stage [L] at the downstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;bt_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: height [L] of the river bed bottom at the upstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;bt_out&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: height [L] of the river bed bottom at the downstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hc_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: hydraulic conductivity [L/T] of the river bed sediments at the upstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hc_out&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: hydraulic conductivity [L/T] of the river bed sediments at the downstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thick_in&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: thickness [L] of the river bed sediments at the upstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;thick_out&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: thickness [L] of the river bed sediments at the downstream cell of the river segment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;If &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the Legend&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is checked, a table containing the river parameters assigned is loaded in the Layers Panel. If &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Add the table to the DB&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is checked, such table is stored also in the model DB.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Activating this Package requires prior processing of a line shapefile, containing the profile of the river within the study area.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;RIVER&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;RIVER Package&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) allows to simulate river/aquifer seepage, depending on the head gradient between the river and the groundwater system.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The river bed is made of low permeability material and the water level in the model cell stays above its bottom.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The flow between the river and the aquifer in cell &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is given by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Q&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;=C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;(H&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;-h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;) &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;where:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the conductance of the river bed material [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;H&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the river stage [L] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;h&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the head at the node of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;-th cell [L].&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; depends on the hydraulic conductivity of the river bed and on its geometry, according to the following equation:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:super;&quot;&gt;RIV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; = K&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; W&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt; / M&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;where:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;K&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the hydraulic conductivity of the river bed material [L/T] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the length of cell [L];&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;W&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the width of the river bed [L] (user-defined);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;M&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; vertical-align:sub;&quot;&gt;n&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; is the thickness of the river bed [L] (user-defined).&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateROVBLayerDialog</name>
    <message>
        <location filename="createRVOBLayer_dialog.py" line="168"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createRVOBLayer_dialog.py" line="155"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="createRVOBLayer_dialog.py" line="168"/>
        <source>{}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CreateReactionLayerDialog</name>
    <message>
        <location filename="ui_createRCTLayer.ui" line="14"/>
        <source>Create layer for REACTION process</source>
        <translation>Crea layer per processo REACTION</translation>
    </message>
    <message>
        <location filename="ui_createRCTLayer.ui" line="42"/>
        <source>Transport Model:</source>
        <translation>Modello di trasporto:</translation>
    </message>
    <message>
        <location filename="ui_createRCTLayer.ui" line="71"/>
        <source>Grid layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createRCTLayer.ui" line="91"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createRCTLayer.ui" line="111"/>
        <source>Flow Model:</source>
        <translation>Modello di flusso:</translation>
    </message>
</context>
<context>
    <name>CreateSFRLayerDialog</name>
    <message>
        <location filename="createSFRLayer_dialog.py" line="119"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createSFRLayer_dialog.py" line="119"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CreateSMLLayerDialog</name>
    <message>
        <location filename="createSMLLayer_dialog.py" line="107"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createSMLLayer_dialog.py" line="103"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="createSMLLayer_dialog.py" line="106"/>
        <source>You have already created a SML layer. 
Add your data in the existing layer!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateSSMLayerDialog</name>
    <message>
        <location filename="ui_createSSMLayer.ui" line="23"/>
        <source>Create Sinks and Sources for Transport Model</source>
        <translation>Crea termini di sorgente/estrazione per il modello di trasporto</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="35"/>
        <source>Flow Model:</source>
        <translation>Modello di flusso:</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="64"/>
        <source>Transport Model:</source>
        <translation>Modello di trasporto:</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="93"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Activate MODFLOW Packages&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attiva pacchetti MODFLOW&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="100"/>
        <source>Distributed Sources (RCH and EVT)</source>
        <translation>Sorgenti distribuite (RCH e EVT)</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="115"/>
        <source>Grid Layer</source>
        <translation>Reticolo</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="135"/>
        <source>Layer Name:</source>
        <translation>Nome del layer:</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="158"/>
        <source>RCH</source>
        <translation>RCH</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="622"/>
        <source>Select StressPeriod(s)</source>
        <translation>Seleziona Stress Period(s)</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="185"/>
        <source>EVT</source>
        <translation>EVT</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="220"/>
        <source>Point Sources</source>
        <translation>Sorgenti Puntuale</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="635"/>
        <source>Layer Name</source>
        <translation>Nome del layer</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="267"/>
        <source>GHB</source>
        <translation>GHB</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="309"/>
        <source>WEL</source>
        <translation>WEL</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="349"/>
        <source>RIV</source>
        <translation>RIV</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="386"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="523"/>
        <source>Mass Loading</source>
        <translation>Trasporto di massa</translation>
    </message>
    <message>
        <location filename="ui_createSSMLayer.ui" line="595"/>
        <source>Constant Conc.</source>
        <translation>Concentrazione costante</translation>
    </message>
</context>
<context>
    <name>CreateSoilTableDialog</name>
    <message>
        <location filename="ui_createSoilsTable.ui" line="14"/>
        <source>Create Table for Soils Parameters </source>
        <translation>Crea tabella per parametri del suolo</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="260"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="239"/>
        <source>Insert Soils and their properties:</source>
        <translation>Inserisci suoli e loro proprietà:</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="305"/>
        <source>Enter soil information</source>
        <translation>Inserisci informazioni dei suoli</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="372"/>
        <source>soil_name</source>
        <translation>soil_name</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="387"/>
        <source>soil_id</source>
        <translation>soil_id</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="395"/>
        <source>CapFringe</source>
        <translation>CapFringe</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="66"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="41"/>
        <source>Load parameters from CSV</source>
        <translation>Carica parametri da CSV</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="73"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="83"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="176"/>
        <source>CSV Parameters Table</source>
        <translation>File CSV dei parametri</translation>
    </message>
    <message>
        <location filename="ui_createSoilsTable.ui" line="202"/>
        <source>Add the table to the Legend</source>
        <translation>Aggiungi la tabella alla legenda</translation>
    </message>
</context>
<context>
    <name>CreateSoilsTableDialog</name>
    <message>
        <location filename="createSoilsTable_dialog.py" line="85"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createSoilsTable_dialog.py" line="85"/>
        <source>Yuo have already created a Soils Table 
 for model %s !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateTRNSPLayerDialog</name>
    <message>
        <location filename="ui_createTRNSPLayer.ui" line="14"/>
        <source>Create layer(s) for TRANSPORT process</source>
        <translation>Crea layer(s) per processo di TRASPORTO</translation>
    </message>
    <message>
        <location filename="ui_createTRNSPLayer.ui" line="26"/>
        <source>Flow Model:</source>
        <translation>Modello di flusso:</translation>
    </message>
    <message>
        <location filename="ui_createTRNSPLayer.ui" line="55"/>
        <source>Transport Model:</source>
        <translation>Modello di trasporto:</translation>
    </message>
    <message>
        <location filename="ui_createTRNSPLayer.ui" line="81"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
</context>
<context>
    <name>CreateTransportModelDialog</name>
    <message>
        <location filename="ui_createTransportTable.ui" line="14"/>
        <source>Create Transport Model </source>
        <translation>Crea modello di trasporto</translation>
    </message>
    <message>
        <location filename="ui_createTransportTable.ui" line="58"/>
        <source>Flow Model Name</source>
        <translation>Modello di flusso</translation>
    </message>
    <message>
        <location filename="ui_createTransportTable.ui" line="92"/>
        <source>Set a name for Transport Model</source>
        <translation>Inserisci un nome per il modello</translation>
    </message>
    <message>
        <location filename="ui_createTransportTable.ui" line="113"/>
        <source>Mass Unit </source>
        <translation>Unità di massa</translation>
    </message>
    <message>
        <location filename="ui_createTransportTable.ui" line="178"/>
        <source>Insert information on chemical species:</source>
        <translation>Inserisci informazioni delle specie chimiche:</translation>
    </message>
    <message>
        <location filename="ui_createTransportTable.ui" line="194"/>
        <source>Enter Species manually</source>
        <translation>Inserisci manualmente le specie</translation>
    </message>
    <message>
        <location filename="ui_createTransportTable.ui" line="296"/>
        <source>Load from CSV</source>
        <translation>Carica da CSV</translation>
    </message>
    <message>
        <location filename="ui_createTransportTable.ui" line="316"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
</context>
<context>
    <name>CreateTransportTable</name>
    <message>
        <location filename="createTransportModel_dialog.py" line="127"/>
        <source>Table is not in  TOC!!</source>
        <translation>La tabella non è nella TOC!</translation>
    </message>
    <message>
        <location filename="createTransportModel_dialog.py" line="127"/>
        <source>There is no transport table in TOC But a transport table is in your DB Add the table to your TOC </source>
        <translation>Non c&apos;è una tabella nella TOC ma una tabella di trasporto è presente nel DB. Caricala nella TOC</translation>
    </message>
</context>
<context>
    <name>CreateUSBLayerDialog</name>
    <message>
        <location filename="createUSBLayer_dialog.py" line="88"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createUSBLayer_dialog.py" line="88"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="ui_createUSBLayer.ui" line="52"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="102"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="42"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="287"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="156"/>
        <source>UZF (Unsaturated Zone)</source>
        <translation>UZF (Zona insatura)</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="173"/>
        <source>Surface Layer:</source>
        <translation>Surface Layer:</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="186"/>
        <source>UZF Layer:</source>
        <translation>UZF Layer:</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="216"/>
        <source>To/From Only Top Model Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="221"/>
        <source>To/From layer specified in iuzfbnd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="226"/>
        <source>To/From Highest Active Cell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="237"/>
        <source>Recharge Option:</source>
        <translation>Opzioni ricarica:</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="263"/>
        <source>Simulate Evapotranspiration</source>
        <translation>Simula evapotraspirazione</translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="273"/>
        <source>Use SFR Package</source>
        <translation>Usa pacchetto SFR</translation>
    </message>
    <message>
        <location filename="ui_createUSBLayer.ui" line="14"/>
        <source>Create a layer for Unsat Solute Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createUSBLayer.ui" line="27"/>
        <source>Create USB Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createUSBLayer.ui" line="101"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the chd MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package requires selecting cells from the grid MDO where a&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;solute source for unsaturated zone is present.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="14"/>
        <source>Compute Unsat Solute Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="27"/>
        <source>Input for USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="62"/>
        <source>Grid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="82"/>
        <source>USB Layer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="137"/>
        <source>Notice: 
before to run this tool, you must insert again inputs for UZF package!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_computeUnsatSoluteBalance.ui" line="293"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the chd MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package requires .....&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateUZFLayerDialog</name>
    <message>
        <location filename="createUZFLayer_dialog.py" line="107"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createUZFLayer_dialog.py" line="107"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CreateUZTLayerDialog</name>
    <message>
        <location filename="ui_createUZTLayer.ui" line="14"/>
        <source>Create layer for Unsat Transport process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createUZTLayer.ui" line="42"/>
        <source>Transport Model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createUZTLayer.ui" line="71"/>
        <source>Grid layer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createUZTLayer.ui" line="91"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createUZTLayer.ui" line="111"/>
        <source>Flow Model:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWELLayerDialog</name>
    <message>
        <location filename="ui_createFarmWELLayer.ui" line="14"/>
        <source>Create a layer for wells associated to a Water Unit (Farm Wells)</source>
        <translation>Crea layer per Farm Wells</translation>
    </message>
    <message>
        <location filename="ui_createFarmWELLayer.ui" line="25"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createFarmWELLayer.ui" line="48"/>
        <source>WEL or MNW Layer:</source>
        <translation>Layer WEL o MNW:</translation>
    </message>
    <message>
        <location filename="ui_createFarmWELLayer.ui" line="58"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createWELLayer.ui" line="14"/>
        <source>Create a layer for WEL package</source>
        <translation>Crea layer per il pacchetto WEL</translation>
    </message>
    <message>
        <location filename="ui_createWELLayer.ui" line="27"/>
        <source>Create WEL Layer</source>
        <translation>Crea Layer WEL</translation>
    </message>
    <message>
        <location filename="ui_createWELLayer.ui" line="58"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createWELLayer.ui" line="81"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createWELLayer.ui" line="87"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the wel MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package requires prior processing of a point shapefile, containing the location of recharge/pumping wells within the study area. Once the point shapefile is loaded in the Layers Panel, cells intersected by these points must be selected from the grid MDO&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;WELL&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;Package &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;(&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;WEL&lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;MODFLOW &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;WELL Package&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;WEL&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;) allows to simulate recharge to the aquifer or extraction of groundwater, defining a specified positive or negative flux, respectively, &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;to individual cells.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must specify the recharge/extraction flow rate [L&lt;/span&gt;&lt;span style=&quot; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/T] for each Stress Period (positive values for recharge wells; negative values for extraction wells) and the model layer(s) to which this condition applies.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;http://pubs.usgs.gov/tm/2005/tm6A16/PDF/TM6A16.pdf&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (2005)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.2).&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWELayerDialog</name>
    <message>
        <location filename="createWELLayer_dialog.py" line="106"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createWELLayer_dialog.py" line="106"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CreateWaterSitesTableDialog</name>
    <message>
        <location filename="ui_createWaterSitesTable.ui" line="14"/>
        <source>Create the Table of Water Demand Sites Properties</source>
        <translation>Crea tabella delle proprietà di richiesta idrica</translation>
    </message>
    <message>
        <location filename="ui_createWaterSitesTable.ui" line="25"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createWaterSitesTable.ui" line="45"/>
        <source>Layer of Water Demand Sites </source>
        <translation>Layer di un sito di richiesta</translation>
    </message>
    <message>
        <location filename="createWaterSitesTable_dialog.py" line="99"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createWaterSitesTable_dialog.py" line="99"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>CreateZONLayerDialog</name>
    <message>
        <location filename="ui_createZoneLayer.ui" line="14"/>
        <source>Create a layer for Zones</source>
        <translation>Crea layer per il pacchetto Zone</translation>
    </message>
    <message>
        <location filename="ui_createZoneLayer.ui" line="34"/>
        <source>Create Zone Layer</source>
        <translation>Crea Layer Zone</translation>
    </message>
    <message>
        <location filename="ui_createZoneLayer.ui" line="42"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_createZoneLayer.ui" line="65"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_createZoneLayer.ui" line="75"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_createZoneLayer.ui" line="88"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createZoneLayer.ui" line="94"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Grid Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: grid MDO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the zonbud MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The Zonebudget (ZONBUD) is a computer program that computes sub-regional water budgets using results from the MODFLOW groundwater flow model.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The user must define the sub-regions by specifying zone numbers. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;A separate budget is computed for each zone and includes also a component of flow between each adjacent zone. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Activating this Package does not require prior processing of a polygon shapefile, as this condition can be applied to all grid cells and the user can define zones &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;by using QGIS selection and editing tools.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to &lt;/span&gt;&lt;a href=&quot;https://water.usgs.gov/nrp/gwsoftware/zonebud3/ofr90392.pdf&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Harbaugh (1990)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and to FREEWAT User Manual (Volume 1, section 6.11). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateZoneLayerDialog</name>
    <message>
        <location filename="createZoneLayer_dialog.py" line="109"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createZoneLayer_dialog.py" line="109"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="formHydroUnitSelection.ui" line="58"/>
        <source>Observations</source>
        <translation>Osservazioni</translation>
    </message>
    <message>
        <location filename="formCreateDB.ui" line="60"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="22"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_loadCropCoefficients.ui" line="48"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_SimplePlotCalibration.ui" line="109"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_loadCropCoefficients.ui" line="88"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_loadCropCoefficients.ui" line="216"/>
        <source>Browse</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="ui_updateWd.ui" line="94"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="141"/>
        <source>Update</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="ui_updateWd.ui" line="57"/>
        <source>Working Directory:</source>
        <translation>Cartella di lavoro:</translation>
    </message>
    <message>
        <location filename="ui_SimplePlotCalibration.ui" line="14"/>
        <source>Plot Observed vs Simulated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_SimplePlotCalibration.ui" line="67"/>
        <source>Save Residual Statistics
 as csv file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_SimplePlotCalibration.ui" line="81"/>
        <source>Show observation labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_SimplePlotCalibration.ui" line="94"/>
        <source>Save Residuals
 as csv file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_SimplePlotCalibration.ui" line="102"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select the stress period&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="farm_crop_test.ui" line="85"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="52"/>
        <source>Crop Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_updateWd.ui" line="24"/>
        <source>Update Working Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_updateWd.ui" line="100"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Working Folder&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: location of the new working folder (it can be reached through the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Browse&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;button)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="42"/>
        <source>Crop table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="72"/>
        <source>Seed Time (ts)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="82"/>
        <source>Climate table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createCrop.ui" line="92"/>
        <source>Harvest Time (th)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_aboutdialogbase.ui" line="14"/>
        <source>About FREEWAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_aboutdialogbase.ui" line="20"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#00007f;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; color:#00007f;&quot;&gt;FREEWAT&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_aboutdialogbase.ui" line="37"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;FREEWAT Development has received funding from the following projects:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;1. Hydrological part has been developed starting from a former project, named SID&amp;amp;GRID, funded by Regione Toscana through EU POR-FSE 2007-2013 (&lt;/span&gt;&lt;a href=&quot;http://ut11.isti.cnr.it/SIDGRID/index.php&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;sidgrid.isti.cnr.it)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;2. Porting of SID&amp;amp;GRID under QGis has been performed through funds provided by Regione Toscana to Scuola Superiore S.Anna -  Project &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-style:italic;&quot;&gt;Evoluzione del sistema open source SID&amp;amp;GRID di elaborazione dei dati geografici vettoriali e raster per il porting negli ambienti QGis e Spatialite in uso presso la Regione Toscana&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt; (CIG: ZA50E4058A) &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;3. Saturated zone solute transport  simulation capability has been developed within the EU &lt;/span&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;FP7-ENV-2013-WATER-INNO-DEMO MARSOL. MARSOL project receives funding from the European Union&apos;s Seventh Framework Programme for Research, Technological Development and Demonstration under grant agreement n. 619120&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt; (&lt;/span&gt;&lt;a href=&quot;http://www.marsol.eu&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;www.marsol.eu&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;4. Latest Version of FREEWAT is under development within EU H2020  project FREEWAT - Free and Open Source Software Tools for Water Resource Management. FREEWAT project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement n. 642224   (&lt;/span&gt;&lt;a href=&quot;http://www.freewat.eu&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;www.freewat.eu&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_loadCropCoefficients.ui" line="14"/>
        <source>Load Root and Crop Coefficients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_loadCropCoefficients.ui" line="78"/>
        <source>Load CSV file of ROOT and Kc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formNormativeSelection.ui" line="14"/>
        <source>Normative Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formNormativeSelection.ui" line="66"/>
        <source>Normative to apply:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formNormativeSelection.ui" line="73"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formNormativeSelection.ui" line="87"/>
        <source>Responsible:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="82"/>
        <source>Details:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="17"/>
        <source>Hydrogeological Unit Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="30"/>
        <source>Unit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="37"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="44"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="115"/>
        <source>Type Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="151"/>
        <source>Create Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formHydroUnitSelection.ui" line="158"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formCreateDB.ui" line="14"/>
        <source>AkvaGIS Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formCreateDB.ui" line="41"/>
        <source>Layer Group:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formCreateDB.ui" line="48"/>
        <source>Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formCreateDB.ui" line="86"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="17"/>
        <source>Manage Hydrochemical Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="23"/>
        <source>Samples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="31"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="45"/>
        <source>Load Samples from CSV file for this point...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="65"/>
        <source>Refresh Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="101"/>
        <source>Edit Point...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="120"/>
        <source>Add/Edit Sample...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="139"/>
        <source>Add/Edit Campaign...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="166"/>
        <source>Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="184"/>
        <source>Sample:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="198"/>
        <source>Load Measurements from CSV file for this sample...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formDBManagement.ui" line="222"/>
        <source>Add/Edit Measurements...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FREEWAT</name>
    <message>
        <location filename="Freewat.py" line="885"/>
        <source>Model Setup</source>
        <translation type="unfinished">Impostazioni del modello</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="895"/>
        <source>MODFLOW Boundary Conditions</source>
        <translation type="unfinished">Condizioni al contorno MODFLOW</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="925"/>
        <source>Water Management and Crop Modeling (FARM PROCESS) </source>
        <translation type="unfinished">Gestione risorsa idrica (FARM PROCESS)</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="953"/>
        <source>Tools</source>
        <translation type="unfinished">Strumenti</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="960"/>
        <source>DataBase</source>
        <translation type="unfinished">DataBase</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="973"/>
        <source>Post-processing</source>
        <translation type="unfinished">Post-processing</translation>
    </message>
    <message>
        <location filename="oatInit.py" line="47"/>
        <source>OAT</source>
        <translation type="unfinished">OAT</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="formQueryMeasurements.ui" line="239"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="360"/>
        <source>Remove</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="591"/>
        <source>Start date</source>
        <translation>Data iniziale</translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffMapSettings.ui" line="20"/>
        <source>Stiff settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffMapSettings.ui" line="45"/>
        <source>Plot Height (Length):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffMapSettings.ui" line="65"/>
        <source>Horizontal Scale (meq/Length):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formSamplesResults.ui" line="14"/>
        <source>Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formSamplesResults.ui" line="26"/>
        <source>Result Samples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formSamplesResults.ui" line="48"/>
        <source>customResultsGroup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formSamplesResults.ui" line="83"/>
        <source>Save Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formSamplesResults.ui" line="90"/>
        <source>Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="454"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetExportMixSettings.ui" line="20"/>
        <source>Choose end members and samples to export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="20"/>
        <source>Piper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="37"/>
        <source>Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="43"/>
        <source>Pane separation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="78"/>
        <source>Pane surface color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="100"/>
        <source>PushButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="107"/>
        <source>Pane border color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="145"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="267"/>
        <source>Number of divisions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="161"/>
        <source>Line style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="171"/>
        <source>Line color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="181"/>
        <source>Line Thickness:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="813"/>
        <source>solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="818"/>
        <source>dashed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="224"/>
        <source>Subgrid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="230"/>
        <source>Subgrid line color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="240"/>
        <source>Subgrid line thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="250"/>
        <source>Subgrid line style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="303"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="322"/>
        <source>Corners label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="332"/>
        <source>Grid label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetPiperPlotSettings.ui" line="342"/>
        <source>Axes label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="20"/>
        <source>Stiff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="34"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="40"/>
        <source>Vertical line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="47"/>
        <source>Horizontal lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="54"/>
        <source>Horizontal axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="61"/>
        <source>Species</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="71"/>
        <source>Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="1050"/>
        <source>Font size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="107"/>
        <source>Polygon color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="117"/>
        <source>y length:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetStiffPlotSettings.ui" line="141"/>
        <source>x range:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralMapSettings.ui" line="20"/>
        <source>Value to use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapGeneralPlotSettings.ui" line="45"/>
        <source>Earliest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapGeneralPlotSettings.ui" line="52"/>
        <source>Latest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapGeneralPlotSettings.ui" line="59"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapGeneralPlotSettings.ui" line="66"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapGeneralPlotSettings.ui" line="73"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="20"/>
        <source>General Plot Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="34"/>
        <source>Plot Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="52"/>
        <source>X (pixels):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="62"/>
        <source>DPI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="128"/>
        <source>Y (pixels):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="188"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="212"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="237"/>
        <source>Font type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="296"/>
        <source>Font color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="318"/>
        <source>ColorPicker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="419"/>
        <source>Marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="443"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="482"/>
        <source>Edge width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="844"/>
        <source>gist_earth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="849"/>
        <source>terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="854"/>
        <source>ocean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="859"/>
        <source>gist_stern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="864"/>
        <source>brg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="869"/>
        <source>CMRmap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="874"/>
        <source>cubehelix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="879"/>
        <source>gnuplot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="884"/>
        <source>gnuplot2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="889"/>
        <source>gist_ncar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="894"/>
        <source>nipy_spectral</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="899"/>
        <source>jet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="904"/>
        <source>rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="909"/>
        <source>gist_rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="914"/>
        <source>hsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="919"/>
        <source>flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="924"/>
        <source>prism</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="929"/>
        <source>Accent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="934"/>
        <source>Dark2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="939"/>
        <source>Paired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="944"/>
        <source>Pastel1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="949"/>
        <source>Pastel2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="954"/>
        <source>Set1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="959"/>
        <source>Set2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="964"/>
        <source>Set3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="746"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="656"/>
        <source>filled set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="661"/>
        <source>circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="666"/>
        <source>complete set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="768"/>
        <source>Colorset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="715"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="790"/>
        <source>Line width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="823"/>
        <source>alternate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="991"/>
        <source>Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="1017"/>
        <source>Marker scale:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="1082"/>
        <source>Number of columns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetGeneralPlotSettings.ui" line="1092"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="14"/>
        <source>Query Samples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="31"/>
        <source>Query Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="113"/>
        <source>Start Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="123"/>
        <source>End Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="174"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="296"/>
        <source>Run Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="256"/>
        <source>Samples Time Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="281"/>
        <source>Spatial Selection (Points)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="319"/>
        <source>Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="347"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="373"/>
        <source>Remove All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="429"/>
        <source>Activate/Deactivate Queried Samples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="444"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="460"/>
        <source>by campaign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="473"/>
        <source>by date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="410"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="376"/>
        <source>Deselect All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="554"/>
        <source>by points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="639"/>
        <source>dd/MM/yyyy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="626"/>
        <source>End date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="687"/>
        <source>Apply Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQuerySamples.ui" line="724"/>
        <source>Active Samples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="383"/>
        <source>Activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="390"/>
        <source>Deactivate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="20"/>
        <source>Time Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="47"/>
        <source>Automatic title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="54"/>
        <source>Time axis format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="65"/>
        <source>time</source>
        <translation type="unfinished">time</translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="70"/>
        <source>day-time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="75"/>
        <source>month-day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="80"/>
        <source>month-day-time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="85"/>
        <source>year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="90"/>
        <source>year-month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="95"/>
        <source>year-month-day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetTimePlotSettings.ui" line="100"/>
        <source>year-month-day-time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapGeneralPlotSettings.ui" line="20"/>
        <source>Value </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="14"/>
        <source>Query Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="31"/>
        <source>Measurements Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="65"/>
        <source>Sample Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="103"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="109"/>
        <source>Used Parameters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="116"/>
        <source>Available Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="138"/>
        <source>&gt;</source>
        <translation type="unfinished">&gt;</translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="145"/>
        <source>&lt;</source>
        <translation type="unfinished">&lt;</translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="244"/>
        <source>&lt; Limit Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="249"/>
        <source>&gt; Limit Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="254"/>
        <source>paramCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="326"/>
        <source>Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="348"/>
        <source>Active Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formQueryMeasurements.ui" line="447"/>
        <source>Next...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="20"/>
        <source>SAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="47"/>
        <source>Background colors:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="58"/>
        <source>Blues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="63"/>
        <source>BuGn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="68"/>
        <source>BuPu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="73"/>
        <source>GnBu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="78"/>
        <source>Greens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="83"/>
        <source>Greys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="88"/>
        <source>Oranges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="93"/>
        <source>OrRd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="98"/>
        <source>PuBu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="103"/>
        <source>PuBuGn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="108"/>
        <source>PuRd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="113"/>
        <source>Purples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="118"/>
        <source>RdPu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="123"/>
        <source>Reds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="128"/>
        <source>YlGn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="133"/>
        <source>YlGnBu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="138"/>
        <source>YlOrBrx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="143"/>
        <source>YlOrBr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetSARPlotSettings.ui" line="148"/>
        <source>YlOrRd</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Freewat</name>
    <message>
        <location filename="Freewat.py" line="271"/>
        <source>Freewat</source>
        <translation>Freewat</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="406"/>
        <source>Create Model</source>
        <translation>Crea modello</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="413"/>
        <source>&amp;Program &amp;Locations</source>
        <translation>&amp;Eseguibili dei programmi</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="420"/>
        <source>Create Grid</source>
        <translation>Crea reticolo</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="427"/>
        <source>Create Model Layer</source>
        <translation>Crea model layer</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="434"/>
        <source>Create CHD Layer</source>
        <translation>Crea Layer CHD</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="441"/>
        <source>Create WEL Layer</source>
        <translation>Crea Layer WEL</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="448"/>
        <source>Create MNW Layer</source>
        <translation>Crea Layer MNW</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="455"/>
        <source>Create RCH Layer</source>
        <translation>Crea Layer RCH</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="462"/>
        <source>Create RIV Layer</source>
        <translation>Crea Layer RIV</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="473"/>
        <source>Create DRN Layer</source>
        <translation>Crea Layer DRN</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="480"/>
        <source>Create GHB Layer</source>
        <translation>Crea Layer GHB</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="487"/>
        <source>Create EVT Layer</source>
        <translation>Crea Layer EVT</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="494"/>
        <source>Create UZF Layer</source>
        <translation>Crea Layer UZF</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="501"/>
        <source>Create SFR Layer</source>
        <translation>Crea Layer SFR</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="508"/>
        <source>Create Surface Model Layer</source>
        <translation>Crea Surface Model Layer</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="514"/>
        <source>Create Zones Layer</source>
        <translation>Crea Layer Zone</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="522"/>
        <source>Create &amp;Transport Model</source>
        <translation>Crea modello di trasporto</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="529"/>
        <source>Create &amp;Transport Layer(s)</source>
        <translation>Crea layer(s) di trasporto</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="537"/>
        <source>Create Sink and Source Layer</source>
        <translation>Crea layer sink e source</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="545"/>
        <source>Create Reaction Layer</source>
        <translation>Crea layer reazione</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="602"/>
        <source>Define Soils properties</source>
        <translation>Definisci proprietà suolo</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="626"/>
        <source>Define Crops properties</source>
        <translation>Definisci proprietà crop</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="671"/>
        <source>Copy from &amp;Vector layer</source>
        <translation>Copia da vettore</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="678"/>
        <source>Copy from &amp;Raster layer</source>
        <translation>Copia da raster</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="685"/>
        <source>Merge SpatiaLite layers</source>
        <translation>Unisci layer SpatiaLite</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="692"/>
        <source>Add Stress Period</source>
        <translation>Aggiungi Stress Period</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="706"/>
        <source>Run Model</source>
        <translation>Esegui modello</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="713"/>
        <source>View Model Output</source>
        <translation>Vedi output del modello</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="725"/>
        <source>View Model Volumetric Budget</source>
        <translation>Visualizza budget volumetrico</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="731"/>
        <source>Run Zone Budget</source>
        <translation>Esegui Zone budget</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="764"/>
        <source>Regione Toscana</source>
        <translation>Regione Toscana</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="767"/>
        <source>GeoBasi</source>
        <translation>GeoBasi</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="771"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="1021"/>
        <source>&amp;Freewat</source>
        <translation>&amp;Freewat</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="551"/>
        <source>Create Unsaturated Zone Layer</source>
        <translation>Crea Zone Layer insaturo</translation>
    </message>
    <message>
        <location filename="Freewat.py" line="558"/>
        <source>Create Unsat Solute Balance Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="565"/>
        <source>Run Unsat Solute Balance Calculus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="572"/>
        <source>Define Water Demand Units (Farm_ID)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="578"/>
        <source>Insert Water Units Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="584"/>
        <source>Load External Deliveries (Non-routed Deliv.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="590"/>
        <source>Load GW Rights (GW Allotments)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="596"/>
        <source>Load SW Rights (SW Allotments)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="608"/>
        <source>Define Wells for a Water Unit (Farm Well)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="614"/>
        <source>Define Pipelines for a Water Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="620"/>
        <source>Define Crops and Soils for a Water Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="632"/>
        <source>Load Root and Crop Coefficients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="638"/>
        <source>Load Precipitation Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="646"/>
        <source>Create Head Observations Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="652"/>
        <source>Create Flow Observations Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="658"/>
        <source>Create Table for 3D-array parameters (LPF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="664"/>
        <source>Create Table for 2D Time-varying parameters (RCH, EVT)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="698"/>
        <source>Update Working Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="719"/>
        <source>View a Cross Section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Freewat.py" line="737"/>
        <source>Plot Model Fit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FreewatDialog</name>
    <message>
        <location filename="ui_Freewat.ui" line="32"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="ui_Freewat.ui" line="39"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="ui_Freewat.ui" line="55"/>
        <source>Grid Layer:</source>
        <translation>Reticolo:</translation>
    </message>
    <message>
        <location filename="ui_Freewat.ui" line="84"/>
        <source>Layer Name:</source>
        <translation>Nome del layer:</translation>
    </message>
    <message>
        <location filename="ui_Freewat.ui" line="131"/>
        <source>TOP:</source>
        <translation>TOP:</translation>
    </message>
    <message>
        <location filename="ui_Freewat.ui" line="150"/>
        <source>BOTTOM:</source>
        <translation>BOTTOM:</translation>
    </message>
    <message>
        <location filename="ui_Freewat.ui" line="14"/>
        <source>valueclass2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GWAllotDialog</name>
    <message>
        <location filename="ui_loadGWAllotments.ui" line="27"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_loadGWAllotments.ui" line="81"/>
        <source>Browse</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="ui_loadGWAllotments.ui" line="90"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_loadGWAllotments.ui" line="148"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_loadGWAllotments.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_loadGWAllotments.ui" line="64"/>
        <source>Load CSV file of groundwater allotments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadCimateDataDialog</name>
    <message>
        <location filename="ui_loadClimateData.ui" line="14"/>
        <source>Load Climate Data as CSV File</source>
        <translation>Carica dati climatici da file CSV</translation>
    </message>
    <message>
        <location filename="ui_loadClimateData.ui" line="46"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_loadClimateData.ui" line="61"/>
        <source>Load CSV file of climate data</source>
        <translation>Carica file CSV dei dati climatici</translation>
    </message>
    <message>
        <location filename="ui_loadClimateData.ui" line="199"/>
        <source>Browse</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="ui_loadClimateData.ui" line="31"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_loadClimateData.ui" line="71"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
</context>
<context>
    <name>LoadClimateDataDialog</name>
    <message>
        <location filename="loadClimateData_dialog.py" line="100"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="loadClimateData_dialog.py" line="100"/>
        <source>Yuo have already created a ClimateData Table 
 for model %s !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadCropCoefficientsDialog</name>
    <message>
        <location filename="loadCropCoefficients_dialog.py" line="87"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="loadCropCoefficients_dialog.py" line="87"/>
        <source>Yuo have already created a Crop Coefficients Table 
 for model %s !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadGWAllotmentsDialog</name>
    <message>
        <location filename="loadGWAllotments_dialog.py" line="87"/>
        <source>Warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="loadGWAllotments_dialog.py" line="87"/>
        <source>Yuo have already created a Table of GW Allotments
 for model %s !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadNRDeliveriesDialog</name>
    <message>
        <location filename="loadNRDeliveries_dialog.py" line="87"/>
        <source>Warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="loadNRDeliveries_dialog.py" line="87"/>
        <source>Yuo have already created a Table of External Deliveries
 for model %s !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadSWAllotmentsDialog</name>
    <message>
        <location filename="loadSWAllotments_dialog.py" line="87"/>
        <source>Warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="loadSWAllotments_dialog.py" line="87"/>
        <source>Yuo have already created a Table of SW Allotments
 for model %s !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MergeTool</name>
    <message>
        <location filename="ui_mergeTool.ui" line="27"/>
        <source>Merge Layers</source>
        <translation>Unisci Layer</translation>
    </message>
    <message>
        <location filename="ui_mergeTool.ui" line="39"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_mergeTool.ui" line="93"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_mergeTool.ui" line="14"/>
        <source>Merge SpatiaLite Layers</source>
        <translation>Unisci layer SpatiaLite</translation>
    </message>
    <message>
        <location filename="ui_mergeTool.ui" line="58"/>
        <source>Select all the Model Data Objects or tables you want to merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_mergeTool.ui" line="68"/>
        <source>Name of merged layer:</source>
        <translation>Nome del layer unito:</translation>
    </message>
    <message>
        <location filename="ui_mergeTool.ui" line="99"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The User must select from the list all the Model Data Objects or tables which are to be merged&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of merged layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the new Model Data Object or table obtained as a merge of the selected features&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to FREEWAT User Manual (Volume 1, section 6.5). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModelBuilderDialog</name>
    <message>
        <location filename="ui_modelBuilder.ui" line="35"/>
        <source>Run Model</source>
        <translation>Esegui Modello</translation>
    </message>
    <message>
        <location filename="ui_programLocations.ui" line="41"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="65"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Activate MODFLOW Packages&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attiva pacchetti MODFLOW&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="72"/>
        <source>Groundwater Flow </source>
        <translation>Flusso sotterraneeo</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="265"/>
        <source>UZF (Unsaturated Zone)</source>
        <translation>UZF (Zona insatura)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="295"/>
        <source>UZF Layer:</source>
        <translation>UZF Layer:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="282"/>
        <source>Surface Layer:</source>
        <translation>Surface Layer:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="346"/>
        <source>Recharge Option:</source>
        <translation>Opzioni ricarica:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="372"/>
        <source>Simulate Evapotranspiration</source>
        <translation>Simula evapotraspirazione</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="382"/>
        <source>Use SFR Package</source>
        <translation>Usa pacchetto SFR</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="402"/>
        <source>SFR2 (Stream Flow Routing)</source>
        <translation>SFR2 (Stream Flow Routing)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="419"/>
        <source>SFR  Layer:</source>
        <translation>SFR Layer:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="508"/>
        <source>SFR Table:</source>
        <translation>SFR Table:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="537"/>
        <source>Simulate Unsaturated Zone</source>
        <translation>Simula zona insatura</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="575"/>
        <source>Conversion Factor (CONSTANT)</source>
        <translation>Fattore di conversione (CONSTANT)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="451"/>
        <source>Weighting Factor (WEIGHT)</source>
        <translation>Fattore di peso (WEIGHT)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="600"/>
        <source>Tolerance Level (DLEAK)</source>
        <translation>Livello di tolleranza (DLEAK)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="476"/>
        <source>Num. of Sub-Time Steps (NUMTIM)</source>
        <translation>Numero di sotto Stress Period (NUMTIM)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="699"/>
        <source>Rch Option:</source>
        <translation>Opzioni RCH:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="735"/>
        <source>Evt Option:</source>
        <translation>Opzioni EVT:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="759"/>
        <source>Activate Link with MT3DMS (LMT Package) </source>
        <translation>Attiva collegamento con MT3DMS (pacchetto LMT) </translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="829"/>
        <source>OBSERVATIONS</source>
        <translation>OBSERVATIONS</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="963"/>
        <source>Rewetting Parameters</source>
        <translation>Parametri di rewetting</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1061"/>
        <source>PCG Solver parameters</source>
        <translation>Parametri risolutivi PCG</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2744"/>
        <source>Only Write Input Files</source>
        <translation>Scrivi solo gli input file</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1237"/>
        <source>Solute Transport</source>
        <translation>Trasporto di soluti</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1250"/>
        <source>Transport Model:</source>
        <translation>Modello di trasporto:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1293"/>
        <source>ADVECTION Package</source>
        <translation>Pacchetto ADVECTION </translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1306"/>
        <source>Solution Option (MIXELM):</source>
        <translation>Opzioni di soluzione (MIXELM): </translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1374"/>
        <source>Particle Tracking Algorithm:</source>
        <translation>Algoritmo di particle tracking:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1429"/>
        <source>Dispersion is Active</source>
        <translation>Dispersione è attiva</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1471"/>
        <source>Reaction Layer</source>
        <translation>Layer reazione</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1491"/>
        <source>Type of Sorption:</source>
        <translation>Tipo di adsorbimento:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1558"/>
        <source>Type of Kinetic Rate Reaction:</source>
        <translation>Tipo di tasso della reazione cinetica:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1624"/>
        <source>SINK and SOURCE Package</source>
        <translation>Pacchetto SINK e SOURCE</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1640"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1660"/>
        <source>WEL</source>
        <translation>WEL</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1680"/>
        <source>RIV</source>
        <translation>RIV</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1700"/>
        <source>GHB</source>
        <translation>GHB</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1724"/>
        <source>Distributed Sink/Source (RCH or EVT)</source>
        <translation>Termini di sorgente/estrazione distribuiti (RCH o EVT)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1741"/>
        <source>Mass Loading</source>
        <translation>Trasporto di massa</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1761"/>
        <source>Constant Conc.</source>
        <translation>Concentrazione costante</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1841"/>
        <source>Density Dependent Flow</source>
        <translation>Flusso a densità dipendente</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1860"/>
        <source>Method for Mass Conservation (MFNADVFD)</source>
        <translation>Metodo di conservazione della massa (MFNADVFD) </translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1895"/>
        <source>Reference Density:</source>
        <translation>Densità di riferimento:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1929"/>
        <source>Slope coefficient for EoS:</source>
        <translation>Pendenza per EoS:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1966"/>
        <source>Viscosity Dependent Flow</source>
        <translation>Flusso a viscosità dipendente</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1985"/>
        <source>Simulate effect of Temperature</source>
        <translation>Simula gli effetti della temperatura</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2005"/>
        <source>Species representing Temperature:</source>
        <translation>Specie rappresentative della temperatura:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2039"/>
        <source>Reference Viscosity:</source>
        <translation>Viscosità di referenza:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2076"/>
        <source>Slope Coefficient (DMUDC):</source>
        <translation>Coefficiente di pendenza (DMUDC): </translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2120"/>
        <source>GCG SolverParameters</source>
        <translation>Parametri risolutivi GCG</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2372"/>
        <source>Water Management and Crop Modeling</source>
        <translation>Gestion risorsa idrica</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2401"/>
        <source>Insert input for this module:</source>
        <translation>Inserisci input per questo modulo:</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2410"/>
        <source>Water Demand Units (Farm_ID)</source>
        <translation>Farm_ID</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2430"/>
        <source>Water Units Properties</source>
        <translation>Proprietà della farm</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2530"/>
        <source>Wells for a Water Unit (Farm Wells)</source>
        <translation>Pozzi della farm (Farm Wells)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2510"/>
        <source>Pipelines for a Water Unit</source>
        <translation>Condutture per la farm</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2550"/>
        <source>Soils properties</source>
        <translation>Proprietà dei suoli</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2570"/>
        <source>Crops and Soils for a Water Unit</source>
        <translation>Colutre e suoli per la farm</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2590"/>
        <source>Crops properties</source>
        <translation>Proprietà delle colture</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2787"/>
        <source>Model Calibration</source>
        <translation>Calibrazione del modello</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2902"/>
        <source>Output Options</source>
        <translation>Opzioni di output</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2956"/>
        <source>Printing of Observations, Simulated Values, Residuals</source>
        <translation>Stampa osservazioni, valori simulati, residui</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2964"/>
        <source>Starting parameters values (StartRes)</source>
        <translation>Valori iniziali dei parametri (StartRes)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2974"/>
        <source>Parameter-estimation iterations (Intermed Res)</source>
        <translation>Iterazioni per la stima dei parametri (Intermed Res)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2981"/>
        <source>Final parameter values (Final Res)</source>
        <translation>Valori finali dei parametri (Final Res)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2996"/>
        <source>Printing of Sensitivity Tables</source>
        <translation>Stampa tabella di sensitività</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3004"/>
        <source>Parameter-estimation iterations (IntermedSens)</source>
        <translation>Iterazioni per la stima dei parametri (IntermedSens)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3018"/>
        <source>Starting parameter values (StartSens)</source>
        <translation>Valori iniziali dei parametri (StartSens)</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2793"/>
        <source>Mode</source>
        <translation>Modalità</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2801"/>
        <source>Sensitivity analysis</source>
        <translation>Analisi di sensitività</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3179"/>
        <source>Parameter estimation settings</source>
        <translation>Impostazioni per la stima dei parametri</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3187"/>
        <source>Maximumnumber of parameter-estimation iterations (MaxIter):</source>
        <translation>Numero massimo di iterazioni per la stima dei parametri (MaxIter):</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3219"/>
        <source>Default parameter-change closure tolerance (TolPar):</source>
        <translation>Parametro di tolleranza sulla stima dei parametri (TolPar):</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3251"/>
        <source>Sum-of-squares closure tolerance (TolSOCS):</source>
        <translation>Parametro di tolleranza per il model fit (TolSOCS):</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3283"/>
        <source>Default maximum fractional parameter-value change(MaxChange):</source>
        <translation>Massima variazione del valore dei parametri (MaxChange):</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3081"/>
        <source>Observations</source>
        <translation>Osservazioni</translation>
    </message>
    <message>
        <location filename="ui_programLocations.ui" line="23"/>
        <source>Program Locations</source>
        <translation>Eseguibili dei programmi</translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="245"/>
        <source>LAK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="625"/>
        <source>Streamflow Tolerance (FLWTOL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="782"/>
        <source>Transport through Usaturated zone is not simulated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="799"/>
        <source>Transport through Unsaturated zone is simulated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1098"/>
        <source>Outer Iteration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1121"/>
        <source>Inner Iteration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1317"/>
        <source>Standard Finite-Difference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1322"/>
        <source>Forward-tracking MOC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1327"/>
        <source>Backward-tracking MMOC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1332"/>
        <source>Hybrid Method HMOC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1337"/>
        <source>Third-order TVD  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1348"/>
        <source>Courant number (PERCEL):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1406"/>
        <source>MXPART:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1787"/>
        <source>Unsat. Zone (UZT)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1875"/>
        <source>Central-in-space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="1880"/>
        <source>Upstream-weighted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2135"/>
        <source>Outer Interation (MXITER):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2161"/>
        <source>Inner Iteration (ITER1):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2187"/>
        <source>Preconditioner (ISOLVE):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2219"/>
        <source>Dispersion Tensor Term (NCRS):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2246"/>
        <source>Relaxation Factor (ACCL):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2272"/>
        <source>CCLOSE:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2298"/>
        <source>IPRGCG:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2386"/>
        <source>Warning: you need to create a MODFLOW Model before to run this module !!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2450"/>
        <source>Groundwater Allotments for Water Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2470"/>
        <source>Surface Water Allotments for Water Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2490"/>
        <source>External Deliveries (Non-routed Deliv.) for Water Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2610"/>
        <source>Root and Crop Coefficients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2630"/>
        <source>Surface Layer for ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2650"/>
        <source>Precipitation Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2670"/>
        <source>Deficiency Scenario Options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2718"/>
        <source>Run Crop Growth Module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2811"/>
        <source>Optimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2818"/>
        <source>Eigenvalues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2836"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2844"/>
        <source>Table of 3D parameters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2867"/>
        <source>Table of 2D time-variant parameters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2913"/>
        <source>Output verbosity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2921"/>
        <source>3 - Warnings, notes, selected input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2926"/>
        <source>0 - No extraneous output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2931"/>
        <source>1 - Warnings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2936"/>
        <source>2 - Warnings, notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2941"/>
        <source>4 - Warnings, notes, all input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="2946"/>
        <source>5 - All available output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3011"/>
        <source>Final parameter values (FinalSens)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3089"/>
        <source>Well Observation (HOB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3106"/>
        <source>Flow Observations Layer (RVOB):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3123"/>
        <source>Flow Observations Layer (GBOB):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3140"/>
        <source>Flow Observations Layer (DROB):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3157"/>
        <source>Flow Observations Layer (CHOB):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3317"/>
        <source>Sensitivity Method:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3325"/>
        <source>Forward Perturbation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3330"/>
        <source>Central Perturbation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_modelBuilder.ui" line="3338"/>
        <source>Use trust region modification of Gauss-Newton regression</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NRDelivDialog</name>
    <message>
        <location filename="ui_loadNRDeliveries.ui" line="27"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_loadNRDeliveries.ui" line="82"/>
        <source>Browse</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="ui_loadNRDeliveries.ui" line="91"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_loadNRDeliveries.ui" line="149"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_loadNRDeliveries.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_loadNRDeliveries.ui" line="64"/>
        <source>Load CSV file of external deliveries
(NON-ROUTED Deliveries)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Parameters2dArrayDialog</name>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="71"/>
        <source> Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="103"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="173"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="267"/>
        <source>Multiplier</source>
        <translation>Moltiplicatore</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="1543"/>
        <source>Load parameters from CSV</source>
        <translation>Carica parametri da CSV</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="1561"/>
        <source>CSV Parameters Table</source>
        <translation>File CSV dei parametri</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="1590"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="1603"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="1667"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="1740"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="26"/>
        <source>Define Parameters for 2D time-variant array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="42"/>
        <source>Define Parameters (2D Time-variant Array)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="146"/>
        <source>Enter parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="195"/>
        <source>Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="223"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="245"/>
        <source>StartValue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="283"/>
        <source>StressPeriod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="305"/>
        <source>Use Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="321"/>
        <source>ZoneLayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="337"/>
        <source>Zone ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="359"/>
        <source>Adjustable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="381"/>
        <source>Transform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="403"/>
        <source>Constraints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="425"/>
        <source>Upper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="447"/>
        <source>Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="469"/>
        <source>PerturbAmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_2DParametersLayer.ui" line="1755"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Zone Layers&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: MDO of Zones Array&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the 2D Parameters MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to  &lt;/span&gt;&lt;a href=&quot;http://igwmc.mines.edu/freeware/ucode/UCODE_2014_User_Manual-version02.pdf&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;UCODE_2014 User&apos;s Manual&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Parameters3dArrayDialog</name>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1517"/>
        <source>Load parameters from CSV</source>
        <translation>Carica parametri da CSV</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1535"/>
        <source>CSV Parameters Table</source>
        <translation>File CSV dei parametri</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1564"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="71"/>
        <source> Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="103"/>
        <source>Name of new layer:</source>
        <translation>Nome del nuovo layer:</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="173"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1710"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1577"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1641"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="26"/>
        <source>Define Parameters for 3D array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="57"/>
        <source>Define Parameters (3D Static Array)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="146"/>
        <source>Enter parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="195"/>
        <source>Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="223"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="245"/>
        <source>StartValue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="267"/>
        <source>Layer-based</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="283"/>
        <source>Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="305"/>
        <source>Use Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="327"/>
        <source>Zone Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="343"/>
        <source>Zone ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="365"/>
        <source>Adjustable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="387"/>
        <source>Transform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="409"/>
        <source>Constraints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="431"/>
        <source>Upper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="453"/>
        <source>Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="475"/>
        <source>PerturbAmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1080"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_3DParametersLayer.ui" line="1725"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Model Name&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Zone Layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: MDO of Zones Array&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: name of the 3D Parameters MDO which has to be created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to  &lt;/span&gt;&lt;a href=&quot;http://igwmc.mines.edu/freeware/ucode/UCODE_2014_User_Manual-version02.pdf&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;UCODE_2014 User&apos;s Manual&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProcessTs</name>
    <message>
        <location filename="processTs_dialog.py" line="331"/>
        <source>Sensor name</source>
        <translation>Nome sensore</translation>
    </message>
    <message>
        <location filename="processTs_dialog.py" line="331"/>
        <source>Enter a new sensor name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="processTs_dialog.py" line="450"/>
        <source>Select output file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="processTs_dialog.py" line="532"/>
        <source>Exception occurred: 
 {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="processTs_dialog.py" line="533"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QCoreApplication.self</name>
    <message>
        <location filename="Freewat.py" line="913"/>
        <source>Solute Transport Process</source>
        <translation>Processo di trasporto soluti</translation>
    </message>
</context>
<context>
    <name>QGridder</name>
    <message>
        <location filename="ui_createGrid.ui" line="26"/>
        <source>Create new grid</source>
        <translation>Crea nuovo reticolo</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="38"/>
        <source>Grid extent</source>
        <translation>Estensione reticolo</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="104"/>
        <source>Note : maximum values of X and Y will be adjusted to obtain exact resolution.</source>
        <translation>Nota: i valori massimi di X e Y verranno adattati per ottenere una risoluzione esatta.</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="123"/>
        <source>Update extents from layer</source>
        <translation>Aggiorna estensione dal layer</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="130"/>
        <source>Update extents from canvas</source>
        <translation>Aggiorna estensione dalla mappa</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="167"/>
        <source>Grid resolution (in map unit)</source>
        <translation>Risoluzione reticolo (in unità di mappa)</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="191"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="201"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="224"/>
        <source> 1:1 ratio</source>
        <translation>rapporto 1:1</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="285"/>
        <source>Output</source>
        <translation>Output</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="325"/>
        <source>Load layer after creation</source>
        <translation>Carica il reticolo nella legenda</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="235"/>
        <source>Estimated number of grid cells</source>
        <translation>Stima del numero di celle</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="299"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="306"/>
        <source>Grid Name</source>
        <translation>Nome reticolo</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="111"/>
        <source>Fetch extents from existing layer:</source>
        <translation>Aggiorna estensione da layer esistente:</translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="53"/>
        <source>Y Min:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="67"/>
        <source>X Min:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="81"/>
        <source>Y Max:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_createGrid.ui" line="88"/>
        <source>X Max:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGridderDialog</name>
    <message>
        <location filename="createGrid_dialog.py" line="367"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="367"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="244"/>
        <source>Gridder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="203"/>
        <source>Please specify valid extent coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="207"/>
        <source>Please specify valid output shapefile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="211"/>
        <source>Please specify valid resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="216"/>
        <source>Check extent coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="227"/>
        <source>Vector grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="227"/>
        <source>Invalid extent coordinates entered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="244"/>
        <source>Invalid extent or resolution entered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="createGrid_dialog.py" line="382"/>
        <source>Generate Grid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SWAllotDialog</name>
    <message>
        <location filename="ui_loadSWAllotments.ui" line="27"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_loadSWAllotments.ui" line="81"/>
        <source>Browse</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="ui_loadSWAllotments.ui" line="90"/>
        <source>Decimal separator</source>
        <translation>Separatore decimale</translation>
    </message>
    <message>
        <location filename="ui_loadSWAllotments.ui" line="148"/>
        <source>Column separator</source>
        <translation>Separatore colonne</translation>
    </message>
    <message>
        <location filename="ui_loadSWAllotments.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_loadSWAllotments.ui" line="64"/>
        <source>Load CSV file of surface water allotments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveSensorList</name>
    <message>
        <location filename="saveSensorList_dialog.py" line="90"/>
        <source>Sensor saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="saveSensorList_dialog.py" line="99"/>
        <source>Sensor name</source>
        <translation>Nome sensore</translation>
    </message>
    <message>
        <location filename="saveSensorList_dialog.py" line="99"/>
        <source>Enter a new sensor name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="saveSensorList_dialog.py" line="103"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="saveSensorList_dialog.py" line="103"/>
        <source>Do you want to save the sensor?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SensorCompare</name>
    <message>
        <location filename="sensorCompare_dialog.py" line="157"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorCompare_dialog.py" line="53"/>
        <source>No sensor found!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorCompare_dialog.py" line="96"/>
        <source>Sensor already added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorCompare_dialog.py" line="117"/>
        <source>Sensor has no data in selected interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorCompare_dialog.py" line="119"/>
        <source>Sensor has no data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SensorManager</name>
    <message>
        <location filename="sensorManager_dialog.py" line="76"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="76"/>
        <source>No sensor found!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="101"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="101"/>
        <source>Are you sure to delete sensor ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="113"/>
        <source>Select output file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="120"/>
        <source>Plese select a valid file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="367"/>
        <source>Save</source>
        <translation type="unfinished">Salva</translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="125"/>
        <source>Sensor saved to CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="345"/>
        <source>Please select a sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="364"/>
        <source>Exception occour: {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="367"/>
        <source>Sensor saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sensorManager_dialog.py" line="388"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowPlot_Dialog</name>
    <message>
        <location filename="ui_viewCalibrationPlots.ui" line="23"/>
        <source>Plots of Calibration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCalibrationPlots.ui" line="39"/>
        <source>ModelFit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCalibrationPlots.ui" line="44"/>
        <source>Sensitivity Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCalibrationPlots.ui" line="49"/>
        <source>Parameter Estimation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCalibrationPlots.ui" line="67"/>
        <source>Show observation labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCalibrationPlots.ui" line="671"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCalibrationPlots.ui" line="628"/>
        <source>Relative to initial value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimplePlotCalibration</name>
    <message>
        <location filename="SimplePlotCalibration.py" line="183"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SimplePlotCalibration.py" line="107"/>
        <source>No HOB file in your working directory found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SimplePlotCalibration.py" line="138"/>
        <source>File already exists!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SimplePlotCalibration.py" line="165"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SimplePlotCalibration.py" line="165"/>
        <source>{}
{}
{}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SimplePlotCalibration.py" line="183"/>
        <source>No OBH file found! Be sure that you run the Modflow simulation with Observation package active</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateWorkingDirectory</name>
    <message>
        <location filename="updateWd.py" line="103"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updateWd.py" line="103"/>
        <source>Your Working directory path has been updated</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>addStressPeriodDialog</name>
    <message>
        <location filename="ui_addStressPeriod.ui" line="59"/>
        <source>You have already defined</source>
        <translation>Hai già definito</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="91"/>
        <source>Stress Periods</source>
        <translation>Stress Periods</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="104"/>
        <source>Add parameters</source>
        <translation>Aggiungi parametri</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="237"/>
        <source>Browse...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="112"/>
        <source>Multiplier:</source>
        <translation>Moltiplicatore:</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="148"/>
        <source>SP Length:</source>
        <translation>Lunghezza SP:</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="158"/>
        <source>Time Steps:</source>
        <translation>Time Steps:</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="168"/>
        <source>State:</source>
        <translation>Stato:</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="213"/>
        <source>Load parameters from CSV</source>
        <translation>Carica parametri da CSV</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="223"/>
        <source>Load table from CSV layer:</source>
        <translation>Carica tabella da CSV:</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="273"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="27"/>
        <source>Add Stress Periods</source>
        <translation>Aggiungi Stress Period</translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="35"/>
        <source>Time Table:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_addStressPeriod.ui" line="279"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Time Table&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the time table created along with the hydrological model&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The number of Stress Periods already defined is indicated&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Add parameters&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; tab:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SP Length&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: length of the current Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;to be expressed in time units&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Time Steps&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of Time Steps within the current Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Multiplier&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: multiplier to calculate the length of Time Steps within the current Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;State&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: option needed to define if the current Stress Period will be run in &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Steady State&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Transient &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;conditions&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The parameters listed above can be also loaded from a csv file, through the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Browse &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;button in the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Load parameters from CSV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; tab&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In the last case, the parameters to be assigned are the following:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;length&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;:  length of each Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;to be expressed in time units&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;time_steps&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: number of Time Steps within each Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;multiplier&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: multiplier to calculate the length of Time Steps within each Stress Period (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;state&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: option needed to define if each Stress Period will be run in &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Steady State&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-style:italic;&quot;&gt;Transient &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;conditions&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to FREEWAT User Manual (Volume 1, section 4.2). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>akvaGIS</name>
    <message>
        <location filename="Freewat.py" line="780"/>
        <source>Data-Preprocessing (akvaGIS)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>cloneSensor</name>
    <message>
        <location filename="cloneSensor.ui" line="14"/>
        <source>Clone sensor</source>
        <translation>Clona sensore</translation>
    </message>
    <message>
        <location filename="cloneSensor.ui" line="37"/>
        <source>New sensor name</source>
        <translation>Nuovo nome sensore</translation>
    </message>
    <message>
        <location filename="cloneSensor.ui" line="49"/>
        <source>Clear data (not working)</source>
        <translation>Rimuovi dati (non funziona)</translation>
    </message>
    <message>
        <location filename="cloneSensor.ui" line="64"/>
        <source>short</source>
        <translation>accorcia</translation>
    </message>
    <message>
        <location filename="cloneSensor.ui" line="88"/>
        <source>begin date</source>
        <translation>data inizio</translation>
    </message>
    <message>
        <location filename="cloneSensor.ui" line="116"/>
        <source>yyyy-MM-dd hh:mm:ss</source>
        <translation>yyyy-MM-dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="cloneSensor.ui" line="109"/>
        <source>end date</source>
        <translation>data fine</translation>
    </message>
    <message>
        <location filename="cloneSensor.ui" line="127"/>
        <source>Timezone</source>
        <translation>Fuso orario</translation>
    </message>
</context>
<context>
    <name>createCropDialog</name>
    <message>
        <location filename="crop_dialog.py" line="322"/>
        <source>Warning!</source>
        <translation type="unfinished">Attenzione!</translation>
    </message>
    <message>
        <location filename="crop_dialog.py" line="245"/>
        <source>Check the input date and the intial date of the model in the modeltable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="crop_dialog.py" line="251"/>
        <source>Harvest time has to be AFTER seed time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="crop_dialog.py" line="322"/>
        <source>Some cell of the table is empty! Fill it before running the object!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>createFarmCropSoilDialog</name>
    <message>
        <location filename="createFarmCrop_dialog.py" line="104"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createFarmCrop_dialog.py" line="104"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>createFarmIdDialog</name>
    <message>
        <location filename="createFarmId_dialog.py" line="104"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createFarmId_dialog.py" line="104"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>createFarmPipelineDialog</name>
    <message>
        <location filename="createFarmPipeline_dialog.py" line="105"/>
        <source>Warning!</source>
        <translation>Attenzione!</translation>
    </message>
    <message>
        <location filename="createFarmPipeline_dialog.py" line="105"/>
        <source>Table %s already exists!</source>
        <translation>Tabella %s già esistente!</translation>
    </message>
</context>
<context>
    <name>createLAKLayer</name>
    <message>
        <location filename="ui_createLAKLayer.ui" line="20"/>
        <source>create LAK layer</source>
        <translation>Crea Layer LAK</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="36"/>
        <source>LAK params</source>
        <translation>parametri LAK</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="101"/>
        <source>Model name</source>
        <translation>Nome del modello</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="124"/>
        <source>Create LAK layers</source>
        <translation>Crea Layer LAK</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="135"/>
        <source>THETA</source>
        <translation>THETA</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="145"/>
        <source>Solution method options: 0 = explicit, 0.0 &lt; x &lt; 1.0 = semi-explicit, 1 = implicit</source>
        <translation>Opzioni: 0 = 0esplicito, 0.0 &lt; x &lt; 1.0 = semi esplicito, 1 = implicito</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="164"/>
        <source>NSSITR</source>
        <translation>NSSITR</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="174"/>
        <source>Maximum number of iteration for Newton&apos;s method</source>
        <translation>Numero massimo di iterazioni per il metodo di Newton</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="184"/>
        <source>SSCNCR</source>
        <translation>SSCNCR</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="194"/>
        <source>Convergence criterion for Newton&apos;s method</source>
        <translation>Criterio di convergenza per il metodo di Newton</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="204"/>
        <source>update solver</source>
        <translation>Aggiorna solver</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="222"/>
        <source>Lake params</source>
        <translation>Parametri lago</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="566"/>
        <source>SURFDEP</source>
        <translation>SURFDEP</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="241"/>
        <source>Height variations in lake-bottom elevations that affect groundwater discharge (succesfully used for values 0.01 - 0.5)</source>
        <translation>Variazione dell&apos;altezza del fondo del lago che influisce sullo scarico nelle acque sotterranee (utilizzato con successo valori tra 0.01 - 0.5)</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="571"/>
        <source>STAGES</source>
        <translation>STAGES</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="261"/>
        <source>The initial stage of each lake at the beginning of the run</source>
        <translation>La fase iniziale per ogni lago, all&apos;inizio di ogni esecuzione</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="576"/>
        <source>SSMN</source>
        <translation>SSMN</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="300"/>
        <source>Minimum stage allowed for each lake in steady-state solution. Omit for transient</source>
        <translation>Fase di minimo consentito per ogni lago in soluzione steady state. Omettere per transitorio</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="581"/>
        <source>SSMX</source>
        <translation>SSMX</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="330"/>
        <source>Maximum stage allowed for each lake in steady-state solution. Omit for transient</source>
        <translation>Fase di massimo consentito per ogni lago in soluzione steady state. Omettere per transitorio</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="343"/>
        <source>leakance</source>
        <translation>leakance</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="375"/>
        <source>lake id</source>
        <translation>id lago</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="405"/>
        <source>Load params from csv:</source>
        <translation>Carica parametri da csv:</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="412"/>
        <source>delimiter</source>
        <translation>separatore</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="422"/>
        <source>Open CSV</source>
        <translation>Apri CSV</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="438"/>
        <source>SP</source>
        <translation>SP</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="443"/>
        <source>PRCPLK</source>
        <translation>PRCPLK</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="448"/>
        <source>EVAPLK</source>
        <translation>EVAPLK</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="453"/>
        <source>RNF</source>
        <translation>RNF</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="458"/>
        <source>WTHDRW</source>
        <translation>WTHDRW</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="471"/>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="478"/>
        <source>Delete selected</source>
        <translation>Elimina selezionato</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="498"/>
        <source>Edit selected</source>
        <translation>Modifica selezionato</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="508"/>
        <source>save</source>
        <translation>salva</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="515"/>
        <source>cancel</source>
        <translation>cancella</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="561"/>
        <source>LAKE_ID</source>
        <translation>LAKE_ID</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="586"/>
        <source>LEAKANCE</source>
        <translation>LEAKANCE</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="601"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_createLAKLayer.ui" line="613"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The Lake solver properties options apply to all the lakes within one MODFLOW simulation.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;THETA&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Theta determines whether the solution for the lake package is solved implicitly (Theta*=*1) or semi-implicitly (0.5*&amp;lt;=*Theta*&amp;lt;*1) &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;NSSITR&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: is the maximum number of iterations for Newton’s method solution for equilibrium lake stages in each MODFLOW iteration for steady-state aquifer head solution&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SSCNCR&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: line shapefile containing the profile of the river segment previously created&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: is the convergence criterion for equilibrium lake stage solution by Newton’s method&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The Lake properties are assigned to each lake individually. For each lake that is to be created, the Lake properties must be filled, with a unique non-zero integer for lake id&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SURFDEP&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: lake bottom elevation undulations. Can be used to smooth the solution for the rewetting of the lake bottom. Values between 0.01 and 0.5 are suggested (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;STAGES&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: initial Stage of each lake at the start of the run (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SSMN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: minimum lake stage for steady-state simulations (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SSMX&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: maximum lake stage for steady-state simulations (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;leakance&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: leakance term representing the lakebed sediments. Values are assumed to represent the combined leakances of the lakebed material and the aquifer material between the lake and the centers of the underlying grid cells (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;lake id&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: lake identifier for determining location, sub-lakes and stream flow routing, unique non-zero value (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The Lake properties frame reads the number of stress periods from the model timetable and displays as many rows. The table is to be filled with precipitation, evaporation, runoff and withdrawal information for the current lake, i.e. for the lake with the currently specified lake id in the Lake properties.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;PRCPLK:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; precipitation flux [L/T]&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;EVAPLK:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; evaporation flux [L/T]&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;RNF:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; surface runoff [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T]&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;WTHDRW:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; direct withdrawal from the lake [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T]&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Refer to &lt;/span&gt;&lt;a href=&quot;http://water.usgs.gov/nrp/gwsoftware/modflow2000/MFDOC/index.html?lak.htm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;MODFLOW LAK Package&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; for further information on these input options and parameters.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Once the Lake properties frame has been filled out for one lake, this lake is created through the Add button.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The lake is then added to the Lake selection frame.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;When the Add button is pressed, the Lake properties frame is reset and, if needed, additional lakes can be added. The created lake is added to the Lake selection frame. This frame displays a summary of the time-constant properties of each created lake.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Once one or more lakes have been created, they can be deleted by selecting the relevant row in the Lake selection frame and using the Delete Selected button or edited and clicking Edit Selected. Edit selected will re-load the properties of the selected lake into the Lake properties frame, where they can be altered. Any changes can be applied using the Save button or discarded using Cancel. When editing a layer, the Add and Delete selected fields are locked. To use these funtions, the editing must be concluded, by saving or cancelling.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;

&lt;head&gt;
    &lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;
    &lt;style type=&quot;text/css&quot;&gt;
        p,
        li {
            white-space: pre-wrap;
        }
    &lt;/style&gt;
&lt;/head&gt;

&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Guida&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Le proprietà del lago si applicano a tutti i laghi all&apos;interno di una simulazione MODFLOW&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;THETA&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Theta determina se la soluzione viene risolta implicitamente (Theta*=*1) o semi-implicitamente (0.5*&amp;lt;=*Theta*&amp;lt;*1) &lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;NSSITR&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: è il numero massimo di iterazioni per il metodo di Newton per stadi di equilibrio in ogni iterazione&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SSCNCR&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: é il criterio di convergenza per l&apos;equilibrio del lago con il metodo di Newton&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Name of new layer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: &lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Le proprietà del lago sono assegante singolarmente. Per ogni lago creato bisogna definire tutti i parametri, compreso l&apos; id del lago, che deve essere diverso da 0&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SURFDEP&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: ondulazioni elevazione fondo del lago. Può essere usato per migliorare la soluzione per la riumidificazione del fondo del lago. Valori suggeriti tra 0,01 e 0,5.(&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;STAGES&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: fase iniziale del lago per ogni esecuzione (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SSMN&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: fase minima del lago per le simulazioni di stato stazionario (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SSMX&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: fase massima del lago per le simulazioni di stato stazionario (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;leakance&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: termine che rapresenta i sedimenti del fondo del lago. Il valore rappresenta la leakances combinata dei materiali del dondo del lago e del materiale dell&apos;acquifero. (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;lake id&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: identificatore del lago per determinare la posizione, sotto laghi e fiumi associati. Valore univoco diverso da zero (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The Lake properties frame reads the number of stress periods from the model timetable and displays as many rows. The table is to be filled with precipitation, evaporation, runoff and withdrawal information for the current lake, i.e. for the lake with the currently specified lake id in the Lake properties.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;PRCPLK:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; precipitazioni [L/T]&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;EVAPLK:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; evaporazione [L/T]&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;RNF:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; deflusso superficie [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T]&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;WTHDRW:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; prelievo diretto dal lago [L&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;/T]&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Fare riferimento a &lt;/span&gt;&lt;a href=&quot;http://water.usgs.gov/nrp/gwsoftware/modflow2000/MFDOC/index.html?lak.htm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;MODFLOW LAK Package&lt;/span&gt;&lt;/a&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; per maggiori informazioni su questi parametri.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Una volta riempiti i parametri del lago, esso viene creato con il bottone Aggiungi.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Ora il nuovo lago è aggiunto.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Quando viene premuto il pulsante Aggiungi,le proprietà del lago vengono resettate ed è possibile aggiungere nuovi laghi. IL frame inferiore mostra un riassunto delle proprietà costanti dei laghi creati&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Quando uno o più laghi sono stati creati, essi possino essere eliminati selezionando la relativa riga nella tabella e utilizzando il bottone Elimina. Oppure è possibile modificare i parametri con il pulsante Modifica selezionato. Modificando un lago verranno ricaricati i parametri del lago , che possono essere modificati. Tutte le modifiche possono essere salvate con il bottone Salva oppure annullate con il pulsante Cancella.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;
</translation>
    </message>
</context>
<context>
    <name>createSensor</name>
    <message>
        <location filename="createSensor.ui" line="20"/>
        <source>Add new sensor</source>
        <translation>Aggiungi nuovo sensore</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="43"/>
        <source>Add sensor</source>
        <translation>Aggiungi sensore</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="99"/>
        <source>Input data</source>
        <translation>Sorgente dati</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="121"/>
        <source>CSV</source>
        <translation>CSV</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="131"/>
        <source>istSOS</source>
        <translation>istSOS</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="144"/>
        <source>Raw</source>
        <translation>Raw</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="154"/>
        <source>listfile</source>
        <translation>listfile</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="164"/>
        <source>hobfile</source>
        <translation>hobfile</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="174"/>
        <source>gagefile</source>
        <translation>gagefile</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="222"/>
        <source>Input file</source>
        <translation>File input</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="229"/>
        <source>Separator</source>
        <translation>Separatore</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="236"/>
        <source>Value column</source>
        <translation>Colonna valori</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="482"/>
        <source>list of column numbers to be used to parse the times of observations e.g. [0,1]</source>
        <translation>lista dei numeri di colonne che sono usate per identificare la data dell&apos;osservazione es. [0,1]</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="246"/>
        <source>Date columns</source>
        <translation>Colonne data</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="253"/>
        <source>no data values</source>
        <translation>valori mancanti</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1601"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="328"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="341"/>
        <source>Comma</source>
        <translation>Comma</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="354"/>
        <source>Semicolon</source>
        <translation>Semicolon</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="364"/>
        <source>Space</source>
        <translation>Space</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="391"/>
        <source>Number of row to skip</source>
        <translation>Numero di righe da ignorare</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="384"/>
        <source>Skiprow</source>
        <translation>Righe da saltare</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="418"/>
        <source>the column number containing the observations values e.g. 2</source>
        <translation>il numero della colonna contenente il valore dell&apos;osservazione es. 2</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="445"/>
        <source>Quality column number: -1 don&apos;t use quality column</source>
        <translation>Numero colonna qualità: -1 non usa la colonna qualità</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="448"/>
        <source>Quality column</source>
        <translation>Colonna qualità</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="455"/>
        <source>the column number containing the quality index, -1 don&apos;t use quality column</source>
        <translation>numero colonna qualità: -1 non usa la colonna qualità</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="489"/>
        <source>Day came before of month?</source>
        <translation>Giorni vengono prima dei mesi?</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="492"/>
        <source>Day first</source>
        <translation>Prima i giorni</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="506"/>
        <source>Date Format</source>
        <translation>Formato data</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="534"/>
        <source>List of values to be associated with no data value</source>
        <translation>Lista di valori da associare a valori mancanti</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="571"/>
        <source> A character indicating a comment line not to be imported</source>
        <translation>Un carattere che indica un commento o una riga da ignorare</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="564"/>
        <source>comment</source>
        <translation>commento</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="578"/>
        <source>Preview</source>
        <translation>Anteprima</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="620"/>
        <source>SOS server</source>
        <translation>Server SOS</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="636"/>
        <source>Sensor</source>
        <translation>Sensore</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="669"/>
        <source>define a custom enevt time, if False load all the available observation</source>
        <translation>definisci un event time, se Falso carica tutte le osservazioni disponibili</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="672"/>
        <source>Interval</source>
        <translation>Intervallo</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="733"/>
        <source>Connect</source>
        <translation>Connetti</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="740"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="747"/>
        <source>New</source>
        <translation>Nuovo</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="754"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1890"/>
        <source>Observed property</source>
        <translation>Proprietà osservata</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1152"/>
        <source>Frequency</source>
        <translation>Frequenza</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="821"/>
        <source>10Min</source>
        <translation>10Min</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="826"/>
        <source>20Min</source>
        <translation>20Min</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="831"/>
        <source>30Min</source>
        <translation>30Min</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="836"/>
        <source>1H</source>
        <translation>1H</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="841"/>
        <source>New Item</source>
        <translation>Nuovo oggetto</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="896"/>
        <source>Begin</source>
        <translation>Inizio</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="903"/>
        <source>Date and time of the begin position</source>
        <translation>Data e orario della posizione iniziale</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1612"/>
        <source>yyyy-MM-dd hh:mm:ss</source>
        <translation>yyyy-MM-dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="913"/>
        <source>End</source>
        <translation>Fine</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="930"/>
        <source>Date and time of the end position</source>
        <translation>Data e ora della posizione finale</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1619"/>
        <source>Timezone</source>
        <translation>Fuso orario</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="975"/>
        <source>Aggregate function</source>
        <translation>Funzione aggregazione</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="982"/>
        <source>aggregate function</source>
        <translation>funzione aggregazione</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="991"/>
        <source>AVG</source>
        <translation>AVG</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="996"/>
        <source>MAX</source>
        <translation>MAX</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1001"/>
        <source>MIN</source>
        <translation>MIN</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1006"/>
        <source>SUM</source>
        <translation>SUM</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1024"/>
        <source>aggregate interval, expressed in iso 8601 duration e.g. &quot;P1DT12H&quot;</source>
        <translation>intervallo aggregazione, espresso nel formato iso8601 duration es. &quot;PT1DT12H&quot;</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1017"/>
        <source>Aggregate interval</source>
        <translation>Intervallo aggregazione</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1053"/>
        <source>Begin position</source>
        <translation>Posizione iniziale</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1084"/>
        <source>End position</source>
        <translation>Posizione finale</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1175"/>
        <source>Edit data</source>
        <translation>Modifica dati</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1202"/>
        <source>Listing file path</source>
        <translation>Path file listing</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1209"/>
        <source>StartDate</source>
        <translation>Data iniziale</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1216"/>
        <source>Property</source>
        <translation>Proprietà</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1241"/>
        <source>load file</source>
        <translation>carica file</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1276"/>
        <source>Cumulative</source>
        <translation>Cumulativa</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1288"/>
        <source>STORAGE</source>
        <translation>STORAGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1293"/>
        <source>CONSTANT HEAD</source>
        <translation>CONSTANT HEAD</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1298"/>
        <source>WELLS</source>
        <translation>WELLS</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1303"/>
        <source>RIVER LEAKAGE</source>
        <translation>RIVER LEAKAGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1308"/>
        <source>TOTAL</source>
        <translation>TOTAL</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1313"/>
        <source>STREAM LEAKAGE</source>
        <translation>STREAM LEAKAGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1318"/>
        <source>HEAD DEP BOUNDS</source>
        <translation>HEAD DEP BOUNDS</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1323"/>
        <source>RECHARGE</source>
        <translation>RECHARGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1328"/>
        <source>ET SEGMENTS</source>
        <translation>ET SEGMENTS</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1333"/>
        <source>DRAIN (DRT)</source>
        <translation>DRAIN (DRT)</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1338"/>
        <source>SPECIFIED FLOWS</source>
        <translation>SPECIFIED FLOWS</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1677"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1348"/>
        <source>LAKE SEEPAGE</source>
        <translation>LAKE SEEPAGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1353"/>
        <source>MNW</source>
        <translation>MNW</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1358"/>
        <source>RESERV. LEAKAGE</source>
        <translation>RESERV. LEAKAGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1363"/>
        <source>UZF RECHARGE</source>
        <translation>UZF RECHARGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1368"/>
        <source>GW ET</source>
        <translation>GW ET</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1373"/>
        <source>SURFACE LEAKAGE</source>
        <translation>SURFACE LEAKAGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1394"/>
        <source>inout</source>
        <translation>inout</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1402"/>
        <source>IN</source>
        <translation>IN</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1407"/>
        <source>OUT</source>
        <translation>OUT</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1428"/>
        <source>Hob in file</source>
        <translation>FIle hob in</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1575"/>
        <source>Start date</source>
        <translation>Data iniziale</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1442"/>
        <source>Disc file</source>
        <translation>File disc</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1449"/>
        <source>Hob out file</source>
        <translation>File hob_out</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1479"/>
        <source>Hobname</source>
        <translation>nome osservazione</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1545"/>
        <source>stat</source>
        <translation>stat</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1568"/>
        <source>GAGE file path</source>
        <translation>File GAGE</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1582"/>
        <source>property</source>
        <translation>Proprietà</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1647"/>
        <source>stage</source>
        <translation>stage</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1652"/>
        <source>flow</source>
        <translation>flow</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1657"/>
        <source>depth</source>
        <translation>depth</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1662"/>
        <source>width</source>
        <translation>width</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1667"/>
        <source>midpt-flow</source>
        <translation>midpt-flow</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1672"/>
        <source>precip.</source>
        <translation>precip.</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1682"/>
        <source>runoff</source>
        <translation>runoff</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1687"/>
        <source>Stage(H)</source>
        <translation>Stage(H)</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1692"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1697"/>
        <source>Vol.Change</source>
        <translation>Vol.Change</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1702"/>
        <source>Precip.</source>
        <translation>Precip.</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1707"/>
        <source>Evap.</source>
        <translation>Evap.</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1712"/>
        <source>Runoff</source>
        <translation>Runoff</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1717"/>
        <source>GW-Inflw</source>
        <translation>GW-Inflw</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1722"/>
        <source>GW-outflw</source>
        <translation>GW-outflw</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1727"/>
        <source>SW-Inflw</source>
        <translation>SW-Inflw</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1732"/>
        <source>SW-Outflow</source>
        <translation>SW-Outflow</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1737"/>
        <source>Withdrawal</source>
        <translation>Withdrawal</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1742"/>
        <source>Lake-Inflx</source>
        <translation>Lake-Inflx</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1747"/>
        <source>Total-Cond</source>
        <translation>Total-Cond</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1752"/>
        <source>Percent-Err</source>
        <translation>Percent-Err</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1835"/>
        <source>Sensor name</source>
        <translation>Nome sensore</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1858"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="1880"/>
        <source>Longitude</source>
        <translation>Longitudine</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2015"/>
        <source>Latitude</source>
        <translation>Latitudine</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2041"/>
        <source>Altitude</source>
        <translation>Altitudine</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2097"/>
        <source>air-temperature</source>
        <translation>air-temperature</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2102"/>
        <source>air-humidity</source>
        <translation>air-humidity</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2107"/>
        <source>air-pressure</source>
        <translation>air-pressure</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2112"/>
        <source>air-rainfall</source>
        <translation>air-rainfall</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2117"/>
        <source>air-radiation</source>
        <translation>air-radiation</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2122"/>
        <source>water-height</source>
        <translation>water-height</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2127"/>
        <source>water-discharge</source>
        <translation>water-discharge</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2132"/>
        <source>water-conductivity</source>
        <translation>water-conductivity</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2137"/>
        <source>water-tension</source>
        <translation>water-tension</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2158"/>
        <source>unit of measure</source>
        <translation>Unità di misura</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2187"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2192"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2197"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2202"/>
        <source>m3/s</source>
        <translation>m3/s</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2207"/>
        <source>mbar</source>
        <translation>mbar</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2212"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2217"/>
        <source>mS/cm</source>
        <translation>mS/cm</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2222"/>
        <source>W/m2</source>
        <translation>W/m2</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2237"/>
        <source>use</source>
        <translation>usa</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2247"/>
        <source>Stat flag</source>
        <translation>Stat flag</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2260"/>
        <source>VAR</source>
        <translation>VAR</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2265"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2270"/>
        <source>CV</source>
        <translation>CV</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2275"/>
        <source>WT</source>
        <translation>WT</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2280"/>
        <source>SQRWT</source>
        <translation>WT</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2288"/>
        <source>top/bottom screen</source>
        <translation>top/bottom screen</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2399"/>
        <source>date</source>
        <translation>data</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2404"/>
        <source>data</source>
        <translation>valori</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2409"/>
        <source>quality</source>
        <translation>qualità</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2419"/>
        <source>Add new row</source>
        <translation>Aggiungi nuova riga</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2426"/>
        <source>Delete selected</source>
        <translation>Elimina selezionati</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2433"/>
        <source>Clear all</source>
        <translation>Elimina tutto</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2450"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="createSensor.ui" line="2456"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;With this interface it&apos;s possible to upload data from multiple sources:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;CSV:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; Comma separated values file&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;istSOS: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;an implementation of the Sensor Observation Service (SOS) standard from the Istituto scienze della Terra&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Raw: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Manually enter the values of a time series&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hobfile: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW standard head observation output file, see &lt;/span&gt;&lt;a href=&quot;http://water.usgs.gov/nrp/gwsoftware/modflow2000/MFDOC/index.html?hob.htm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;MODFLOW HOB&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;listfile: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW listing file; here MODFLOW writes detailed run records including head and overall budget&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;gagefile: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW gage file, see &lt;/span&gt;&lt;a href=&quot;http://water.usgs.gov/nrp/gwsoftware/modflow2000/MFDOC/index.html?gage.htm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;MODFLOW GAGE&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Sensor metadata&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sensor name&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: MANDATORY - This is the name of the point of the measure (unique within a FREEWAT model) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Description&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPTIONAL - A textual description of the sensor (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Latitude&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: MANDATORY - The latitude in WGS84 (EPSG:4326) of the sensor (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Longiture&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: MANDTORY - The longiude in WGS84 (EPSG:4326) of the sensor (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Altitude&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPTIONAL - The altitude in m a.s.l. of the sensor (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Observed property&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: MANDATORY - The physical property that is measured (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Unit of measures&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: MANDATORY - The unit of measure of the observation (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use option&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPTIONAL - Used for MODFLOW observations, assigns the Sensor with a use field which can be used to indicates the include in future operations (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Stat flag&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPTIONAL: Used in MODFLOW obseravations, the type of statistic used to determine the weigth of the obseravation (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Top/Bottom&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPTIONAL - Screen used for MODFLOW obseravtions, the used to define the screening of weels (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;  &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The button at the end of the coordinates line allows for picking-up 2D coordinates from the QGIS map frame. Coordinates are automatically converted and stored in WGS84 whatever coordinates system is in use on your QGIS map frame.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;CSV&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Input file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: path to the csv file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: value separator (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;checkbox&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Skiprow&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Number of row to skip, to remove the header (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Value column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Index of the data column, starting from 0 (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Quality column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Index of the quality column, starting from 0, -1 means no quality column (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Date format&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Dropdown with some predefined timeformat, it can be manually entered according to the strftime format (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;No data value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;comment&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: character that is used inside csv to start a comment line (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In the lower frame a preview of the raw CSV file is shown. This can be used to correctly assign columns. When the Preview button is pressed a preview of the data as read with the set options is visualized in the lower row of the lower frame&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;IstSOS&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;When you select the istSOS option as input data you have to connect to an istSOS server (similarly to database connection in QGIS you have to create a new one, or select an existing and connect). Servers can be selected from the SOS server drop-down menu. &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If no server is available from the drop-down menu, a new connection can be established the the New button by specifying the server URL. Password protected URLs can also be accessed. Once connected, the Sensor, Observed Property and Frequency drop-down menu show the available options available from the selected server.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;When loading data from an istSOS server, the metadata frame is filled automatically with metadata from the server after the Apply button is pressed.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SOS server&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: SOS server name (string)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sensor&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: name of the sensor (string)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Observed property&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: sensor observed property to load (string)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Frequency&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Frequency of the measures (string)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Interval&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: if checked allow to upload data within a selectable time interval set with a &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Begin&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;End&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; date. It&apos;s also possible to select an &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Aggregate Option &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;with an &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Aggregate interval &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;(&lt;/span&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/ISO_8601#Durations&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;ISO:8601 duration&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;RAW&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The RAW input method can be used to enter data manually through the OAT.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Begin position&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Date of the first measure (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;End position&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Date of the last measure (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Timezone&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Timezone of the measures (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Frequency&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Dropdown with some predefined frequency (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;List file&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;It&apos;s possible to create a time-series from a MODFLOW simulation throught the MODFLOW listing file.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Listing file path&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: path  to the listing file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Starting date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: starting date of the simulation (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Timezone&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: timezone of the simulation (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Property&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: dropdwn containing MODFLOW packages (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Input&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Selection of flow into or outo the model (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Hob file&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The simulated heads of a MODLFOW model can be loaded as a Sensor to create a new time-series. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Hob in file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the MODLFOW head observation file path (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Start date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The starting date of the MODFLOW model (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Timezone&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The timezone modifier for starting date (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Disc file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The MODFLOW head result file path (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Hob out file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The MODFLOW head result file path (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Hobname&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The prefix name of the chosen observation point used in the head observation input file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Gage file&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The MODFLOW gage file may contain model results for surface water bodies&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;GAGE file path&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The path of the location GAGE file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Start date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: THe starting date of the MODFLOW model (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Timezone&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The timezone of the starting date (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Property&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Gage file property to be loaded (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;

&lt;head&gt;
    &lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;
    &lt;style type=&quot;text/css&quot;&gt;
        p,
        li {
            white-space: pre-wrap;
        }
    &lt;/style&gt;
&lt;/head&gt;

&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Guida&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Con questa interfaccia é possibile caricare dati da diverse fonti:&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;CSV:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; Comma separated values file&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;istSOS: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;un implementazione dello standard Sensor Observation Service (SOS) sviluppato dall&apos; Istituto Scienze della Terra &lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Raw: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; Inserisci manualmente i valori di una serie temporale&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;hobfile: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW standard head observation output file, vedi &lt;/span&gt;&lt;a href=&quot;http://water.usgs.gov/nrp/gwsoftware/modflow2000/MFDOC/index.html?hob.htm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;MODFLOW HOB&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;listfile: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW listing file;  qui MODFLOW scrive i risultati dettagliati delle esecuzioni del modello, incluso head e overall budget&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;gagefile: &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;MODFLOW gage file, vedi &lt;/span&gt;&lt;a href=&quot;http://water.usgs.gov/nrp/gwsoftware/modflow2000/MFDOC/index.html?gage.htm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;MODFLOW GAGE&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Metadati sensore&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Nome sensore&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OBBLIGATORIO - Questo é il nome del punto di misura (univoco all&apos;interno del modello FREEWAT) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Descrizione&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: FACOLTATIVO - Una descrizione testuale del sensore(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Latitude&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OBBLIGATORIO - La latitudine in WGS84 (EPSG:4326) del sensore (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Longiture&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OBBLIGATORIO - La longitudine in WGS84 (EPSG:4326) del sensore (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Altitudine&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPZIONALE - L&apos;altitudine (m.s.l.m.) del sensore (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Proprietà osservata&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OBBLIGATORIO - La proprietà osservata (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Unità di misura&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OBBLIGATORIO - L&apos;unità di misura della proprietà osservata (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Usa&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPZIONALE - Usata per le osservazioni MODFLOW, assegna al sensore un valore che può essere utilizzato per indicare l&apos;inclusione nelle operazioni future (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Stat flag&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPZIONALE -Usato per le osservazioni MODFLOW, il tipo di statistca usata per determinare il peso dell&apos;osservazione (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Top/Bottom&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: OPZIONALE - Proiezione utilizzata nelle osservazioni MODFLOW, usata per definire screening of weels (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;  &lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; Il bottone di fianco alle coordinate permette di selezionare le coordinate 2D dalla mappa QGIS. Le coordinate sono convertite e salvate automaticamente in WGS84 qualsiasi sia il sistema di riferimento della mappa QGIS.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;CSV&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Input file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: percorso al file CSV (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: separatore valori (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;checkbox&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Skiprow&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Numero di righe da ignorare, per rimuovere l&apos;intestazione (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Value column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Indice della colonna dati, partendo da 0 (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Quality column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Indice della colonna qualità, partendo da 0, -1 significa nessuna colonna qualità (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Date format&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Dropdown con dei formati data predefiniti, puô essere inseito manualmente seguendo le specifiche strftime  (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;No data value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: &lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;comment&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: carattere che è utilizzato all&apos;interno del csv per iniziare una linea di commento (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; Nel riquadro inferiore viene mostrata un&apos;anteprima del file CSV. Essa può essere utilizzata per assegnare le colonne. Quando si preme il bottone Anteprima, viene mostrata un&apos;anteprima dei dati utilizzando i parametri inseriti precedentemente.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;IstSOS&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Quando si seleziona l&apos;opzione IstSOS come input, bisogna connettersi ad un server IstSOS(similmente alla connessione database in QGIS, bisogna crearne uno nuovo oppure utilizzare uno esistente). I server possone essere selezionati del drop-down
        menu. &lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Se non sono disponibili server, una nuova connessione può essere stabilita con il pulsante Nuovo, specificando, in seguito, i parametri del server. È possibile utilizzare anche l&apos;autenticazione. Una volta connesso viene caricata la lista di sensori disponibili, le proprietà osservate la frequenza.&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Quando si caricano dati da un server istSOS, il frame dei metadati viene riempito automaticamente con i metadatiricevuti dal serverdopo che si preme il bottone Applica.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;SOS server&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: nome del server SOS (string)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sensor&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: nome del sensore (string)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Observed property&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: proprietà osservata del sensore da caricare (string)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Frequency&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: frequenza delle misure (string)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Interval&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: se selezionato permette di caricare i dati da un intervallo di tempo definito da&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Inizio&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; e &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Fine&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; . È anche possibile selezionare una &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;metodo d&apos;aggregazione &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;con un &lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Intervallo d&apos;aggregazione &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;(&lt;/span&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/ISO_8601#Durations&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;ISO:8601 duration&lt;/span&gt;&lt;/a&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;RAW&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Il metodo RAW può essere utilizzato per inserire i dati manualmente.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Posizione iniziale&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Data della prima misura (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Posizione finale&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Data dell&apos;ultima misurae (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Fuso orario&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Fuso orario delle misure(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Frequenza&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Dropdown con delle frequenze predefinite (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;List file&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;È possibile creare una serie temporale da una simulazione MODFLOW tramite il file MODFLOW listing.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Listing file path&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: percorso al file listing (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Data iniziale&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: data iniziale della simulazione (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Fuso orario&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: fuso orario della simulazione (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Property&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: dropdwn contentete MODFLOW packages (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Input&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Selection of flow into or outo the model (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Hob file&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Le simulazioni di un modello MODFLOW possono essere caricate come un nuovo sensore. &lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Hob in file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the MODLFOW head observation file path (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Start date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Data iniziale del modello MODFLOW (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Timezone&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il fuso orario per la data iniziale (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Disc file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The MODFLOW head result file path (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Hob out file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The MODFLOW head result file path (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Hobname&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il prefisso dell&apos;osservazione selezionata (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Gage file&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Il MODFLOW gage file può contenere risultati del modello di corpi idrici superficiali&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;GAGE file path&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il percorso al file GAGE (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Start date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: La data iniziale(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Timezone&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il fuso orario della data iniziale (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Property&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Proprietà da caricare all&apos;interno del GAGE file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;/body&gt;

&lt;/html&gt;
</translation>
    </message>
</context>
<context>
    <name>linux</name>
    <message>
        <location filename="linux.ui" line="14"/>
        <source>Linux installer</source>
        <translation>Installer linux</translation>
    </message>
    <message>
        <location filename="linux.ui" line="26"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Install missing dependencies&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In order to run the FREEWAT plugin is mecessary to install some dependencies:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;pip,flopy, numpy, pandas, requests, isodate, seaborn, xlwt, xlrd&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please insert the sudo password below to install it&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Installazione dipendenze mancanti&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Per poter eseguire FREEWAT è necessario installare le seguenti dipendenze:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;pip,flopy, numpy, pandas, requests, isodate, seaborn, xlwt, xlrd&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Per favore inserisci la password di un utente sudo nel campo sottostante per installare le dipendenze mancanti&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="linux.ui" line="43"/>
        <source>sudo password</source>
        <translation>password utente sudo</translation>
    </message>
    <message>
        <location filename="linux.ui" line="65"/>
        <source>Use proxy</source>
        <translation>Usa proxy</translation>
    </message>
    <message>
        <location filename="linux.ui" line="98"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="linux.ui" line="108"/>
        <source>port</source>
        <translation>porta</translation>
    </message>
    <message>
        <location filename="linux.ui" line="132"/>
        <source>User autentication</source>
        <translation>Autenticazione</translation>
    </message>
    <message>
        <location filename="linux.ui" line="159"/>
        <source>User</source>
        <translation>Utente</translation>
    </message>
    <message>
        <location filename="linux.ui" line="176"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
</context>
<context>
    <name>multiSensor</name>
    <message>
        <location filename="multiSensor.ui" line="14"/>
        <source>Load multiple sensors</source>
        <translation>Carica sensori multipli</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="27"/>
        <source>Add Multiple Sensors</source>
        <translation>Aggiungi sensori</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="147"/>
        <source>Source</source>
        <translation>Sorgente</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="60"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="73"/>
        <source>IstSOS</source>
        <translation>IstSOS</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="154"/>
        <source>CSV</source>
        <translation>CSV</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="167"/>
        <source>SHP</source>
        <translation>SHP</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="187"/>
        <source>Metadata sensor file</source>
        <translation>File metadati</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="272"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="218"/>
        <source>Sensors CRS</source>
        <translation>CRS sensore</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="258"/>
        <source>Sensor data</source>
        <translation>Dati sensori</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="318"/>
        <source>Separator</source>
        <translation>Separatore</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="325"/>
        <source>Value column</source>
        <translation>Colonna valori</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="332"/>
        <source>date column</source>
        <translation>Colonna data</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="339"/>
        <source>No data value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="381"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="394"/>
        <source>Comma</source>
        <translation>Comma</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="407"/>
        <source>Semicolon</source>
        <translation>Semicolon</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="417"/>
        <source>Space</source>
        <translation>Space</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="444"/>
        <source>Number of row to skip</source>
        <translation>Numero di righe da ignorare</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="437"/>
        <source>Skiprow</source>
        <translation>righe da saltare</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="471"/>
        <source>the column number containing the observations values e.g. 2</source>
        <translation>il numero della colonna contenente il valore dell&apos;osservazione es. 2</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="498"/>
        <source>Quality column number: -1 don&apos;t use quality column</source>
        <translation>Numero colonna qualità: -1 non usa la colonna qualità</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="501"/>
        <source>Quality column</source>
        <translation>Colonna qualità</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="508"/>
        <source>the column number containing the quality index, -1 don&apos;t use quality column</source>
        <translation>Numero colonna qualità: -1 non usa la colonna qualità</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="535"/>
        <source>list of column numbers to be used to parse the times of observations e.g. [0,1]</source>
        <translation>Lista dei numeri di colonne che sono usate per identificare la data dell&apos;osservazione es. [0,1]</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="542"/>
        <source>Day came before of month?</source>
        <translation>i giorni vengono prima dei mesi?</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="545"/>
        <source>Day first</source>
        <translation>prima i giorni</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="559"/>
        <source>Date Format</source>
        <translation>Formato data</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="587"/>
        <source>List of values to be associated with no data value</source>
        <translation>Lista di valori da associare</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="624"/>
        <source> A character indicating a comment line not to be imported</source>
        <translation>Un carattere che indica un commento o una riga da ignorare</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="617"/>
        <source>comment</source>
        <translation>Commento</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="631"/>
        <source>Preview</source>
        <translation>Anteprima</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="663"/>
        <source>date</source>
        <translation>data</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="668"/>
        <source>data</source>
        <translation>valori</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="673"/>
        <source>quality</source>
        <translation>qualità</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="722"/>
        <source>IstSOS server</source>
        <translation>IstSOS server</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="757"/>
        <source>Connect</source>
        <translation>Connetti</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="764"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="771"/>
        <source>New</source>
        <translation>Nuovo</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="778"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="797"/>
        <source>Observed property</source>
        <translation>Proprietà osservata</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="837"/>
        <source>Aggregate function</source>
        <translation>Funzione aggregazione</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="844"/>
        <source>aggregate function</source>
        <translation>Funzione aggregazione</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="853"/>
        <source>AVG</source>
        <translation>AVG</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="858"/>
        <source>MAX</source>
        <translation>MAX</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="863"/>
        <source>MIN</source>
        <translation>MIN</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="868"/>
        <source>SUM</source>
        <translation>SUM</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="886"/>
        <source>aggregate interval, expressed in iso 8601 duration e.g. &quot;P1DT12H&quot;</source>
        <translation>Intervallo aggregazione, espresso nel formato iso8601 duration es. &quot;PT1DT12H&quot;</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="879"/>
        <source>Aggregate interval</source>
        <translation>Intervallo aggregazione</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="1014"/>
        <source>Procedure</source>
        <translation>Procedura</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="951"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="958"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="978"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="985"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="1029"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="1038"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Compare sensor &lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Multiple sensors can be added to OAT at once. This require two file types:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;A single metadata file containing all of the principal metadata. Each row must contain the metadata for one sensor.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;A .csv or .txt file for each Sensor containing the time-series data&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Metadata&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The prinipal metadata required for any sensor:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Sensor name&lt;/span&gt;: MANDATORY - this is the name of the sensor of this point of measure (unique within a FREEWAT model)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Description&lt;/span&gt;: OPTIONAL - a texual description of the sensor&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Latitude&lt;/span&gt;: MANDATORY - the latitude in WGS84 (EPSG:4326)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Longitude&lt;/span&gt;: MANDATORY - the longitude in WGS84 (EPSG:4326)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Altitude&lt;/span&gt;: OPTIONAL - the altitude in m a.s.l. of the sensor&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Observed property&lt;/span&gt;: MANDATORY - the physical property that is measured&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Unit of measure&lt;/span&gt;: MANDATORY - the unit of measure of the observation&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Use option&lt;/span&gt;: OPTIONAL - used for MODFLOW observations, assigns the Sensor with a use field which can be used to indicates the incude in future operations&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Stat flag&lt;/span&gt;: OPTIONAL - used in MODFLOW observations, the type of statistic used to determine the weight of the observations&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Top/Bottom position&lt;/span&gt;: OPTIONAL - screen used in MODFLOW observations, the used to define the screening of wells&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Sensor data file&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Input file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: path to the csv file (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: value separator (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;checkbox&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Skiprow&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Number of row to skip, to remove the header (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Value column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Index of the data column, starting from 0 (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Quality column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Index of the quality column, starting from 0, -1 means no quality column (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Date format&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Dropdown with some predefined timeformat, it can be manually entered according to the strftime format (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;No data value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;comment&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: character that is used inside csv to start a comment line (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;r:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Guida&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Aggiungi più sensori &lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Molti sensori possono esse aggiunti simultaneamente. Questo richiede due tipi di file:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Un singolo file contenente i metadati dei sensori. Ogni riga deve contenere i metadati di un sensore, può essere CSV o SHP.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Un file .csv o .txt per ogni sensore contenente le serie temporali&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Metadati&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;I principali metadati richiesti per ogni sensore:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Nome sensore&lt;/span&gt;: OBBLIGATORIO - nome del sensore (univocoall&apos;interno del modello FREEWAT)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Descrizione&lt;/span&gt;: OPZIONALE - una descrizione del sensore&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Latitudine&lt;/span&gt;: OBBLIGATORIO - la latitudine in WGS84 (EPSG:4326)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Longitudine&lt;/span&gt;: OBBLIGATORIO - la longitudine in WGS84 (EPSG:4326)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Altitudine&lt;/span&gt;: OPZIONALE -l&apos;altitudine in m.s.l.m. del sensore&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Proprietà osservata&lt;/span&gt;: OBBLIGATORIO - la proprietà osservata&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Unità di misura&lt;/span&gt;: OBBLIGATORIO -l&apos;unità di misura della proprietà osservata&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Usa&lt;/span&gt;: OPZIONALE - used for MODFLOW observations, assigns the Sensor with a use field which can be used to indicates the incude in future operations&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Stat flag&lt;/span&gt;: OPZIONALE - il tipo di statistica usata per determinare il peso dell&apos;osservazione&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Top/Bottom position&lt;/span&gt;: OPZIONALE - screen used in MODFLOW observations, the used to define the screening of wells&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Sensor data file&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Input file&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: percorso al file csv (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Separator&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: separatore dei valori (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;checkbox&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Skiprow&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: numero di righe da ignorare, per rimuovere l&apos;intestazione (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Value column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Indice della colonna dei dati, partendo da 0 (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Quality column&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Indice della colonna della qualità, partendo da 0, -1 significa nessuna colonna qualità (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Date format&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Dropdown con dei formati data predefiniti, è possibile inserirlo manualmente utilizzando il formato strftime  (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;No data value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;comment&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: carattere utilizzato all&apos;interno del file csv per indicare una linea di commento all&apos;interno del file csv (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;r:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="1114"/>
        <source>force overwrite</source>
        <translation>Forza sovrascrizione</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="1121"/>
        <source>load</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="multiSensor.ui" line="1128"/>
        <source>close</source>
        <translation>Chiudi</translation>
    </message>
</context>
<context>
    <name>plotWaterUnitBudget</name>
    <message>
        <location filename="ui_plotWaterUnitBudgets.ui" line="14"/>
        <source>View Model Output</source>
        <translation>Vedi output del modello</translation>
    </message>
    <message>
        <location filename="ui_plotWaterUnitBudgets.ui" line="27"/>
        <source>Model Name:</source>
        <translation>Nome modello:</translation>
    </message>
    <message>
        <location filename="ui_plotWaterUnitBudgets.ui" line="50"/>
        <source>Water Unit n.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_plotWaterUnitBudgets.ui" line="80"/>
        <source>Plot Water Unit Delivery Budget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_plotWaterUnitBudgets.ui" line="89"/>
        <source>Plot Water Unit Detailed Budget (INFLOW)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_plotWaterUnitBudgets.ui" line="96"/>
        <source>Plot Water Unit Detailed Budget (OUTFLOW)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>process</name>
    <message>
        <location filename="process.ui" line="26"/>
        <source>Process time series</source>
        <translation>Processa serie temporali</translation>
    </message>
    <message>
        <location filename="process.ui" line="2193"/>
        <source>Sensor</source>
        <translation>Sensore</translation>
    </message>
    <message>
        <location filename="process.ui" line="83"/>
        <source>Preview</source>
        <translation>Anteprima</translation>
    </message>
    <message>
        <location filename="process.ui" line="205"/>
        <source>Execute</source>
        <translation>Esegui</translation>
    </message>
    <message>
        <location filename="process.ui" line="212"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="process.ui" line="219"/>
        <source>Overwrite</source>
        <translation>Sovrascrivi</translation>
    </message>
    <message>
        <location filename="process.ui" line="319"/>
        <source>Order</source>
        <translation>Ordine</translation>
    </message>
    <message>
        <location filename="process.ui" line="343"/>
        <source>filter type</source>
        <translation>tipo filtro</translation>
    </message>
    <message>
        <location filename="process.ui" line="351"/>
        <source>highpass</source>
        <translation>passa alto</translation>
    </message>
    <message>
        <location filename="process.ui" line="356"/>
        <source>lowpass</source>
        <translation>passa basso</translation>
    </message>
    <message>
        <location filename="process.ui" line="498"/>
        <source>Under</source>
        <translation>Sotto</translation>
    </message>
    <message>
        <location filename="process.ui" line="1006"/>
        <source>period</source>
        <translation>periodo</translation>
    </message>
    <message>
        <location filename="process.ui" line="397"/>
        <source>exceedance values</source>
        <translation>valori superamento</translation>
    </message>
    <message>
        <location filename="process.ui" line="421"/>
        <source>exceedance probability</source>
        <translation>probabilità superamento</translation>
    </message>
    <message>
        <location filename="process.ui" line="452"/>
        <source>exceedance time unit</source>
        <translation>unità tempo superamento</translation>
    </message>
    <message>
        <location filename="process.ui" line="495"/>
        <source>calculate the probability for which values are exceeded</source>
        <translation>calcola la probabilità per cui si sono superati i valori</translation>
    </message>
    <message>
        <location filename="process.ui" line="833"/>
        <source>component</source>
        <translation>componente</translation>
    </message>
    <message>
        <location filename="process.ui" line="907"/>
        <source>classification</source>
        <translation>classificazione</translation>
    </message>
    <message>
        <location filename="process.ui" line="1483"/>
        <source>median</source>
        <translation>mediana</translation>
    </message>
    <message>
        <location filename="process.ui" line="983"/>
        <source>drain area</source>
        <translation>area drenaggio</translation>
    </message>
    <message>
        <location filename="process.ui" line="1516"/>
        <source>value</source>
        <translation>valore</translation>
    </message>
    <message>
        <location filename="process.ui" line="1099"/>
        <source>Type of statistics</source>
        <translation>Tipo di statistica</translation>
    </message>
    <message>
        <location filename="process.ui" line="1316"/>
        <source>Frequency</source>
        <translation>Frequenza</translation>
    </message>
    <message>
        <location filename="process.ui" line="1764"/>
        <source>alpha</source>
        <translation>alfa</translation>
    </message>
    <message>
        <location filename="process.ui" line="1818"/>
        <source>time units</source>
        <translation>unità di tempo</translation>
    </message>
    <message>
        <location filename="process.ui" line="1967"/>
        <source>factor</source>
        <translation>fattore</translation>
    </message>
    <message>
        <location filename="process.ui" line="2022"/>
        <source>dates as text</source>
        <translation>date come testo</translation>
    </message>
    <message>
        <location filename="process.ui" line="2066"/>
        <source>exponent</source>
        <translation>esponente</translation>
    </message>
    <message>
        <location filename="process.ui" line="2105"/>
        <source>BIAS</source>
        <translation>BIAS</translation>
    </message>
    <message>
        <location filename="process.ui" line="2112"/>
        <source>STANDARD_ERROR</source>
        <translation>STANDARD_ERROR</translation>
    </message>
    <message>
        <location filename="process.ui" line="2119"/>
        <source>RELATIVE_BIAS</source>
        <translation>RELATIVE_BIAS</translation>
    </message>
    <message>
        <location filename="process.ui" line="2126"/>
        <source>RELATIVE_STANDARD_ERROR</source>
        <translation>RELATIVE_STANDARD_ERROR</translation>
    </message>
    <message>
        <location filename="process.ui" line="2140"/>
        <source>NASH_SUTCLIFFE</source>
        <translation>NASH_SUTCLIFFE</translation>
    </message>
    <message>
        <location filename="process.ui" line="2147"/>
        <source>COEFFICIENT_OF_EFFICIENCY</source>
        <translation>COEFFICIENT_OF_EFFICIENCY</translation>
    </message>
    <message>
        <location filename="process.ui" line="2154"/>
        <source>INDEX_OF_AGREEMENT</source>
        <translation>INDEX_OF_AGREEMENT</translation>
    </message>
    <message>
        <location filename="process.ui" line="2161"/>
        <source>VOLUMETRIC_EFFICIENCY</source>
        <translation>VOLUMETRIC_EFFICIENCY</translation>
    </message>
    <message>
        <location filename="process.ui" line="2200"/>
        <source>Sensor to be used to subtract values</source>
        <translation>Sensore utilizzato per la sottrazione</translation>
    </message>
    <message>
        <location filename="process.ui" line="2227"/>
        <source>align method</source>
        <translation>metodo allineamento</translation>
    </message>
    <message>
        <location filename="process.ui" line="2234"/>
        <source>method for alignment of the sensor time serie</source>
        <translation>metodo per allineare le serie temporali dei sensori</translation>
    </message>
    <message>
        <location filename="process.ui" line="2325"/>
        <source>filling no data method</source>
        <translation>metodo riempimento valori mancanti</translation>
    </message>
    <message>
        <location filename="process.ui" line="2348"/>
        <source>spline</source>
        <translation>spline</translation>
    </message>
    <message>
        <location filename="process.ui" line="2353"/>
        <source>linear</source>
        <translation>linear</translation>
    </message>
    <message>
        <location filename="process.ui" line="2358"/>
        <source>quadratic</source>
        <translation>quadratic</translation>
    </message>
    <message>
        <location filename="process.ui" line="2363"/>
        <source>cubic</source>
        <translation>cubic</translation>
    </message>
    <message>
        <location filename="process.ui" line="2288"/>
        <source>consecutive no data allowed</source>
        <translation>numero di valori mancanti consentito</translation>
    </message>
    <message>
        <location filename="process.ui" line="2407"/>
        <source>Data</source>
        <translation>Valori</translation>
    </message>
    <message>
        <location filename="process.ui" line="2417"/>
        <source>Quality</source>
        <translation>Qualità</translation>
    </message>
    <message>
        <location filename="process.ui" line="2564"/>
        <source>press Execute to run the process</source>
        <translation>premi Esegui per eseguire il processamento</translation>
    </message>
    <message>
        <location filename="process.ui" line="2623"/>
        <source>History</source>
        <translation>Storico</translation>
    </message>
    <message>
        <location filename="process.ui" line="2637"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="process.ui" line="267"/>
        <source>low cutoff freq.</source>
        <translation>freq. taglio inferiore</translation>
    </message>
    <message>
        <location filename="process.ui" line="36"/>
        <source>Process</source>
        <translation>Processamento</translation>
    </message>
    <message>
        <location filename="process.ui" line="132"/>
        <source>digital filter</source>
        <translation>digital filter</translation>
    </message>
    <message>
        <location filename="process.ui" line="137"/>
        <source>exceedance</source>
        <translation>exceedance</translation>
    </message>
    <message>
        <location filename="process.ui" line="142"/>
        <source>hydro events</source>
        <translation>hydro events</translation>
    </message>
    <message>
        <location filename="process.ui" line="147"/>
        <source>hydro indices</source>
        <translation>hydro indices</translation>
    </message>
    <message>
        <location filename="process.ui" line="152"/>
        <source>quality</source>
        <translation>quality</translation>
    </message>
    <message>
        <location filename="process.ui" line="157"/>
        <source>resample</source>
        <translation>resample</translation>
    </message>
    <message>
        <location filename="process.ui" line="162"/>
        <source>data values</source>
        <translation>data values</translation>
    </message>
    <message>
        <location filename="process.ui" line="167"/>
        <source>hydro separation</source>
        <translation>hydro separation</translation>
    </message>
    <message>
        <location filename="process.ui" line="172"/>
        <source>integrate</source>
        <translation>integrate</translation>
    </message>
    <message>
        <location filename="process.ui" line="177"/>
        <source>compare</source>
        <translation>compare</translation>
    </message>
    <message>
        <location filename="process.ui" line="182"/>
        <source>subtract</source>
        <translation>subtract</translation>
    </message>
    <message>
        <location filename="process.ui" line="187"/>
        <source>fill</source>
        <translation>fill</translation>
    </message>
    <message>
        <location filename="process.ui" line="192"/>
        <source>statistics</source>
        <translation>statistics</translation>
    </message>
    <message>
        <location filename="process.ui" line="197"/>
        <source>hargreaves</source>
        <translation>hargreaves</translation>
    </message>
    <message>
        <location filename="process.ui" line="274"/>
        <source>low cutoff frequency</source>
        <translation>Frequenza di taglio inferirore</translation>
    </message>
    <message>
        <location filename="process.ui" line="291"/>
        <source>high cutoff freq</source>
        <translation>freq. taglio superiore</translation>
    </message>
    <message>
        <location filename="process.ui" line="298"/>
        <source>high cutoff frequency</source>
        <translation>frequenza di taglio superiore</translation>
    </message>
    <message>
        <location filename="process.ui" line="404"/>
        <source>list of excedance values to calculate the excedance probability</source>
        <translation>lista di valori di superamento per calcoalre la probabilità</translation>
    </message>
    <message>
        <location filename="process.ui" line="428"/>
        <source>list of exceedance probability to calculate the excedance values</source>
        <translation>lista di probabilità di superamento per calcolare i valori di superamento</translation>
    </message>
    <message>
        <location filename="process.ui" line="1844"/>
        <source>days</source>
        <translation>days</translation>
    </message>
    <message>
        <location filename="process.ui" line="1829"/>
        <source>seconds</source>
        <translation>seconds</translation>
    </message>
    <message>
        <location filename="process.ui" line="1834"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location filename="process.ui" line="1839"/>
        <source>hours</source>
        <translation>hours</translation>
    </message>
    <message>
        <location filename="process.ui" line="1849"/>
        <source>years</source>
        <translation>years</translation>
    </message>
    <message>
        <location filename="process.ui" line="524"/>
        <source>Days prior the peak</source>
        <translation>Giorni prima del picco</translation>
    </message>
    <message>
        <location filename="process.ui" line="531"/>
        <source>The number of days prior to the peak to include in the event hydrograph</source>
        <translation>Il numero di giorni che precedono il picco da inserire nel grafico degli eventi idrici</translation>
    </message>
    <message>
        <location filename="process.ui" line="545"/>
        <source>Days following the peak</source>
        <translation>Giorni dopo il picco</translation>
    </message>
    <message>
        <location filename="process.ui" line="552"/>
        <source>The number of days following the peak to include in the event hydrograph</source>
        <translation>Il numero di giorni successivi al picco da includere nel grafico degli eventi idrici</translation>
    </message>
    <message>
        <location filename="process.ui" line="573"/>
        <source>Min days between peak</source>
        <translation>Min giorni tra picchi</translation>
    </message>
    <message>
        <location filename="process.ui" line="580"/>
        <source>Minimum time between successive peaks, in days</source>
        <translation>Tempo minimo, in giorni, tra due picchi</translation>
    </message>
    <message>
        <location filename="process.ui" line="594"/>
        <source>Minimun value for a peak</source>
        <translation>Valore minimo picco</translation>
    </message>
    <message>
        <location filename="process.ui" line="628"/>
        <source>The name of the time series on which statistical calculations will be carried out</source>
        <translation>Il nome della serie temporale</translation>
    </message>
    <message>
        <location filename="process.ui" line="621"/>
        <source>Name of time series</source>
        <translation>Nome serie temporale</translation>
    </message>
    <message>
        <location filename="process.ui" line="2343"/>
        <source>time</source>
        <translation>time</translation>
    </message>
    <message>
        <location filename="process.ui" line="2473"/>
        <source>Begin</source>
        <translation>Inizio</translation>
    </message>
    <message>
        <location filename="process.ui" line="2498"/>
        <source>yyyy-MM-dd hh:mm:ss</source>
        <translation>yyyy-MM-dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="process.ui" line="2491"/>
        <source>End</source>
        <translation>Fine</translation>
    </message>
    <message>
        <location filename="process.ui" line="729"/>
        <source>alphanumeric code</source>
        <translation>codice alfanumerico</translation>
    </message>
    <message>
        <location filename="process.ui" line="737"/>
        <source>MA</source>
        <translation>MA</translation>
    </message>
    <message>
        <location filename="process.ui" line="742"/>
        <source>ML</source>
        <translation>ML</translation>
    </message>
    <message>
        <location filename="process.ui" line="747"/>
        <source>MH</source>
        <translation>MH</translation>
    </message>
    <message>
        <location filename="process.ui" line="752"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="process.ui" line="757"/>
        <source>FH</source>
        <translation>FH</translation>
    </message>
    <message>
        <location filename="process.ui" line="762"/>
        <source>DL</source>
        <translation>DL</translation>
    </message>
    <message>
        <location filename="process.ui" line="767"/>
        <source>DH</source>
        <translation>DH</translation>
    </message>
    <message>
        <location filename="process.ui" line="772"/>
        <source>TA</source>
        <translation>TA</translation>
    </message>
    <message>
        <location filename="process.ui" line="777"/>
        <source>TL</source>
        <translation>TL</translation>
    </message>
    <message>
        <location filename="process.ui" line="782"/>
        <source>TH</source>
        <translation>TH</translation>
    </message>
    <message>
        <location filename="process.ui" line="787"/>
        <source>RA</source>
        <translation>RA</translation>
    </message>
    <message>
        <location filename="process.ui" line="812"/>
        <source>code that jointly with htype determine the indiced to calculate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="process.ui" line="805"/>
        <source>indices to calculate</source>
        <translation>indici da calcolare</translation>
    </message>
    <message>
        <location filename="process.ui" line="914"/>
        <source>hydrologic regime as defined in Olden and Poff </source>
        <translation>regime idrologico come definito da Olden &amp; Poff</translation>
    </message>
    <message>
        <location filename="process.ui" line="844"/>
        <source>AVERAGE_MAGNITUDE</source>
        <translation>AVERAGE_MAGNITUDE</translation>
    </message>
    <message>
        <location filename="process.ui" line="849"/>
        <source>LOW_FLOW_MAGNITUDE</source>
        <translation>LOW_FLOW_MAGNITUDE</translation>
    </message>
    <message>
        <location filename="process.ui" line="854"/>
        <source>HIGH_FLOW_MAGNITUDE</source>
        <translation>HIGH_FLOW_MAGNITUDE</translation>
    </message>
    <message>
        <location filename="process.ui" line="869"/>
        <source>LOW_FLOW_FREQUENCY</source>
        <translation>LOW_FLOW_FREQUENCY</translation>
    </message>
    <message>
        <location filename="process.ui" line="864"/>
        <source>HIGH_FLOW_FREQUENCY</source>
        <translation>HIGH_FLOW_FREQUENCY</translation>
    </message>
    <message>
        <location filename="process.ui" line="874"/>
        <source>LOW_FLOW_DURATION</source>
        <translation>LOW_FLOW_DURATION</translation>
    </message>
    <message>
        <location filename="process.ui" line="879"/>
        <source>HIGH_FLOW_DURATION</source>
        <translation>HIGH_FLOW_DURATION</translation>
    </message>
    <message>
        <location filename="process.ui" line="884"/>
        <source>TIMING</source>
        <translation>TIMING</translation>
    </message>
    <message>
        <location filename="process.ui" line="889"/>
        <source>RATE_OF_CHANGE</source>
        <translation>RATE_OF_CHANGE</translation>
    </message>
    <message>
        <location filename="process.ui" line="918"/>
        <source>HARSH_INTERMITTENT</source>
        <translation>HARSH_INTERMITTENT</translation>
    </message>
    <message>
        <location filename="process.ui" line="923"/>
        <source>FLASHY_INTERMITTENT</source>
        <translation>FLASHY_INTERMITTENT</translation>
    </message>
    <message>
        <location filename="process.ui" line="928"/>
        <source>SNOWMELT_PERENNIAL</source>
        <translation>SNOWMELT_PERENNIAL</translation>
    </message>
    <message>
        <location filename="process.ui" line="933"/>
        <source>SNOW_RAIN_PERENNIAL</source>
        <translation>SNOW_RAIN_PERENNIAL</translation>
    </message>
    <message>
        <location filename="process.ui" line="938"/>
        <source>GROUNDWATER_PERENNIAL</source>
        <translation>GROUNDWATER_PERENNIAL</translation>
    </message>
    <message>
        <location filename="process.ui" line="943"/>
        <source>FLASHY_PERENNIAL</source>
        <translation>FLASHY_PERENNIAL</translation>
    </message>
    <message>
        <location filename="process.ui" line="948"/>
        <source>ALL_STREAMS</source>
        <translation>ALL_STREAMS</translation>
    </message>
    <message>
        <location filename="process.ui" line="965"/>
        <source>Requests that indices that normally report the mean of some other sumamry statistic to instead report the median value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="process.ui" line="990"/>
        <source>the gauge area in m3</source>
        <translation>l&apos;area di calibrazione in m3</translation>
    </message>
    <message>
        <location filename="process.ui" line="1086"/>
        <source>the value of the weigth to be assigned</source>
        <translation>il valore del peso da assegnare</translation>
    </message>
    <message>
        <location filename="process.ui" line="1106"/>
        <source>The type of statistics the weight is estimated from</source>
        <translation>Il tipo di statistica</translation>
    </message>
    <message>
        <location filename="process.ui" line="1110"/>
        <source>VAR</source>
        <translation>VAR</translation>
    </message>
    <message>
        <location filename="process.ui" line="1115"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="process.ui" line="1120"/>
        <source>CV</source>
        <translation>CV</translation>
    </message>
    <message>
        <location filename="process.ui" line="1125"/>
        <source>WT</source>
        <translation>WT</translation>
    </message>
    <message>
        <location filename="process.ui" line="1130"/>
        <source>SQRWT</source>
        <translation>WT</translation>
    </message>
    <message>
        <location filename="process.ui" line="2444"/>
        <source>Use time</source>
        <translation>Usa tempo</translation>
    </message>
    <message>
        <location filename="process.ui" line="1896"/>
        <source>Begin position</source>
        <translation>Posizione iniziale</translation>
    </message>
    <message>
        <location filename="process.ui" line="1914"/>
        <source>End position</source>
        <translation>Posizione finale</translation>
    </message>
    <message>
        <location filename="process.ui" line="2511"/>
        <source>Timezone</source>
        <translation>Fuso orario</translation>
    </message>
    <message>
        <location filename="process.ui" line="1656"/>
        <source>Use limit</source>
        <translation>Usa limiti</translation>
    </message>
    <message>
        <location filename="process.ui" line="1678"/>
        <source>Low</source>
        <translation>Basso</translation>
    </message>
    <message>
        <location filename="process.ui" line="1688"/>
        <source>High</source>
        <translation>Alto</translation>
    </message>
    <message>
        <location filename="process.ui" line="1323"/>
        <source>Offset Aliases sting (A=year,M=month,W=week,D=day,H=hour,T=minute,S=second; e.g.: 1H10T)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="process.ui" line="1454"/>
        <source>sampling method</source>
        <translation>metodo campionatura</translation>
    </message>
    <message>
        <location filename="process.ui" line="1333"/>
        <source>Sampling method</source>
        <translation>Metodo campionatura</translation>
    </message>
    <message>
        <location filename="process.ui" line="2238"/>
        <source>mean</source>
        <translation>mean</translation>
    </message>
    <message>
        <location filename="process.ui" line="1463"/>
        <source>max</source>
        <translation>max</translation>
    </message>
    <message>
        <location filename="process.ui" line="1468"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="process.ui" line="1473"/>
        <source>first</source>
        <translation>first</translation>
    </message>
    <message>
        <location filename="process.ui" line="1478"/>
        <source>last</source>
        <translation>last</translation>
    </message>
    <message>
        <location filename="process.ui" line="1488"/>
        <source>sum</source>
        <translation>sum</translation>
    </message>
    <message>
        <location filename="process.ui" line="1396"/>
        <source>if not null it defines the method for filling no-data</source>
        <translation>se non vuoto, definische il metodo per riempire i valori mancanti</translation>
    </message>
    <message>
        <location filename="process.ui" line="1389"/>
        <source>Fill</source>
        <translation>Riempimento</translation>
    </message>
    <message>
        <location filename="process.ui" line="2333"/>
        <source>bfill</source>
        <translation>bfill</translation>
    </message>
    <message>
        <location filename="process.ui" line="2338"/>
        <source>ffill</source>
        <translation>ffill</translation>
    </message>
    <message>
        <location filename="process.ui" line="1428"/>
        <source>if not null defines the maximum numbers of allowed consecutive no-data</source>
        <translation>se non nullo definische il numero massimo di valori mancanti consecutivi</translation>
    </message>
    <message>
        <location filename="process.ui" line="1421"/>
        <source>Limit</source>
        <translation>Limite</translation>
    </message>
    <message>
        <location filename="process.ui" line="1447"/>
        <source>How Quality</source>
        <translation>qualità</translation>
    </message>
    <message>
        <location filename="process.ui" line="1523"/>
        <source>the value to be assigned</source>
        <translation>valore da assegnare</translation>
    </message>
    <message>
        <location filename="process.ui" line="1721"/>
        <source>mode</source>
        <translation>modo</translation>
    </message>
    <message>
        <location filename="process.ui" line="1728"/>
        <source>the method for hydrograph separation
 TPDF: Two Parameter Digital Filter
 SPDF: Single Parameter Digital Filter</source>
        <translation>il metodo per la separazione idorgrafica
 TPDF: Two Parameter Digital Filter
 SPDF: Single Parameter Digital Filter</translation>
    </message>
    <message>
        <location filename="process.ui" line="1734"/>
        <source>TPDF</source>
        <translation>TPDF</translation>
    </message>
    <message>
        <location filename="process.ui" line="1739"/>
        <source>SPDF</source>
        <translation>SPDF</translation>
    </message>
    <message>
        <location filename="process.ui" line="1781"/>
        <source>bfl_max</source>
        <translation>bfl_max</translation>
    </message>
    <message>
        <location filename="process.ui" line="1825"/>
        <source>The time units of data employed by the time series</source>
        <translation>Le unità di tempo dei valori impiegati dalla serie temporale</translation>
    </message>
    <message>
        <location filename="process.ui" line="1974"/>
        <source>factor by which integrated volumes or masses are multiplied before storage
 generally used for unit conversion (e.g.: 0.0283168 will convert cubic feets to cubic meters)</source>
        <translation>fattore per cui il volume o le masse integrate si moltiplica prima del salvataggio
- generalmente utilizzato per convertire in un altra unità di misura (es: 0.0283168 converte piedi cubi in metri cubi)</translation>
    </message>
    <message>
        <location filename="process.ui" line="1985"/>
        <source>how</source>
        <translation>come</translation>
    </message>
    <message>
        <location filename="process.ui" line="1992"/>
        <source>integration method</source>
        <translation>metodo integrazione</translation>
    </message>
    <message>
        <location filename="process.ui" line="1996"/>
        <source>trapz</source>
        <translation>trapz</translation>
    </message>
    <message>
        <location filename="process.ui" line="2001"/>
        <source>cumtrapz</source>
        <translation>cumtrapz</translation>
    </message>
    <message>
        <location filename="process.ui" line="2006"/>
        <source>simps</source>
        <translation>simps</translation>
    </message>
    <message>
        <location filename="process.ui" line="2011"/>
        <source>romb</source>
        <translation>romb</translation>
    </message>
    <message>
        <location filename="process.ui" line="2019"/>
        <source>define if dates has to be returned as text (True) or Timestamp (False)</source>
        <translation>definisce se le date devono essere convertite a testo(True) o Timestamp (False)</translation>
    </message>
    <message>
        <location filename="process.ui" line="2059"/>
        <source>Sensor to compare</source>
        <translation>Sensore da confrontare</translation>
    </message>
    <message>
        <location filename="process.ui" line="2073"/>
        <source>the exponent used in the calculation of COEFFICIENT_OF_EFFICIENCY or INDEX_OF_AGREEMENT</source>
        <translation>l&apos;esponente utilizzato per calcolare COEFFICIENT_OF_EFFICIENCY o INDEX_OF_AGREEMENT</translation>
    </message>
    <message>
        <location filename="process.ui" line="2086"/>
        <source>Align</source>
        <translation>Allinea</translation>
    </message>
    <message>
        <location filename="process.ui" line="2646"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Digital filter&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The digital filter Method calculates a new Sensor by passing an existing Sensor through a digital filter to remove high or low frequency components. (low- and high-pass filters respectively). A high-pass filter removes long-term variations from a time-series, whereas a low-pass filter removes short term variations&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;low cutoff frequency&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the 3 dB point of high frequency roll-off for a low-pass filter (in units of day&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;-1&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;high cutoff frequency&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the 3dB point of low frequency roll-off for a high-pass filter (in units of day&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;-1&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Order&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the number of filter stages (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer between 1 and 3&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;filter type&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: highpass or lowpass (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Exceedance filter&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Exceedance probability method calculates the exceedance probability after Searcy (1959) for a particular Sensor&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Exceedance time unit&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the time unit in which the exceedance frequency will be displayed (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Under&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: change the calculation to specify when values are not ecceeded (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Exceedance values&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: A discharge value for which the exceedance probability will be calculated (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;list of float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Exceedance probability&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The discharge values that correspond to a given exceedance probability (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;list of float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;FIll filter&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If a Sensor is missing data in the form of gaps, or if it contains no-data values, it can be filled using a variety of methods:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;bfill&lt;/span&gt;: backward fill (fills all the missing data values with the last data value before the no-data gap)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ffill&lt;/span&gt;: forward fill (fills all the missing data values with the next data value after the no-data gap)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Extract hydro events&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The extract hydrologic events method can be used to extract hydrographs for a time period (in days) preceding and following a peak hydrologic event, such as a storm. The minimum peak value is to be defined, as well as the miniumum time between peaks. The method will produce a new time-series for every event with the name of time-series assigned&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Days prior the peak&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The number of days prior the peak to include in the event hydrograph (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Days following the peak&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The number of days following the peak to include in the event hydrograph (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Min days between peak&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Minimum time between successive peak, in days (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Exceedance probability&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: suffix to be given to the created time-series; they will be named seriesName + suffix + number (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;time&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: a tuple of two elements indicating the Begin and End of datetimes records to be sed in peak extraction (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Hydrograph separation&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The hydrograph separation method is a filter which produces two time-series, storm-flow and baseflow hydrograph, from stream discharge. Either the Two Parameter Digital Filter (&lt;/span&gt;&lt;a href=&quot;http://onlinelibrary.wiley.com/doi/10.1002/hyp.5675/abstract;jsessionid=BD9AC71979A38AA7E63A2EE95E4D1C19.f02t02?systemMessage=Wiley+Online+Library+will+be+unavailable+on+Saturday+17th+December+2016+at+09%3A00+GMT%2F+04%3A00+EST%2F+17%3A00+SGT+for+4hrs+due+to+essential+maintenance.Apologies+for+the+inconvenience&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Eckhardt, 2005&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) or Single Parameter Digital Filter (&lt;/span&gt;&lt;a href=&quot;http://onlinelibrary.wiley.com/doi/10.1029/WR026i007p01465/abstract?systemMessage=Wiley+Online+Library+will+be+unavailable+on+Saturday+17th+December+2016+at+09%3A00+GMT%2F+04%3A00+EST%2F+17%3A00+SGT+for+4hrs+due+to+essential+maintenance.Apologies+for+the+inconvenience&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Nathan &amp;amp; McMahon, 1990&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) may be used.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Mode&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Select bewteen TPDF and SPDF (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Alpha&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: rate of decay of baseflow relative to current flow rate (recommended value 0.92 - 0.98) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Bfl_max&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Long-term ratio of baseflow to total streamflow (requiredin case of TPDF) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Hydrologic indices&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The hydrologic indeces are a series of statistical measures of streamflow used to describe various ecologically important aspects of flow regime. Over 160 different hydrologic indeces can be calculated. For a complete discription of the available hydrologic indices, as well as the strings required to activate them, see the appendix in &lt;/span&gt;&lt;a href=&quot;http://onlinelibrary.wiley.com/doi/10.1002/rra.700/abstract?systemMessage=Wiley+Online+Library+will+be+unavailable+on+Saturday+17th+December+2016+at+09%3A00+GMT%2F+04%3A00+EST%2F+17%3A00+SGT+for+4hrs+due+to+essential+maintenance.Apologies+for+the+inconvenience&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Olden &amp;amp; Poff (2003)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, Table 3-2 in&lt;/span&gt;&lt;a href=&quot;https://pubs.usgs.gov/tm/tm7c7/pdf/TM7_C7_112712.pdf#page=90&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Westenbroek et al (2012)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; or &lt;/span&gt;&lt;a href=&quot;https://pubs.usgs.gov/of/2006/1093/report.pdf&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Henrickson (2006)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Alphanumeric code&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: one of the predefined values (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Indices to calculate&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Code that jointly with htype determines the indiced to calculate (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;list of integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Component&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Used to specify the hydrologic regime as defined in Olden and Poff (2003) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Classification&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Used to specify the hydrologic regime as defined in Olden and Poff (2003) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Median&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Requests that indices that normally report the mean of some other summary static to insteadreport the median value (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Drain area&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The gauge area in m&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Period&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: a tuple of two elements indicating the Begin and End of datetimes records to be used (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Integrate&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Several methods are available for integrating a Sensors time-series with respect to time.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Time units&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The time units of data employed by the time-series (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use time&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: A list of tuples with upper and lower time limits for volumes computation (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Factor&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;:  a factor by which integrated volumes or masses are multiplied before storage generally used for unit conversion (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;flaot&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;how&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Used to specify the hydrologic regime as defined in Olden and Poff (2003) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Dates as text&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: defines if dates have to be returned as text (True) or Timestamp (False) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Resample&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The resample method calculates a new time-series with a given frequency by sampling values of a time-series with a different frequency.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Frequency&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: An alphanumeric code specifying the desired frequency (A=year, M=month, W=week, D=day, H=hour, T=minute, S=second; e.g.: 1H10T) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sampling method&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the sampling method (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;FIll&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: If not null it defines the method for filling no-data (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Limit&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: if not null it defines the maximum numbers of allowed consecutive no-data valuas to be filled (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;How quality&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the sampling method for observation quality index (s&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;tring&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Data values&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The data values method can be used to set uniform data values for a Sensor within time bounds, or to remove values that fall outside of specified value bounds.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the value to be assigned (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use time&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: a flag to determine upper (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Begin position&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) and lower (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;End position&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) time limits for assignment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use limit&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: a flag to determine upper and lower value limits for assignment. The bounds are closed bounds (min &amp;gt;= x &amp;lt;= max) e.g: [(None,0.2),(0.5,1.5),(11,None)] will apply: if data is lower then 0.2; –&amp;gt; (None,0.2); or data is between 0.5 and 1.5 –&amp;gt; (0.5,1.5); or data is higher then 11 –&amp;gt; (11,None)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Set quality&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The set quality method can be used to set uniform quality values for a Sensor within time bounds, or to remove values that fall outside of specified value bounds.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The value to be assigned (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Type of statistics&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: The type of statistics the weight is estimated from (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use time&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: A flag to determine upper (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Begin position&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) and lower (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;End position&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) time limits for assignment (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use limit&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: A flag to determine upper and lower value limits for weigth assignment. The bounds are closed bounds (min &amp;gt;= x &amp;lt;= max) e.g: [(None,0.2),(0.5,1.5),(11,None)] will apply: if data is lower then 0.2 –&amp;gt; (None,0.2) or data is between 0.5 and 1.5 –&amp;gt; (0.5,1.5) or data is higher then 11 –&amp;gt; (11,None)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Calculate statistics&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The calculate statistics method returns some basic satistics for the Sensor time-series. The statistics can be performed for a selected time period and on the data values as well as their associated quality values.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The statistics calculated are:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;count&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: returns the number of data values in the series;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;std&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the standard deviation of the series;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;min&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the minimum value in the series;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;max&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the maximum value in the series;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;50%&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the median value in the series;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;25%&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the first quartile value in the series;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;75%&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the third quartile value in the series;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;mean&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: the mean of the series.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Params:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Data&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: A flag to determinewether to compute statistics of data (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Quality&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: A flag to determine wether to compute statistics of quality (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use time&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: used when setting upper and lower time limits for statistic calculation. The bounds are closed bounds (t0 &amp;gt;= t &amp;lt;= t1) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Subtract&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The subtract method can be used to subtract the data values of a Sensor from the data values of the selected Sensor.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;If not aligned, data values of the two time-series are aligned using the mean with respect to the first term in each series.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sensor&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Name of the sensor to subtract (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Align method&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Align method (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Hargreaves&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The &lt;/span&gt;&lt;a href=&quot;http://www.sciencedirect.com/science/article/pii/S1161030110001103&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Hargreaves-Somani&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; equation can be used to estimate the reference crop evapotransipration using minimum climatological data. In OAT the daily reference evapotransipration can be calculated from a given Sensor with a resolution smaller than 1 day, by clicking the “Execute” button. The parameters needed by the Hargreaves equation are automatically extracted and a new time series is produced with daily evapotransiration values..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;

&lt;head&gt;
    &lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;
    &lt;style type=&quot;text/css&quot;&gt;
        p,
        li {
            white-space: pre-wrap;
        }
    &lt;/style&gt;
&lt;/head&gt;

&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Guida&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Filtro digitale&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Il filtro digitale  by passing an existing Sensor through a digital filter to remove high or low frequency components. (low- and high-pass filters respectively). A high-pass filter removes long-term variations from a time-series, whereas a low-pass filter removes short term variations&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;freqeunza di taglio inferiore&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il punto a 3dB per un filtro passa-batto (in unità di giorni)the 3 dB point of high frequency roll-off for a low-pass filter (in units of day&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;-1&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;frequenza di taglio superiore&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il punto a 3dB per un filtro passa-batto (in unità di giorni) the 3dB point of low frequency roll-off for a high-pass filter (in units of day&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;-1&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;positive float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Ordine&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il numero di stadi del filtro(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer between 1 and 3&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;tipo filtro&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: passa-alto o passa-basso(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Filtro superamento&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Il metodo calcola la probabilità di superamento dopo Searcy (1959) per un particolare sensore&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Unità di tempo superamento&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: l&apos;unità di tempo in cui verrà visualizzata la frequenza di superamento (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sotto&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: modifica il calcolo per specificare quando i valori non sono superati (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Valori superamento&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Un valore per il quale sarà calcolata la probabilità di superamento (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;list of float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Probabilità superamento&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: I valori di portata che corrispondono a una data probabilità di superamento (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;list of float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Fitro riempimento&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Se a un sensore mancano dei dati, o se contiene dei valori da ignorare,è possibile riempirli utilizzando i seguenti metodi:&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;bfill&lt;/span&gt;: riempimento inverso (riempe tutti i valori mancanti con l&apos;ultimo valore disponibile prima dell&apos;intervallo senza dati)&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ffill&lt;/span&gt;: riempimento avanti (riempe tutti i valori mancanti con il primo valore disponibile dopo dell&apos;intervallo senza dati)&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Estrazione eventi idrici&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Il metodo per l&apos;estrazione di eventi idrici può essere utilizzato per estrarre idrogrammi per un periodo di tempo (in giorni) precedenti e successivi a un evento idrologico di picco, come una tempesta. Va definito il valore minimo di picco e la distanza minima tra due picchi (in giorni). Il metodo produce una serie temporale per ogni evento con il nome della serie temporale asseganto.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Giorni precedenti al picco&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il numero di giorni precedenti al picco da includere nell&apos;evento idrico (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Giorni successivi al picco&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il numero di giorni successivi al picco da includere nell&apos;evento idrico (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Min giorni tra due picchi&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Tempo minimo tra due picchi, in giorni (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Suffisso&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: suffisso da dare alla serie temporale generata; il nome sarà nome sensore + suffisso + numero (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Tempo&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: una tupla di due elementi che indica la data iniziale e quella finale da utilizzare per estrarre i picchi (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Separazione idrografica&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Il fitro per la separazione idrologica produce due serie temporali, flusso tempesta e flusso di base, partendo da un deflusso.(&lt;/span&gt;&lt;a
            href=&quot;http://onlinelibrary.wiley.com/doi/10.1002/hyp.5675/abstract;jsessionid=BD9AC71979A38AA7E63A2EE95E4D1C19.f02t02?systemMessage=Wiley+Online+Library+will+be+unavailable+on+Saturday+17th+December+2016+at+09%3A00+GMT%2F+04%3A00+EST%2F+17%3A00+SGT+for+4hrs+due+to+essential+maintenance.Apologies+for+the+inconvenience&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Eckhardt, 2005&lt;/span&gt;&lt;/a&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) o Single Parameter Digital Filter (&lt;/span&gt;&lt;a href=&quot;http://onlinelibrary.wiley.com/doi/10.1029/WR026i007p01465/abstract?systemMessage=Wiley+Online+Library+will+be+unavailable+on+Saturday+17th+December+2016+at+09%3A00+GMT%2F+04%3A00+EST%2F+17%3A00+SGT+for+4hrs+due+to+essential+maintenance.Apologies+for+the+inconvenience&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Nathan &amp;amp; McMahon, 1990&lt;/span&gt;&lt;/a&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) possono essere usati.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Modo&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Seleziona tra TPDF e SPDF (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Alfa&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: tasso di decadimento rispetto al tasso di flusso corrente (valori raccomandati tra 0.92 - 0.98) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Bfl_max&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: rapporto a lungo termine tra deflusso di base e deflusso totale(richiesto con TPDF) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Indici idrologici&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;GLi indici idrologici sono una serie di misure statistiche di deflussi usate per descrivere vari aspetti ecologicamente importanti del regimne di flusso. Possono essere calcolati più di 160 tipi di filtro. Per maggiorni informazioni: &lt;/span&gt;&lt;a
            href=&quot;http://onlinelibrary.wiley.com/doi/10.1002/rra.700/abstract?systemMessage=Wiley+Online+Library+will+be+unavailable+on+Saturday+17th+December+2016+at+09%3A00+GMT%2F+04%3A00+EST%2F+17%3A00+SGT+for+4hrs+due+to+essential+maintenance.Apologies+for+the+inconvenience&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Olden &amp;amp; Poff (2003)&lt;/span&gt;&lt;/a&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;, Tabella 3-2 in&lt;/span&gt;&lt;a href=&quot;https://pubs.usgs.gov/tm/tm7c7/pdf/TM7_C7_112712.pdf#page=90&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Westenbroek et al (2012)&lt;/span&gt;&lt;/a&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; or &lt;/span&gt;&lt;a href=&quot;https://pubs.usgs.gov/of/2006/1093/report.pdf&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Henrickson (2006)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Codice alfanumerico&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: uno dei valori predefiniti (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Indici da calcolare&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Codici che insieme a htype determinano gli indici da calcolare (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;list of integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Componente&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;:Usato per specificare il regime idrologico come definito in Olden and Poff (2003) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Classififcazione&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;:Usato per specificare il regime idorlogico come definito in Olden and Poff (2003) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Mediana&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Richede che gli indici, che normalmente calcolano la media di qualche altra statistica, calcolono la mediana (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Area drenaggio&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: L&apos;area d drenaggio in m&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Periodo&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: una tupla di due elementi che indica la data d&apos;inizio e la fine da utilizzare (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Integrale&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Sono disponibili diversi metodo per integrare la serie temporale di un sensoree.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Unità di tempo&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: L&apos;unità di tempo della serie temporale utilizzata (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Usa tempo&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Una lista di tuple on limiti di tempo superiori e inferioriper i volumi di calcolo (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Factor&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;:  un fattore per il quale i volumi o masse integrate sono moltiplicati, normalmente utilizzati per convertire l&apos;unità di misura (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;flaot&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;how&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Usato per specificare il regime idrologico, come definito in Olden and Poff (2003) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Dates as text&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: definisce se le date sono da convertire a testo (True) o Timestamp (False) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Ricampionamento&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Il metodo ricampionamento calcola una nuova serie temporale con una data frequenza.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Frequenza&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Un codice alfanumerico che definisce la frequenza desiderata (A=anno, M=month, W=settimana, D=giorni, H=ore, T=minuti, S=secondi; es.: 1H10T) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Metodo campionatura&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il metodo di campionatura(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Riempimento&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Se non nullo definische il metodo di riempimento per i dati mancanti (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Limite&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Se non nullo definische il numero massimo di misure mancanti consecutive da riempire (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;integer&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Qualità&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il metodo di sampling per l&apos;indice di qualità della serie temporale (s&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;valori dati&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Questo metodo può essere utilizzato per assegnare valori uniformi a un sensore in un intervallo di tempo preciso, oppure per rimuovere valori che non rientrano in un intervallo predefinito.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Valore&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il valore da assegnare (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Usa tempo&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: una variabile booleana per determinare i limiti di tempo, posizione iniziale (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Posizione iniziale&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) e finale (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Posizione finale&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;) (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Usa limiti&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: un checkbox per determinare i valori limiti entro i quali applicare il processamento. Es: (min &amp;gt;= x &amp;lt;= max)  [(None,0.2),(0.5,1.5),(11,None)] applica: se il dato è mionore di 0.2; –&amp;gt; (None,0.2); se il dato è tra 0.5 e 1.5 –&amp;gt; (0.5,1.5); se il dato è maggiore di 11 –&amp;gt; (11,None)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Qualità&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Questo metodo può essere utilizzato per assegnare valori di qualità uniformi a un sensore in un intervallo di tempo preciso, oppure per rimuovere valori di qualità che non rientrano in un intervallo predefinito. The set quality method can be used to set uniform quality values for a Sensor within time bounds, or to remove values that fall outside of specified value bounds.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Valore&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il valore da assegnare (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;float&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Tipo di statistica&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Il tipo di statistica del peso stimato  (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Use tempo&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;:un checkbox per determinarela i limiti di tempo, (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Posizione iniziale&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)/(&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Posizione finale&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)  (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Usa limiti&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: un checkbox per determinare i valori limiti entro i quali applicare il processamento.Es: (min &amp;gt;= x &amp;lt;= max)  [(None,0.2),(0.5,1.5),(11,None)] applica: se la qualità è minore di 0.2; –&amp;gt; (None,0.2); se la qualità è tra 0.5 e 1.5 –&amp;gt; (0.5,1.5); se la qualità è maggiore di 11 –&amp;gt; (11,None)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Calcola statistiche&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Questo metodo calcola alcune statistiche basilari per le serie temporali.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Le statistiche colcolabili sono:&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;count&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: calcola il numero di misure all&apos;interno della serie temporale&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;std&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: la deviazione standard (?) della serie&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;min&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il valore minimo della serie&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;max&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il valore massimo della serie&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;50%&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il valore mediano della serie&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;25%&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il primo valore quartile nella serie temporale&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;75%&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il terzo valore quartile nella serie temporale&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;mean&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: il valore medio della serie&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Parametri:&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Dati&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: determina se calcolare le statistiche sulla serie di dati (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;bool&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Qualità&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: determina se calcolare le statistiche sulla serie qualità (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Usa date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: se abilitato usa i limiti temporali per calcolare le statistiche (&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;date, date&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Sottrazione&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;La sottrazione può essere utilizzata per sottrarre i valori di una serie temporale di un sensore dalla serie temporale del sensore selezionato&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Se le serie non sono allineate, i valori delle due serie temporali sono allineate utilizzando la media rispetto al primo termine di ogni serie.&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sensore&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Nome del sensore da sottrarre (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Metodo allineamento&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Metodo allineamento (&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;string&lt;/span&gt;&lt;span
            style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;)&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Hargreaves&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Il metodo &lt;/span&gt;&lt;a href=&quot;http://www.sciencedirect.com/science/article/pii/S1161030110001103&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Hargreaves-Somani&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; può essere utilizzato per calcolare l&apos;evapotranspirazione usando un minimio di dati climatologici. &lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;/body&gt;

&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>runPlotCalibration</name>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="26"/>
        <source>Model name</source>
        <translation type="unfinished">Nome modello</translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="14"/>
        <source>Plot Calibration Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="56"/>
        <source>DSS Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="63"/>
        <source>Plot all parameters in same plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="70"/>
        <source>Plot only specific Params</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="80"/>
        <source>Bubble Plot Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="87"/>
        <source>For multiple observations, calculated as:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="93"/>
        <source>Mean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="103"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="110"/>
        <source>Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="149"/>
        <source>Chart Plots:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runPlotCalibration.ui" line="162"/>
        <source>Map Plots:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>runZoneBudget</name>
    <message>
        <location filename="ui_runZoneBudget.ui" line="14"/>
        <source>Run Zone Budget</source>
        <translation>Esegui Zone budget</translation>
    </message>
    <message>
        <location filename="ui_runZoneBudget.ui" line="23"/>
        <source>Model name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_runZoneBudget.ui" line="40"/>
        <source>Zone Layer:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>saveSensorList</name>
    <message>
        <location filename="saveSensorList.ui" line="14"/>
        <source>save sensors</source>
        <translation>salva sensori</translation>
    </message>
    <message>
        <location filename="saveSensorList.ui" line="20"/>
        <source>Sensor list</source>
        <translation>Lista sensori</translation>
    </message>
</context>
<context>
    <name>self.gui</name>
    <message>
        <location filename="istsos.py" line="133"/>
        <source>Problem with server, please check if server is online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsos.py" line="136"/>
        <source>Unknown Error: {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsos.py" line="156"/>
        <source>istSOS connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsos.py" line="156"/>
        <source>Successfully connected to server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsosManager.py" line="175"/>
        <source>please select a istSOS configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsosManager.py" line="189"/>
        <source>Missing data, please select a procedure and an obseved property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsosManager.py" line="247"/>
        <source>An error occur: {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsosManager.py" line="225"/>
        <source>Problem loading sensor, please check input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="rawManager.py" line="132"/>
        <source>Please define at least 1 measure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="rawManager.py" line="159"/>
        <source>Please define a correct date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="listfileManager.py" line="67"/>
        <source>Error occur: 
 {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hobfileManager.py" line="65"/>
        <source>Please insert a valid name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hobfileManager.py" line="69"/>
        <source>Please insert a valid input file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hobfileManager.py" line="73"/>
        <source>Please insert a valid disc file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="listfileManager.py" line="75"/>
        <source>Select listing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hobfileManager.py" line="91"/>
        <source>Select hob_out file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hobfileManager.py" line="94"/>
        <source>Select disc file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="csvManager.py" line="115"/>
        <source>Select input file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="358"/>
        <source>Please select a valid csv file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="365"/>
        <source>Please define a date column!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="csvManager.py" line="218"/>
        <source>An error occurred: 
 Wrong separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="423"/>
        <source>An error occurred:
 {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gageManager.py" line="47"/>
        <source>Please select a valid file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gageManager.py" line="44"/>
        <source>Select GAGE file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="98"/>
        <source>Select input file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="111"/>
        <source>Select Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="132"/>
        <source>Please select a metadata file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="136"/>
        <source>Please select a data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="143"/>
        <source>Error reading metadata file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="147"/>
        <source>No sensor found inside metadata file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="167"/>
        <source>Mandatory fileds are missing (name / prop / unit / lat / lon / altitude )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsos.py" line="261"/>
        <source>The following sensors report errors:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsos.py" line="264"/>
        <source>Sensor: {}
Error : {}

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsos.py" line="268"/>
        <source>Upload done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="istsos.py" line="268"/>
        <source>Sensors loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="323"/>
        <source>Please select a valid data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="334"/>
        <source>Please check input values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="multicsv.py" line="421"/>
        <source>An error occured: 
 Wrong separator</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>sensorCompare</name>
    <message>
        <location filename="sensorCompare.ui" line="14"/>
        <source>Compare sensors</source>
        <translation>Confronta sensori</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="27"/>
        <source>Compare</source>
        <translation>Confronta</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="53"/>
        <source>Sensor</source>
        <translation>Sensore</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="63"/>
        <source>Load sensor</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="79"/>
        <source>Quality</source>
        <translation>Qualità</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="86"/>
        <source>filter</source>
        <translation>filtro</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="103"/>
        <source>Begin position</source>
        <translation>Posizione iniziale</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="128"/>
        <source>yyyy-MM-dd hh:mm:ss</source>
        <translation>yyyy-MM-dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="121"/>
        <source>End position</source>
        <translation>Posizione finale</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="147"/>
        <source>Remove Sensor</source>
        <translation>Rimuovi sensore</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="157"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="232"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="sensorCompare.ui" line="241"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Compare sensor &lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Through this interface it is possible to plot several Sensors in the same window to visually compare them:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A number of sensors can be selected from the drop-down menu and added to the display window with the Load sensor button. If the quality check-box is selected only the quality data is loaded. If the filter option is checked the user have to specify a time interval for which sensor data should be loaded, e.g. when two Sensor to be compared do not have the same starting or ending dates.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Any Sensor loaded into the display can be removed by selecting it from the Remove Sensor drop-down menu and pressing the Remove button.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If required, axes and plotting options can be formatted using the inbuilt matplotlib tools&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;

&lt;head&gt;
    &lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;
    &lt;style type=&quot;text/css&quot;&gt;
        p,
        li {
            white-space: pre-wrap;
        }
    &lt;/style&gt;
&lt;/head&gt;

&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Guida&lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Confronta sensori &lt;/span&gt;&lt;/p&gt;
    &lt;hr /&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Con questa interfaccia è possibile stampare diversi sensori e confrontarli visualmente tra di loro:&lt;/span&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Un certo numero di sensori può essere scelto dal menù a tendina e aggiunto alla finestra, tramite il pulsante Carica sensore. Se la casella qualità è abilitata nel grafico viene anche visualizzata la serie temporale relativa alla qualità. Se è abilitata l&apos;opzione filtro, l&apos;utente deve specificare l&apos;intervallo di tempo da visualizzare.&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ogni sensore aggiunto può essere rimosso selezionandolo dal menù a tendina Rimuovi sensore e premendo il bottone Rimuovi.&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; &lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
    &lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;/body&gt;

&lt;/html&gt;
</translation>
    </message>
</context>
<context>
    <name>sensorManager</name>
    <message>
        <location filename="sensorManager.ui" line="30"/>
        <source>Manage sensors</source>
        <translation>Gestione sensori</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="73"/>
        <source>Sensor</source>
        <translation>Sensore</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="83"/>
        <source>Edit data</source>
        <translation>Modifica dati</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="110"/>
        <source>Save as CSV</source>
        <translation>Salva come CSV</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="117"/>
        <source>Clone</source>
        <translation>Clona</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="124"/>
        <source>Update</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="131"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="177"/>
        <source>Sensor name</source>
        <translation>Nome sensore</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="205"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="225"/>
        <source>Longitude</source>
        <translation>Longitudine</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="276"/>
        <source>Latitude</source>
        <translation>Latitudine</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="309"/>
        <source>Altitude</source>
        <translation>Altitudine</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="336"/>
        <source>Observed property</source>
        <translation>Proprietà osservata</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="360"/>
        <source>Unit of measure</source>
        <translation>Unità di misura</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="384"/>
        <source>Timezone</source>
        <translation>Fuso orario</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="403"/>
        <source>Frequency</source>
        <translation>Frequenza</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="422"/>
        <source>Begin position</source>
        <translation>Posizione iniziale</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="450"/>
        <source>yyyy-MM-dd hh:mm:ss</source>
        <translation>yyyy-MM-dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="443"/>
        <source>End position</source>
        <translation>Posizione finale</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="467"/>
        <source>Top screen</source>
        <translation>Top screen</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="497"/>
        <source>Bottom screen</source>
        <translation>Bottom screen</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="527"/>
        <source>stat flag</source>
        <translation>stat flag</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="537"/>
        <source>Use</source>
        <translation>Usa</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="552"/>
        <source>See on QGIS</source>
        <translation>VIsualizza in QGIS</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="626"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="sensorManager.ui" line="635"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Manage sensor &lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The sensor manager is divided into three frames:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Metadata Frame&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: displays the metadata for the selected Sensor&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sensor preview frame&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: an automatically loaded preview chart of the Sensor data&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Sensor data frame&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: frame to display the data table for the selected Sensor&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Using the Manage sensor interface it&apos;s possible to act the with the following buttons: &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Save as CSV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: export the sensor data as a new CSV file&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Update&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: update the metadata of the selected sensor&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Delete&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: remove the selected sensor&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Clone&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: create a copy of the sensor&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;See on QGIS&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: navigate to your QGIS map and zoom to the selected sensor&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Guida&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Gestione sensore &lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;La gestione dei sensori è divisa in tre frames:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Metadati&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: visualizza i metadati del sensore selezionato&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Anteprima&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: un grafico con la serie temporale del sensore&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Dati sensore&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Area per visualizzare i dati del sensore in una tabella&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Usando questa interfaccia è possible esegure le seguenti azioni: &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Salva come CSV&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: esporta i dati del sensore come file csv&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Modifica&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: modificare i metadati del sensore selezionato&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Elimina&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: eliminare il sensore selezionato&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Clona&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: creare una copia del sensore selezionato&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Seleziona in QGIS&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: visualizza sensore in QGIS&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>sosServer</name>
    <message>
        <location filename="sosServer.ui" line="14"/>
        <source>istSOS server</source>
        <translation>istSOS server</translation>
    </message>
    <message>
        <location filename="sosServer.ui" line="20"/>
        <source>Connection information</source>
        <translation>Informatzioni connessione</translation>
    </message>
    <message>
        <location filename="sosServer.ui" line="29"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="sosServer.ui" line="56"/>
        <source>Service</source>
        <translation>Servizio</translation>
    </message>
    <message>
        <location filename="sosServer.ui" line="83"/>
        <source>Use auth</source>
        <translation>Usa aut</translation>
    </message>
    <message>
        <location filename="sosServer.ui" line="113"/>
        <source>User</source>
        <translation>Utente</translation>
    </message>
    <message>
        <location filename="sosServer.ui" line="143"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
</context>
<context>
    <name>viewBudget</name>
    <message>
        <location filename="ui_viewBudget.ui" line="14"/>
        <source>Visualize Model Budget</source>
        <translation>Visualizza bilancio idrico</translation>
    </message>
    <message>
        <location filename="ui_viewBudget.ui" line="32"/>
        <source>Plot a bar chart of volumetric budget </source>
        <translation>Mostra istogramma del bilancio idrico</translation>
    </message>
    <message>
        <location filename="ui_viewBudget.ui" line="41"/>
        <source>Model Name</source>
        <translation>Nome modello</translation>
    </message>
    <message>
        <location filename="ui_viewBudget.ui" line="58"/>
        <source>Select Stress Period</source>
        <translation>Seleziona Stress Period</translation>
    </message>
</context>
<context>
    <name>viewCrossSection</name>
    <message>
        <location filename="ui_viewCrossSection.ui" line="14"/>
        <source>View Model Cross Section</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="27"/>
        <source>View Cross Section </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="57"/>
        <source>Flow Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="83"/>
        <source>Transport Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="106"/>
        <source>Species n.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="219"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="138"/>
        <source>COL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="143"/>
        <source>ROW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="166"/>
        <source>Stress Period:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="186"/>
        <source>Time Step:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="212"/>
        <source>Vertical exageration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="228"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_viewCrossSection.ui" line="234"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic; color:#0055ff;&quot;&gt;Note&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#0055ff;&quot;&gt;this tool is still in development. User will get a reasonable output only for model with flat top and bottom surfaces, and without &amp;quot;no data&amp;quot; values in the model output&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The User can choose to visualize model outputs for a &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Flow Model  &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;(the simulated hydraulic head for each model layer is produced)&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;or for a &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Transport Model  &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;(the simulated solute concentration for each model layer is produced). In the last case, the User must specify for which among the simulated species the concentration will be visualized&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Select &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;COL &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;option if the desired section must be visualized fixing a specific COL of the model. Then, input the specific COL selected&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Alternatevely, select &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;ROW  &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;option if the desired section must be visualized fixing a specific ROW of the model. Then, input the specific ROW selected&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Stress Period&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: Stress Period number for which the simulated output will be loaded&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Time Step&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: Time Step number, within the selected Stress Period, for which the simulated output will be loaded&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For further information, refer to FREEWAT User Manual (Volume 1, section 8.1). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>viewOutput</name>
    <message>
        <location filename="ui_viewOutput.ui" line="27"/>
        <source>View Model Output</source>
        <translation>Vedi output del modello</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="174"/>
        <source>Select Single Time Step</source>
        <translation>Seleziona singolo Time Step</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="229"/>
        <source>Select Stress Period</source>
        <translation>Seleziona Stress Period</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="280"/>
        <source>Help</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="252"/>
        <source>Stress Period:</source>
        <translation>Stress Period:</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="198"/>
        <source>Time Step:</source>
        <translation>Time Step:</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="35"/>
        <source>Flow Model</source>
        <translation>Modello di flusso</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="61"/>
        <source>Transport Model</source>
        <translation>Modello di trasporto</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="84"/>
        <source>Species n.</source>
        <translation>Specie n.</translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="97"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="120"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="132"/>
        <source>Raster format is suggested, but if you have a rotated grid, vector format is needed !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="144"/>
        <source>Raster (1 file for each layer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="154"/>
        <source>Vector (1 grid for all layer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="241"/>
        <source>CAUTION! You get a layer for EACH time step
of EACH stress period selected...
this can take a long time ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_viewOutput.ui" line="286"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;Help&lt;/span&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;The User can choose to visualize model outputs for a &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Flow Model &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;(the simulated hydraulic head for each model layer is produced)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;or for a &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Transport Model &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;(the simulated solute concentration for each model layer is produced). In the last case, the User must specify for which among the simulated species the concentration will be visualized&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;In the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Select Single Time Step&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; section (it must be checked if used):&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Stress Period&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Stress Period number for which the simulated output will be loaded&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;Time Step&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;: Time Step number, within the selected Stress Period, for which the simulated output will be loaded&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;Alternatively, in the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt; font-weight:600;&quot;&gt;Select Stress Period(s)&lt;/span&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt; section (it must be checked if used), the User can specify more than one Stress Period for which the simulated output will be loaded&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:10pt;&quot;&gt;For further information, refer to FREEWAT User Manual (Volume 1, section 8.1). &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>windows</name>
    <message>
        <location filename="windows.ui" line="14"/>
        <source>Windows installer</source>
        <translation>Installer windows</translation>
    </message>
    <message>
        <location filename="windows.ui" line="23"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Install missing dependencies&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In order to run the FREEWAT plugin is necessary to install some dependencies:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;pip,flopy, numpy, pandas, requests, isodate, seaborn, xlwt, xlrd&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please confirm with &apos;Yes&apos; to the next pop-up&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Installa dipendenze mancanti&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Per poter eseguire FREEWAT è necessario installare le seguenti dipendenze:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;pip,flopy, numpy, pandas, requests, isodate, seaborn, xlwt, xlrd&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Per favore conferma con &apos;Si&apos;alle prossime finestre&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="windows.ui" line="48"/>
        <source>Use proxy</source>
        <translation>Usa proxy</translation>
    </message>
    <message>
        <location filename="windows.ui" line="81"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="windows.ui" line="91"/>
        <source>port</source>
        <translation>porta</translation>
    </message>
    <message>
        <location filename="windows.ui" line="115"/>
        <source>User autentication</source>
        <translation>Autenticazione</translation>
    </message>
    <message>
        <location filename="windows.ui" line="142"/>
        <source>User</source>
        <translation>Utente</translation>
    </message>
    <message>
        <location filename="windows.ui" line="159"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
</context>
</TS>
