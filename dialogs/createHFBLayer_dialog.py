# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2019 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************


from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
import os
import numpy as np
from PyQt4 import QtGui, uic
from freewat.freewat_utils import getVectorLayerByName, getVectorLayerNames, getModelsInfoLists, getModelInfoByName
from freewat.mdoCreate_utils  import createHbfLayer
from freewat.sqlite_utils import getTableNamesList
#
#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_createHFBLayer.ui') )
#
class CreateHFBLayerDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.createHBF)
        self.label.setEnabled(False)
        self.cmbLinearLayer.setEnabled(False)
        self.manageGui()
##
##
    def manageGui(self):
        self.cmbModelName.clear()
        self.cmbGridLayer.clear()
        self.cmbLinearLayer.clear()
        layerNameList = getVectorLayerNames()
        layerNameList.sort()
        # Retrieve modelname and pathfile List
        (modelNameList, pathList) =  getModelsInfoLists(layerNameList)
        self.cmbModelName.addItems(modelNameList)

        # fill the Grid combobox with only the _grid layer
        grid_layers = []
        for nametemp in layerNameList:
            if '_grid' in nametemp:
                grid_layers.append(nametemp)
        self.cmbGridLayer.addItems(grid_layers)

        # create a list of only vector line layers loaded in the TOC
        layerList = QgsMapLayerRegistry.instance().mapLayers()
        lineList = []
        for k, v in layerList.items():
            # check if vector
            if v.type() == 0:
                # check if line
                if v.wkbType() == 2:
                    lineList.append(v.name())

        for l in lineList:
            self.cmbLinearLayer.addItem(l)

    def reject(self):
        QDialog.reject(self)
##
    def restoreGui(self):
        self.progressBar.setFormat("%p%")
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(0)
        self.cancelButton.clicked.disconnect(self.stopProcessing)
        self.okButton.setEnabled(True)
##
    def createHBF(self):

        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.setValue(0)

        QApplication.processEvents()

        # ------------ Load input data  ------------
        modelName = self.cmbModelName.currentText()
        gridLayer = getVectorLayerByName(self.cmbGridLayer.currentText())
        lineLayer = getVectorLayerByName(self.cmbLinearLayer.currentText())

        # Remark: pathfile from model table and number of stress periods (nsp) from time table
        (pathfile, nsp ) = getModelInfoByName(modelName)
        # crs
        for ft in getVectorLayerByName('modeltable_'+modelName).getFeatures():
            crs = ft['crs']


        # Retrieve the name of the new layer from Text Box
        name = self.textEdit.text()

        # Retrieve the information of the model and the name
        dbName = os.path.join(pathfile, modelName + '.sqlite')
        tableList = getTableNamesList(dbName)
        layerName = name + "_hfb"

        if layerName in tableList:
            QMessageBox.warning(self, self.tr('Warning!'), self.tr('Table %s already exists!' % layerName))
            return


        if self.line_check.isChecked():
            for i in gridLayer.getFeatures():
                for l in lineLayer.getFeatures():
                    if i.geometry().intersects(l.geometry()):
                        gridLayer.select(i.id())

        # Create HBF layer
        createHbfLayer(gridLayer, name, pathfile, modelName)

        # attempt to fill out the values
        vl = getVectorLayerByName(layerName = name + "_hfb")
        #  Start editing to write record
        vl.startEditing()
        ft = [f for f in vl.getFeatures()]
        # define the line mamory layer
        discLine = QgsVectorLayer("LineString?crs=" + crs, "temporary_line", "memory")
        discLine.dataProvider().addAttributes([QgsField("id",  QVariant.Int)])
        discLine.updateFields()
        discLineFeatList = []
        #
        changedAttributesDict = {}
        barrierFeatures = [f for f in lineLayer.getFeatures()]

        for ff in ft[:-1]:
            gridPoly = ff.geometry()
            cellRow = ff['ROW']
            cellCol = ff['COL']
            p = gridPoly.centroid().asPoint()
            xc, yc = p[0], p[1]
            cellPoints= gridPoly.asMultiPolygon()[0][0]
            p1,p2,p3,p4 = cellPoints[0],cellPoints[1],cellPoints[2],cellPoints[3],
            for bf in barrierFeatures:
                barrierLine = bf.geometry()
                if barrierLine.intersects(gridPoly):
                    # get the 2 vertex of the intersection line
                    inCellBarrier = barrierLine.intersection(gridPoly)
                    if inCellBarrier.wkbType() == QGis.WKBLineString:
                        linepoints = inCellBarrier.asPolyline()
                        # first point
                        pointIn = linepoints[0]
                        # last point
                        pointOut = linepoints[len(linepoints)-1]
                    elif inCellBarrier.wkbType() == QGis.WKBMultiLineString:
                        multilinepoints = inCellBarrier.asMultiPolyline()
                        # first point of first list
                        pointIn = multilinepoints[0][0]
                        lastGroup = multilinepoints[len(multilinepoints)-1]
                        # last point of last list
                        pointOut = lastGroup[len(lastGroup)-1]
                    # ..... from  here, follow the ModelMuse approach:
                    x1, y1 = pointIn[0], pointIn[1]
                    x2, y2 = pointOut[0], pointOut[1]
                    xLineMin = np.minimum(x1,x2)
                    xLineMax = np.maximum(x1,x2)
                    yLineMin = np.minimum(y1,y2)
                    yLineMax = np.maximum(y1,y2)
                    linePosition = None
                    if xLineMin < xc and xLineMax > xc: # the line is somehow transversal
                        linePosition = 'transversal'
                    if yLineMin < yc and yLineMax > yc:
                        linePosition = 'vertical'

                    if linePosition == 'transversal':
                        # evaluate y related to xc along the straightline
                        y = (y2-y1)*(xc-x1)/(x2-x1) + y1
                        if y > yc : # we are above the centroid
                            crossRow = cellRow - 1
                            newP1 = QgsPoint(p1[0],p1[1])
                            newP2 = QgsPoint(p2[0],p2[1])
                        else: # we are below the centroid
                            crossRow = cellRow + 1
                            newP1 = QgsPoint(p4[0],p4[1])
                            newP2 = QgsPoint(p3[0],p3[1])
                        # # assign adjacent cells values
                        row1 = cellRow
                        row2 = crossRow
                        col1 = cellCol
                        col2 = cellCol
                    if linePosition == 'vertical': # the line is somehow vertical
                        # evaluate x related to yc along the straightline
                        x = (x2-x1)*(yc-y1)/(y2-y1) + x1
                        if x > xc : # we are on the right w.r.t. the centroid
                            crossCol = cellCol + 1
                            newP1 = QgsPoint(p3[0],p3[1])
                            newP2 = QgsPoint(p2[0],p2[1])
                        else: # we are on the left w.r.t. the centroid
                            crossCol = cellCol - 1
                            newP1 = QgsPoint(p4[0],p4[1])
                            newP2 = QgsPoint(p1[0],p1[1])
                        # # assign adjacent cells values
                        row1 = cellRow
                        row2 = cellRow
                        col1 = cellCol
                        col2 = crossCol
                    if linePosition is None:
                        row1 = -99999
                        row2 = -99999
                        col1 = -99999
                        col2 = -99999

                    # select values according to the position
                    changedAttributes = {}
                    # Insert value to layer field (layer)
                    fieldIdx = vl.dataProvider().fieldNameIndex('row1')
                    changedAttributes[fieldIdx] = row1
                    fieldIdx = vl.dataProvider().fieldNameIndex('col1')
                    changedAttributes[fieldIdx] = col1
                    fieldIdx = vl.dataProvider().fieldNameIndex('row2')
                    changedAttributes[fieldIdx] = row2
                    fieldIdx = vl.dataProvider().fieldNameIndex('col2')
                    changedAttributes[fieldIdx] = col2
                    #
                    changedAttributesDict[ff.id()] = changedAttributes
                    #
                    if linePosition is not None: # define new segment for the HFB discretized line
                        feat = QgsFeature()
                        gLine = QgsGeometry.fromPolyline([newP1,newP2])
                        feat.setGeometry(gLine)
                        feat.setAttributes([ff.id()])
                        # append to list of linear features to be added
                        discLineFeatList.append(feat)

        vl.dataProvider().changeAttributeValues(changedAttributesDict)
        vl.commitChanges()
        # add features to the linear layer in memory
        (res, outFeats) = discLine.dataProvider().addFeatures(discLineFeatList)
        QgsMapLayerRegistry.instance().addMapLayer(discLine)
        self.progressBar.setMaximum(100)

        #Close the dialog window after the execution of the algorithm
        self.reject()
