# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2019 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
import os
from PyQt4 import QtGui, uic
from freewat.freewat_utils import getVectorLayerByName, getVectorLayerNames, getModelsInfoLists, getModelInfoByName, getModelNlayByName, getTransportModelsByName, ComboStyledItemDelegate
# load flopy addironal modules
# FloPy Add-On
import flopyVtk
from flopyVtk.export import vtk as fv
# Import the code for the dialog
from flopyVtk.modflow.mf import Modflow
from flopyVtk.utils import *
#from flopyVtk.modflow.mf import Modflow
#from flopyVtk.utils import *
import numpy as np
# improt FREEWAT sub-moduls
import freewat.createGrid_utils
#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_exportVtk.ui') )
#
class exportVtkDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)

        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.export)
        # self.cancelButton.clicked.connect(self.stopProcessing)
        self.manageGui()
##
##
    def manageGui(self):
        self.cmbModelName.clear()
        layerNameList = getVectorLayerNames()
        (modelNameList, pathList) =  getModelsInfoLists(layerNameList)
        self.cmbModelName.addItems(modelNameList)

##
    def export(self, vector_output = False):
        self.progressBar.setValue(0)
        # ------------ Load input data  ------------
        modelName = self.cmbModelName.currentText()
        self.modelName = modelName
        #

        # Remark: model_ws from modelltable
        layerNameList = getVectorLayerNames()

        for mName in layerNameList:
            if mName == 'modeltable_'+ modelName:
                modelNameTable = getVectorLayerByName(mName)
                for f in modelNameTable.getFeatures():
                    model_ws = f['working_dir']

        # Retrieve LPF table, and from there model layers and data needed for LPF
            if mName ==  "lpf_"+ modelName:
                lpftable = getVectorLayerByName(mName)
                # Number of layers
                nlay = 0
                # Get layers name from LPF table
                dpLPF = lpftable.dataProvider()

                # Create lists of layers properties
                layNameList = []

                for ft in lpftable.getFeatures():
                    attrs = ft.attributes()
                    layNameList.append(attrs[0])
                    nlay = nlay + 1

        # Retrieve the basis of the grid for Extent etc.
        #layerGrid = getVectorLayerByName(layNameList[0])
        # Spatial discretization:

        # --- Post processing
        nam_file = modelName+".nam"
        ml = Modflow.load(nam_file, model_ws=model_ws, check=False)
        #
        #
        # DIS
##        ml.dis.sr.proj4_str = proj4_str
##        ml.dis.sr.xul = xul
##        ml.dis.sr.yul = yul
##        ml.dis.sr.rotation = rotation
        nrow, ncol, nlay = ml.nrow , ml.ncol, ml.nlay
        # read Head
        fileHds =  modelName + '.hds'
        hdobj = HeadFile(os.path.join(model_ws, fileHds), text = 'head', precision='single')
        kstpkper = hdobj.get_kstpkper()
        kstp = 0
        kper = 0
        harray = hdobj.get_data(kstpkper = (kstp, kper))

        #
        if not os.path.isdir(os.path.join(model_ws, 'vtk_grid')):
            os.makedirs(os.path.join(model_ws, 'vtk_grid'))
        vtkfileName = os.path.join(model_ws, 'vtk_grid', 'vtk_grid.vtu')
        vtkobj = fv.Vtk(vtkfileName, ml)
        # store the array from the MODFLOW LPF package
        hk = ml.lpf.hk
        vtkobj.add_array('hk', hk)
        # add BOTTOM
        BTM = ml.dis.botm.array
        vtkobj.add_array('bottom', BTM)
        # create np.array of layers number:
        layNum = np.zeros(shape = BTM.shape, dtype = int)
        for l in range(nlay):
            layNum[l] = (l+1)*np.ones(shape = (nrow,ncol), dtype = int)
        vtkobj.add_array('laynum', layNum)
        #
        vtkobj.write(ibound_filter=True)
        #
        kstpkper = hdobj.get_kstpkper()


        if self.radioLast.isChecked():
            # The User wants anly the last time step for each stress period!
            # So redefine the kstpkper (list of tuples):
            kstpkper = [(ml.dis.nstp[k] - 1, k) for k in range(ml.dis.nper)]
            nper = ml.dis.nper

        # set bar increment
        percBar = 100/len(kstpkper)
        self.progressBar.setValue(0)
        #



        # --- Heads
        # Example: get the last time step of each stress period
        # create a sub-folder
        if not os.path.isdir(os.path.join(model_ws, 'vtu_head')):
            os.makedirs(os.path.join(model_ws, 'vtu_head'))
        it = 0
        self.progressBar.setValue(0)
        print(kstpkper)
        for kstep, kper in kstpkper:
            it += 1
            vtkfileName = os.path.join(model_ws, 'vtu_head', 'vtk_head.{}.vtu'.format(it))
            vtkobj = fv.Vtk(vtkfileName, ml)
            harray = hdobj.get_data(kstpkper = (kstep, kper))
            vtkobj.add_array('head', harray)
            vtkobj.add_array('laynum', layNum)
            vtkobj.write(ibound_filter=True)
            #
            # update bar
            self.progressBar.setValue(it*percBar)
        #

        # --- Wells
        # the array stores (for each SP, values for well presenze. 0- no well, 1 = injection well, -1 = pumping well
        if ml.wel is not None:
            if not os.path.isdir(os.path.join(model_ws, 'vtu_well')):
                os.makedirs(os.path.join(model_ws, 'vtu_well'))
            #for kper in list(ml.wel.stress_period_data.data.keys()): # the list of tuples for this SP
            it = 0
            self.progressBar.setValue(0)
            for kstep, kper in kstpkper:
                it += 1
                wlArray = np.zeros(shape = (nlay,nrow,ncol))
                pumpingArray = np.zeros(shape = (nlay,nrow,ncol))
                vtkfileName = os.path.join(model_ws, 'vtu_well', 'vtk_well.{}.vtu'.format(it))
                vtkobj = fv.Vtk(vtkfileName, ml)
                wl = ml.wel.stress_period_data.data[kper]
                for v in wl: # the 4-tuple for (lay,row,col,pumping_rate)
                    pumpingArray[int(v[0]),int(v[1]),int(v[2])] = v[3]
                    if v[3] > 0:
                        wlArray[int(v[0]),int(v[1]),int(v[2])] = 1
                    if v[3] < 0:
                        wlArray[int(v[0]),int(v[1]),int(v[2])] = -1.0
                # write VTU file
                vtkobj.add_array('wells', wlArray)
                vtkobj.add_array('rate', pumpingArray)
                vtkobj.write(ibound_filter=True)
                # update bar
                self.progressBar.setValue(it*percBar)

        # --------- SWI2
        zfileName = os.path.join(os.path.join(model_ws, modelName +'.zta'))
        # check if SWI2 is included (and the run was good!)
        if os.path.isfile(zfileName):
            zfile = CellBudgetFile(zfileName)
            if not os.path.isdir(os.path.join(model_ws, 'vtu_zeta_surf')):
                os.makedirs(os.path.join(model_ws, 'vtu_zeta_surf'))
            it = 0
            self.progressBar.setValue(0)
            for kstep, kper in kstpkper:
                it += 1
                # this is the list of zeta (len is NSRF):
                zall = zfile.get_data(kstpkper=(kstep, kper), text='ZETASRF  1')
                # but here we assume NSFR = 1, so:
                zeta = zall[0]
                # zeta is now collecting 1 surface for each layer
                # zeta is the aray shaped (nlay,nrow,ncol) at tstep = kstpkper[0], stress period = kstpkper[1]
                vtkfileName = os.path.join(model_ws, 'vtu_zeta_surf', 'vtk_zeta_surf.{}.vtu'.format(it))
                vtkobj = fv.Vtk(vtkfileName, ml)
                vtkobj.add_array('salt_fresh_surface', zeta)
                vtkobj.add_array('laynum', layNum)
                vtkobj.write(ibound_filter=True)
                # update bar
                self.progressBar.setValue(it*percBar)
        #
        # Close the dialog
        QDialog.reject(self)
