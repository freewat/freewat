# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2019 - Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************

from PyQt4.QtCore import *
from PyQt4.QtCore import QSettings
from PyQt4.QtGui import *
from qgis.gui import QgsGenericProjectionSelector
import os
from PyQt4 import QtGui, uic
#
import numpy as np
#
# Import from flopy
from flopy.modflow import Modflow
#

# module shapefile is saved under freewat module
from freewat.shapefile import Shape, Writer
from freewat.sqlite_utils import checkIfTableExists, uploadQgisVectorLayer
from freewat.freewat_utils import *
from freewat.freewat_utils import createAttributes
from freewat.ftools_utils import writeVectorLayerToShape
from freewat.mdoCreate_utils import createModel
import datetime
from pyspatialite import dbapi2 as sqlite3

# ---- This is taken from export module of Flopy, which some time seems not working properly.
def write_grid_shapefile(filename, sr, array_dict, nan_val=-1.0e9, selectedCell = None):
    """
    Write a grid shapefile array_dict attributes.

    Parameters
    ----------
    filename : string
        name of the shapefile to write
    sr : spatial reference instance
        spatial reference object for model grid
    array_dict : dict
       Dictionary of name and 2D array pairs.  Additional 2D arrays to add as
       attributes to the grid shapefile.

    selectedCell : if not None, is an array (nrow, ncol) with value = 1 for
       cells to be include, 0 otherwise.

    Returns
    -------
    None

    """

    try:
        import shapefile
    except Exception as e:
        raise Exception("io.to_shapefile(): error " +
                        "importing shapefile - try pip install pyshp")

    wr = shapefile.Writer(filename, shapeType=shapefile.POLYGON)
    wr.field("ROW", "N", 10, 0)
    wr.field("COL", "N", 10, 0)

    # manage selcetion option
    if selectedCell is None:
        selectedCell = np.ones(shape = (sr.nrow, sr.ncol))

    arrays = []
    names = list(array_dict.keys())
    names.sort()
    # for name,array in array_dict.items():
    for name in names:
        array = array_dict[name]
        if array.ndim == 3:
            assert array.shape[0] == 1
            array = array[0, :, :]
        assert array.shape == (sr.nrow, sr.ncol)
        array[np.where(np.isnan(array))] = nan_val
        if array.dtype in [np.int,np.int32,np.int64]:
            wr.field(name, "N", 10, 0)
        else:
            wr.field(name, "N", 20, 12)
        arrays.append(array)

    for i in range(sr.nrow):
        for j in range(sr.ncol):
            if selectedCell[i,j] > 0:
                pts = sr.get_vertices(i, j)
                wr.poly([pts])
                rec = [i + 1, j + 1]
                for array in arrays:
                    rec.append(array[i, j])
                wr.record(*rec)



#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_importModflow.ui') )

#class CreateModelDialog(QDialog, Ui_CreateModelDialog):
class ImportModflowModelDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)

        self.OutFilePath = None
        self.encoding = None
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.importModel)

        self.btnModflowDir.clicked.connect(self.modflowFile)
        self.btnWorkingDir.clicked.connect(self.workingDir)

    def modflowFile(self):
        (self.InputFilePath) = fileDialog(self)
        self.txtModflowFile.setText(str(self.InputFilePath))
        if self.InputFilePath is None or self.encoding is None:
            return

    def workingDir(self):
        (self.OutFilePath, self.encoding) = dirDialog(self)
        #see the current working directory in the text box
        self.txtDirectory.setText(self.OutFilePath)
        if self.OutFilePath is None or self.encoding is None:
            return

##    def restoreGui(self):
##        self.progressBar.setFormat("%p%")
##        self.progressBar.setRange(0, 1)
##        self.progressBar.setValue(0)
##        self.cancelButton.clicked.disconnect(self.stopProcessing)
##        self.okButton.setEnabled(True)

    # ----
    def createHobLayer(self, obs_data):
        # --
        import shapefile
        #
        gridLayer = self.gridLayer
        dbName = self.dbname
        crs = self.crs
        #
        provider = gridLayer.dataProvider()
        # a memory copy of the grid:
        grid = QgsVectorLayer("Polygon?crs=" + crs, "temporary_layer", "memory")
        pr = grid.dataProvider()
        ############
        filepath = os.path.join(self.shapes_dir, 'model_hob')
        wr = shapefile.Writer(filepath, shapeType=shapefile.POLYGON)
        wr.field("ROW", "N", 10, 0)
        wr.field("COL", "N", 10, 0)
        wr.field("WELLNAME", "C", 50, 12 )
        wr.field("OBSNAM", "C", 50, 12)
        wr.field("WEIGHT", "N", 20, 12)
        wr.field("STATISTICS", "C", 50, 12)
        wr.field("from_lay", "N", 10, 0)
        wr.field("to_lay", "N", 10, 0)
        wr.field("HOBS", "N", 20, 12)
        wr.field("ROFF", "N", 20, 12)
        wr.field("COFF", "N", 20, 12)
        for i in range(1,self.nlay+1):
            wr.field('PR_'+ str(i), "N", 10, 0 )
        wr.field("IREFSP", "N", 20, 12 )
        wr.field("TOFFSET", "N", 20, 12)
        #
        for oo in obs_data:
            i,j = oo.row, oo.column
            pts = self.sr.get_vertices(i, j)
            wr.poly([pts])
            rec = [i + 1, j + 1]
            #
            roff = oo.roff + 1
            coff = oo.coff + 1
            from_lay = list(oo.mlay.keys())[0] + 1
            to_lay = list(oo.mlay.keys())[-1] + 1
            t_data = oo.time_series_data
            #
            rec.append(oo.obsname)
            rec.append(oo.obsname)
            rec.append(1.0)
            rec.append('SD')
            rec.append(from_lay)
            rec.append(to_lay)
            rec.append(t_data['hobs'][0])
            rec.append(roff)
            rec.append(coff)
            rec.append(1.0)
            rec.append(t_data['irefsp'][0])
            rec.append(t_data['toffset'][0])
            wr.record(*rec)

##
##
##        # fields of original grid layer
##        fld = gridLayer.dataProvider().fields()
##        for f in fld:
##        	pr.addAttributes([f])
##        # features from the original grid layer
##        for feature in processing.features(gridLayer):
##            att = feature.attributes()
##            pr.addFeatures([feature])
##
##        grid.startEditing()
##        grid.commitChanges()
##
##        # Add the new field 'NAME'
##        newfield = QgsField(, QVariant.String)
##        pr.addAttributes( [newfield] )
##
##        newfield = QgsField('OBSNAM', QVariant.String)
##        pr.addAttributes( [newfield] )
##        newfield = QgsField('WEIGHT', QVariant.Double)
##        pr.addAttributes( [newfield] )
##        newfield = QgsField('STATISTICS', QVariant.String)
##        pr.addAttributes( [newfield] )
##
##
##        # Add the new field 'from_lay', 'to_lay'
##        newfield = QgsField('from_lay', QVariant.Int)
##        pr.addAttributes( [newfield] )
##        newfield = QgsField('to_lay', QVariant.Int)
##        pr.addAttributes( [newfield] )
##
##        # Define List of new fields of type Double
##        fieldsList = ['HOBS','ROFF','COFF']
##        # Add PR_(n) where n is the number of layer
##        for i in range(1,self.nlay+1):
##            fieldsList.append('PR_'+ str(i) )
##        #
##        fieldsList.append('IREFSP')
##        fieldsList.append('TOFFSET')
##
##        # Call the method to write new fields of type Double
##        createAttributes(grid, fieldsList)
##
##        #  Start editing to write record
##        grid.startEditing()
##
##        changedAttributesDict = {}
##        #
##        ft_sel_id = []
##        ft_dict = {}
##        for ft in grid.getFeatures():
##            for oo in obs_data:
##
##                if oo.row == (ft['ROW'] - 1) and oo.column == (ft['COL'] - 1):
##                    # Prepare dictionary to insert values within new fields
##                    changedAttributes = {}
##                    #
##                    myCellId = ft.id()
##                    ft_sel_id.append(myCellId)
##
##
##                    # Insert value to PR field (multi-layer not supported so far!!)
##                    fieldIdx = grid.dataProvider().fieldNameIndex('PR_' + str(from_lay))
##                    changedAttributes[fieldIdx] = 1.0
##                    #
##                    fieldIdx = grid.dataProvider().fieldNameIndex('from_lay')
##                    changedAttributes[fieldIdx] = from_lay
##                    #
##                    fieldIdx = grid.dataProvider().fieldNameIndex('to_lay')
##                    changedAttributes[fieldIdx] = to_lay
##                    #
##                    fieldIdx = grid.dataProvider().fieldNameIndex('ROFF')
##                    changedAttributes[fieldIdx] = roff
##                    #
##                    fieldIdx = grid.dataProvider().fieldNameIndex('COFF')
##                    changedAttributes[fieldIdx] = coff
##                    #
##                    fieldIdx = grid.dataProvider().fieldNameIndex('WELLNAME')
##                    changedAttributes[fieldIdx] = str(oo.obsname)
##                    #
##                    changedAttributesDict[ft.id()] = changedAttributes
##
##                    # Dictionary to store (possible) multiple cells
##                    if ft.id() not in ft_dict.keys():
##                        ft_dict[ft.id()] = [[ft, changedAttributes]]
##                    else:
##                        ft_dict[ft.id()].append([ft, changedAttributes])
##
##
##        grid.dataProvider().changeAttributeValues(changedAttributesDict)
##        grid.commitChanges()
##
##        # Select only intersected cells (whose IDs are in ft_sel_id)
##        grid.select(ft_sel_id)
##
##        # Upload the vlayer into DB SQlite
##        uploadQgisVectorLayer(dbName, grid, newName + 'temp_hob', srid=grid.crs().postgisSrid(), selected=True)
##
##        # Retrieve the Spatialite layer and add it to mapp
##        uri = QgsDataSourceURI()
##        uri.setDatabase(dbName)
##        schema = ''
##        table = newName + 'temp_hob'
##        geom_column = "Geometry"
##        uri.setDataSource(schema, table, geom_column)
##        display_name = table
##
##        tmp_table = '{}temp_hob'.format(newName)
##
##        wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
##        #
##        del grid
##        grid = wlayer
##        grid.startEditing()
##        # Now, add (possible) multiple cells.
##        nfeats = grid.featureCount()
##        for j, f in enumerate(ft_dict.keys()):
##            if len(ft_dict[f]) > 1:
##                # this is a multi-cell feature - take all the cells except the last one (already considered!)
##                for i, ff in enumerate(ft_dict[f][:-1]):
##                    ## -- Create futures as copies of this feature
##                    # Values of dictionary are list of 2-sized list: [feature, changedAttributesDictionary]
##                    feat = QgsFeature(grid.fields())
##
##                    # Update the features counter
##                    nfeats += 1
##                    feat.id = nfeats
##
##                    # feat.setGeometry(new_geometry)
##                    feat.setGeometry(ff[0].geometry())
##
##                    # Here we need to change attributes 0, which is the primary key
##                    attributesList = ff[0].attributes()
##                    attributesList[0] = attributesList[0] + nfeats
##                    # Set attributes
##                    feat.setAttributes(attributesList)
##
##                    # Add attributes stored in the dictionary
##                    for fieldIdx in ff[1].keys():
##                        feat.setAttribute(fieldIdx, ff[1][fieldIdx])
##
##                    # Add feature
##                    (res, outFeats) = grid.dataProvider().addFeatures( [ feat ] )
##                    del feat
##
##        grid.commitChanges()
##        # Create another memory copy, to add time-varying features
##        del grid
##        grid = QgsVectorLayer("Polygon?crs=" + crs, "temporary_layer", "memory")
##        pr = grid.dataProvider()
##
##        # fields of original grid layer
##        fld = wlayer.dataProvider().fields()
##        for f in fld:
##            pr.addAttributes([f])
##
##        for f in wlayer.getFeatures():
##            for oo in obs_data:
##                t_data = oo.time_series_data
##                #
##                # Create a new feature for the new layer
##                newfeature = QgsFeature()
##                newfeature.setAttributes(f.attributes())
##                newfeature.setGeometry(f.geometry())
##
##                fieldIdx = grid.dataProvider().fieldNameIndex('OBSNAM')
##                newfeature.setAttribute(fieldIdx, t_data['obsname'][0])
##                #
##                fieldIdx = wlayer.dataProvider().fieldNameIndex('HOBS')
##                newfeature.setAttribute(fieldIdx, float(t_data['hobs'][0]))
##                # upload of weight not supported, since it is not stored in HOB input file. Set to 1.0
##                fieldIdx = wlayer.dataProvider().fieldNameIndex('WEIGHT')
##                newfeature.setAttribute(fieldIdx, 1.0)
##                # as above
##                fieldIdx = wlayer.dataProvider().fieldNameIndex('STATISTICS')
##                newfeature.setAttribute(fieldIdx, 'SD')
##                #
##                fieldIdx = wlayer.dataProvider().fieldNameIndex('TOFFSET')
##                newfeature.setAttribute(fieldIdx, float(t_data['toffset'][0]))
##                #
##                fieldIdx = grid.dataProvider().fieldNameIndex('IREFSP')
##                newfeature.setAttribute(fieldIdx, float(t_data['irefsp'][0]))
##
##                # Add feature to new layer
##                pr.addFeatures([newfeature])
##
##        # Make the vlayer shapefile
##        newName = 'model_hob'
##        filepath = os.path.join(self.shapes_dir, newName)
##        QgsVectorFileWriter.writeAsVectorFormat( grid, filepath, "UTF-8", grid.dataProvider().crs(), "ESRI Shapefile", False )
##        final_layer = QgsVectorLayer(filepath, newName, 'ogr')
##        QgsMapLayerRegistry.instance().addMapLayer(final_layer)

##        if self.chkSqlite.isChecked():
##            uploadQgisVectorLayer(dbName, grid, newName, srid=grid.crs().postgisSrid(), selected=False)
##            # Retrieve the Spatialite layer and add it to mapp
##            uri = QgsDataSourceURI()
##            uri.setDatabase(dbName)
##            schema = ''
##            table = newName
##            geom_column = "Geometry"
##            uri.setDataSource(schema, table, geom_column)
##            display_name = table
##            final_layer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
##
##        # remove tmp layer
##        con = sqlite3.connect(dbName)
##        cur = con.cursor()
##        cur.execute("DROP TABLE {}".format(tmp_table))
##
##        cur.close()
##        con.commit()
##        con.close()
# ---------------------------------------

#
    def importModel(self):
        modelName = str(self.txtModelName.text())
        model_ws = self.txtDirectory.text()
        old_model_ws = os.path.dirname(str(self.txtModflowFile.text()))
        nam_file = os.path.basename(str(self.txtModflowFile.text()))
        dbname = os.path.join(model_ws, modelName + '.sqlite')
        # make it also self:
        self.dbname = dbname
        #
        self.progressBar.setMinimum(0)
        # ------------------
        #
        if os.path.isfile(os.path.join(model_ws,modelName + '.sqlite')):
            pop_message(self.tr('In this Working Folder you halready have a model named:\n {} \n Please change directory or model name'.forma(modelName)), self.tr('warning'))
            return
        #
        m = Modflow()
        #
        ml = Modflow.load(nam_file, model_ws=old_model_ws, check=False)
        # Here we need to enter CRS and x_upperLeft, y_upperLeft
        # IF you have a rectangle representing the domain (e.g.the grid), you can load it in QGIS to get geo-information
        # as follows:
        ##vl = iface.activeLayer()
        ##xul, yul  = vl.extent().xMinimum(), vl.extent().yMaximum()
        ##proj4_str = vl.crs().toProj4 ()
        if self.anchorGroup.isChecked():
            xul = float(self.txtX.text())
            yul = float(self.txtY.text())
            rotation = float(self.txtAngle.text())
            # get Projection as string from the Qgs object
            proj4_str = str(self.projApp.crs().toProj4())
            crs = str(self.projApp.crs().authid())

        else:
            # set default values
            xul = 0.0
            yul = 0.0
            rotation = 0.0
            proj4_str = '+proj=utm +zone=32 +datum=WGS84 +units=m +no_defs'
            crs = 'EPSG:32632'

        # make crs self
        self.crs = crs
        # DIS
        ml.dis.sr.proj4_str = proj4_str
        ml.dis.sr.xul = xul
        ml.dis.sr.yul = yul
        ml.dis.sr.rotation = rotation

        nrow, ncol, nlay = ml.nrow , ml.ncol, ml.nlay
        self.nrow, self.ncol, self.nlay = nrow , ncol, nlay
        #
        self.progressBar.setValue(1)

        # --
        # Create Spatialite DB of your model
        lengthInteger = ml.dis.lenuni
        timeInteger = ml.dis.itmuni
        # convert to string, for FREEWAT table
        # length unit
        if lengthInteger == 0:
            lengthString  = 'undefined'
        elif lengthInteger == 1:
            lengthString  = 'ft'
        elif lengthInteger == 2:
            lengthString  = 'm'
        elif lengthInteger == 3:
            lengthString  = 'cm'
        # time unit
        if timeInteger == 0:
            timeString = 'undefined'
        elif timeInteger == 1:
            timeString = 'sec'
        elif timeInteger == 2:
            timeString = 'min'
        elif timeInteger == 3:
            timeString = 'hour'
        elif timeInteger == 4:
            timeString = 'day'
        elif timeInteger == 5:
            timeString = 'year'

        isChild = 1.0
        # info on stress period
        kper = ml.kper
        length = ml.dis.perlen.array
        time_steps = ml.dis.nstp.array
        multiplier = ml.dis.tsmult.array
        state = []
        for s in [ss for i, ss in enumerate(ml.dis.steady.array)]:
            if s :
                state.append('SS')
            else:
                state.append('TR')

        # first stress period info for defining the FREEWAT model
        timeParameters  = [float(length[0]), int(time_steps[0]), float(multiplier[0]), state[0]]

        # Info on initial time: this is fake!
        initTimeString = '12:00'
        initDateString = '2019-01-01'
        #
        self.progressBar.setValue(2)
        #
        createModel(modelName, model_ws, isChild, lengthString, timeString, timeParameters, initDateString, initTimeString,  crs)
        #
        self.progressBar.setValue(3)
        # --
        # start retriving info on the grid
        self.sr = ml.dis.sr
        # grid
        fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'mygrid_grid.shp')
        wr = write_grid_shapefile(fileshp, ml.dis.sr, {'top': ml.dis.top.array})
        # make it as self:
        self.gridLayer = QgsVectorLayer(fileshp, "model_grid", "ogr")
        # creating/connecting SQL database object
        con = sqlite3.connect(dbname)
        # con = sqlite3.connect(":memory:") if you want write it in RAM
        con.enable_load_extension(True)
        cur = con.cursor()
        #
        #
        if self.chkSqlite.isChecked():
            # temporarly disable the pop-up asking CRS
            settings = QSettings()
            oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
            settings.setValue( "/Projections/defaultBehaviour", "useProject" )
            layerName = 'model_grid'
            wb = QgsVectorLayer(fileshp, layerName, 'ogr')
            srs = QgsCoordinateReferenceSystem(unicode(crs))
            wb.setCrs(srs)
            srid = wb.crs().postgisSrid()
            uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
            #
            # Retrieve the Spatialite layer and add it to mapp
            uri = QgsDataSourceURI()
            uri.setDatabase(dbname)
            schema = ''
            table = layerName
            geom_column = "Geometry"
            uri.setDataSource(schema, table, geom_column)
            display_name = table
            wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
            QgsMapLayerRegistry.instance().addMapLayer(wlayer)
            # revert settings to original
            settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
        #
        # Update TIME TABLE with other stress periods (if any): all info is already uploaded!
        timeTable = 'timetable_' + modelName
        updateList = []
        for i, ln in enumerate(length[1:]):
            updateList.append((i+2,float(ln), int(time_steps[i+1]) , float(multiplier[i+1]), state[i+1] ))

        cur.executemany('INSERT INTO {} (sp, length, ts, multiplier, state) VALUES (?, ?, ?, ?, ?);'.format(timeTable), updateList)

        # Create new LPF table in DB
        nameTable = "lpf_"+ modelName
        ##        SQLcreate = 'CREATE TABLE %s ("name" varchar(20), "type" integer,' \
        ##                '"layavg" integer, "chani" integer, "laywet" integer );'% nameTable
        SQLcreate = 'CREATE TABLE %s ("name" varchar(20), "type" varchar(20),' \
                '"layavg" varchar(20), "chani" integer, "laywet" varchar(20) );'% nameTable
        cur.execute(SQLcreate)

        # prepare for LPF table insert
        layType = []
        for i, lt in enumerate(ml.lpf.laytyp.array):
            if lt == 1:
                layType.append('convertible')
            else:
                layType.append('confined')
        #
        layAverage = []
        for i, lv in enumerate(ml.lpf.layavg.array):
            if lv == 0:
                layAverage.append('harmonic')
            elif lv == 1:
                layAverage.append('logarithmic')
            else:
                layAverage.append('arithmetic-mean')
        # Remark: in FREEWAT chani is always 1.0
        chani = [1.0 for i in ml.lpf.chani.array]
        #
        layWet = []
        for i, lw in enumerate(ml.lpf.laywet.array):
            if lw == 1:
                layWet.append('Yes')
            else:
                layWet.append('No')
        #
        #
        self.progressBar.setValue(5)
        # model layers
        for l in range(nlay):
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_layer_%i.shp'%(l+1))
            modelLayerDict = {}
            # DIS
            'BORDER' ,'ACTIVE','TOP', 'BOTTOM', 'THICKNESS', 'STRT', 'KX', 'KY', 'KZ', 'SS', 'SY', 'NT', 'NE', ''
            if l == 0:
                modelLayerDict['TOP'] = ml.dis.top.array
            else:
                modelLayerDict['TOP'] = ml.dis.botm.array[l-1]
            modelLayerDict['BOTTOM'] = ml.dis.botm.array[l]
            # BAS
            modelLayerDict['ACTIVE'] = ml.bas6.ibound.array[l]
            modelLayerDict['STRT'] = ml.bas6.strt.array[l]
            modelLayerDict['KX'] = ml.lpf.hk.array[l] # KX = hk
            modelLayerDict['KY'] = ml.lpf.hk.array[l] # KY = KX
            modelLayerDict['KZ'] = ml.lpf.vka.array[l] # KZ = vka
            modelLayerDict['SS'] = ml.lpf.ss.array[l]
            modelLayerDict['SY'] = ml.lpf.sy.array[l]
            modelLayerDict['WETDRY'] = ml.lpf.wetdry.array[l]
            modelLayerDict['NT'] = 0.2*np.ones(shape = (nrow,ncol))
            modelLayerDict['NE'] = 0.2*np.ones(shape = (nrow,ncol))


            wr = write_grid_shapefile(fileshp, ml.dis.sr, modelLayerDict)
            # update LPF table
            # Insert values in LPF Table
            ftinsert = ('model_layer_%i'%(l+1),layType[l],layAverage[l],chani[l],layWet[l])
            sql3 = 'INSERT INTO %s '%nameTable +  '(name,type,layavg,chani,laywet)'
            cur.execute(sql3 + 'VALUES (?, ?, ?, ?, ?);', ftinsert)
            #
            self.progressBar.setValue(5 + l)

        # commit and close connection
        # Close SpatiaLiteDB
        cur.close()
        # Save the changes
        con.commit()
        # Close connection
        con.close()
        # Upload table into map
        # Add the model table into QGis map
        uri = QgsDataSourceURI()
        uri.setDatabase(dbname)
        schema = ''
        table = nameTable
        #geom_column = 'the_geom'
        geom_column = None
        uri.setDataSource(schema, table, geom_column)
        display_name = nameTable
        tableLayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
        QgsMapLayerRegistry.instance().addMapLayer(tableLayer)
        #
        val = self.progressBar.value()
        self.progressBar.setValue(val + nlay + 1)

        # Upload the layers in DB:
        # this could be done inside the previous loop, but we need to chenge the method asking
        # only for cur and for dbName
        #
        if self.chkSqlite.isChecked():
            # temporarly disable the pop-up asking CRS
            settings = QSettings()
            oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
            settings.setValue( "/Projections/defaultBehaviour", "useProject" )
            for l in range(nlay):
                layerName = 'model_layer_%i'%(l+1)
                fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_layer_%i.shp'%(l+1))
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                #
                val = self.progressBar.value()
                self.progressBar.setValue(val + 1)

            # revert settings to original
            settings.setValue( "/Projections/defaultBehaviour", oldProjValue )

        # --
        # Scout other packages
        pklist = [p.name[0] for p in ml.packagelist]
        nper  = ml.nper
        # RCH
        if 'RCH' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_rch.shp')
            rch_dict = {}
        ##    if ml.rch.irc is None:
        ##        irch = np.ones(shape = (nrow, ncol))
        ##    else:
            for t in range(nper):
                if ml.rch.irch is None:
                    irch = np.ones(shape = (nrow, ncol), dtype = np.int)
                else:
                    irch = ml.rch.irch[t].array
                rch_dict['sp_%i_irch'%(t+1)] = irch
                rch_dict['sp_%i_rech'%(t+1)] = ml.rch.rech[t].array

            wr = write_grid_shapefile(fileshp, ml.dis.sr, rch_dict)
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_rch'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)


        # WEL
        if 'WEL' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_well.shp')
            wel_dict = {}
            welCells = np.zeros(shape = (nrow, ncol))
            fromLayarray = np.ones(shape = (nrow,ncol), dtype = np.int)
            toLayarray = np.ones(shape = (nrow,ncol), dtype = np.int)
            for t in range(nper):
                nwells = len(ml.wel.stress_period_data[t])
                pumpingarray = np.zeros(shape = (nrow,ncol))
                for i in range(len(ml.wel.stress_period_data[t])):
                    dataWell = ml.wel.stress_period_data[t][i]
                    (lay, r, c, f ) = (dataWell[0], dataWell[1], dataWell[2], dataWell[3] )
                    if fromLayarray[r,c] > lay + 1:
                        fromLayarray[r,c] = np.int(lay + 1)
                    if toLayarray[r,c] < lay + 1:
                        toLayarray[r,c] = np.int(lay + 1)
                    pumpingarray[r,c] = f
                    welCells[r,c] = 1
                wel_dict['sp_%i'%(t+1)] = pumpingarray
            wel_dict['from_lay'] = fromLayarray
            wel_dict['to_lay'] = toLayarray
            wr = write_grid_shapefile(fileshp, ml.dis.sr, wel_dict, selectedCell = welCells)
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_well'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)

        # CHD
        if 'CHD' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_chd.shp')
            chd_dict = {}
            chdCells = np.zeros(shape = (nrow, ncol))
            fromLayarray = np.ones(shape = (nrow,ncol), dtype = np.int)
            toLayarray = np.ones(shape = (nrow,ncol), dtype = np.int)
            for t in range(nper):
                # manage the case of ITMP = 0 (this data equal to the previous Stress Period Data):
                if ml.chd.stress_period_data[t] is None:
                    ml.chd.stress_period_data[t] = ml.chd.stress_period_data[t-1]
                nchd = len(ml.chd.stress_period_data[t])
                sheadarray = np.zeros(shape = (nrow,ncol))
                eheadarray = np.zeros(shape = (nrow,ncol))
                for i in range(len(ml.chd.stress_period_data[t])):
                    (lay, r, c, starting, ending )= ml.chd.stress_period_data[t][i]
                    if fromLayarray[r,c] > lay + 1:
                        fromLayarray[r,c] = np.int(lay + 1)
                    if toLayarray[r,c] < lay + 1:
                        toLayarray[r,c] = np.int(lay + 1)
                    sheadarray[r,c] = starting
                    eheadarray[r,c] = ending
                    chdCells[r,c] = 1
                chd_dict['%i_shead'%(t+1)] = sheadarray
                chd_dict['%i_ehead'%(t+1)] = eheadarray
            chd_dict['from_lay'] = fromLayarray
            chd_dict['to_lay'] = toLayarray
            wr = write_grid_shapefile(fileshp, ml.dis.sr, chd_dict, selectedCell = chdCells)
            #
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_chd'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)

        # HFB
        if 'HFB6' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_hfb.shp')
            hfb_dict = {}
            hfbCells = np.zeros(shape = (nrow, ncol))
            nhfb = len(ml.hfb6.hfb_data)
            layarray = np.ones(shape = (nrow,ncol),dtype = np.int)
            row1array = np.zeros(shape = (nrow,ncol),dtype = np.int)
            row2array = np.zeros(shape = (nrow,ncol),dtype = np.int)
            col1array = np.zeros(shape = (nrow,ncol),dtype = np.int)
            col2array = np.zeros(shape = (nrow,ncol),dtype = np.int)
            hydchrarray = np.zeros(shape = (nrow,ncol))
            for i in range(nhfb):
                (lay, row1, col1, row2, col2, hydchr) = ml.hfb6.hfb_data[i]
                row1array[row1, col1] = int(row1)
                row2array[row1, col1] = int(row2)
                col1array[row1, col1] = int(col1)
                col2array[row1, col1] = int(col2)
                hydchrarray[row1, col1] = float(hydchr)
                hfbCells[row1,col1] = 1

            hfb_dict['lay'] = layarray
            hfb_dict['row1'] = row1array
            hfb_dict['row2'] = row2array
            hfb_dict['col1'] = col1array
            hfb_dict['col2'] = col2array
            hfb_dict['hydchr'] = hydchrarray

            wr = write_grid_shapefile(fileshp, ml.dis.sr, hfb_dict, selectedCell = hfbCells)
            #
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_hfb'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)


        # GHB
        if 'GHB' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_ghb.shp')
            ghb_dict = {}
            ghbCells = np.zeros(shape = (nrow, ncol))
            fromLayarray = np.ones(shape = (nrow,ncol), dtype = np.int)
            toLayarray = np.ones(shape = (nrow,ncol), dtype = np.int)
            for t in range(nper):
                nghb = len(ml.ghb.stress_period_data[t])
                bheadarray = np.zeros(shape = (nrow,ncol))
                condarray = np.zeros(shape = (nrow,ncol))
                xyzarray = np.zeros(shape = (nrow,ncol))
                # first deal with the possibility to manage the existance of (optional) [xyz] input in stress period data
                if len(ml.ghb.stress_period_data[0][0]) == 6:
                    xyzOption = True
                else:
                    xyzOption = False

                for i in range(len(ml.ghb.stress_period_data[t])):
                    if xyzOption:
                        (lay, r, c, bhead, cond , xyz ) = ml.ghb.stress_period_data[t][i]
                    else:
                        (lay, r, c, bhead, cond ) = ml.ghb.stress_period_data[t][i]
                        xyz = 1

                    if fromLayarray[r,c] > lay + 1:
                        fromLayarray[r,c] = np.int(lay + 1)
                    if toLayarray[r,c] < lay + 1:
                        toLayarray[r,c] = np.int(lay + 1)
                    bheadarray[r,c] = bhead
                    condarray[r,c] = cond
                    ghbCells[r,c] = 1
                    xyzarray[r,c] = xyz
                ghb_dict['bhead_%i'%(t+1)] = bheadarray
                ghb_dict['cond_%i'%(t+1)] = condarray
            ghb_dict['from_lay'] = fromLayarray
            ghb_dict['to_lay'] = toLayarray

            wr = write_grid_shapefile(fileshp, ml.dis.sr, ghb_dict, selectedCell = ghbCells)
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_ghb'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)

        # RIV
        if 'RIV' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_riv.shp')
            riv_dict = {}
            rivCells = np.zeros(shape = (nrow, ncol))
            layArray = np.ones(shape = (nrow,ncol), dtype = np.int)
            for t in range(nper):
                nriv = len(ml.riv.stress_period_data[t])
                stagearray = np.zeros(shape = (nrow,ncol))
                rbotarray = np.zeros(shape = (nrow,ncol))
                condarray = np.zeros(shape = (nrow,ncol))
                #
                for i in range(len(ml.riv.stress_period_data[t])):
                    (lay, r, c, stage, cond , rbot ) = ml.riv.stress_period_data[t][i]
                    if lay +1 != 1:
                        layArray[r,c] = np.int(lay + 1)
                    stagearray[r,c] = stage
                    condarray[r,c] = cond
                    rbotarray[r,c] = rbot
                    rivCells[r,c] = 1
                riv_dict['stage_%i'%(t+1)] = stagearray
                riv_dict['cond_%i'%(t+1)] = condarray
                riv_dict['rbot_%i'%(t+1)] = rbotarray

            riv_dict['layer'] = layArray
            wr = write_grid_shapefile(fileshp, ml.dis.sr, riv_dict, selectedCell = rivCells)
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_riv'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)

        # DRN
        if 'DRN' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_drn.shp')
            drn_dict = {}
            drnCells = np.zeros(shape = (nrow, ncol))
            layArray = np.ones(shape = (nrow,ncol), dtype = np.int)
            lengthArray = np.zeros(shape = (nrow,ncol))

            for t in range(nper):
                ndrn = len(ml.drn.stress_period_data[t])
                elevarray = np.zeros(shape = (nrow,ncol))
                condarray = np.zeros(shape = (nrow,ncol))
                #
                for i in range(len(ml.drn.stress_period_data[t])):
                    drainData = ml.drn.stress_period_data[t][i]
                    (lay, r, c, elev, cond ) = (drainData[0],drainData[1],drainData[2],drainData[3],drainData[4])
                    if lay +1 != 1:
                        layArray[r,c] = np.int(lay + 1)
                    elevarray[r,c] = elev
                    condarray[r,c] = cond
                    drnCells[r,c] = 1
                drn_dict['elev_%i'%(t+1)] = elevarray
                drn_dict['cond_%i'%(t+1)] = condarray

            drn_dict['layer'] = layArray
            drn_dict['length'] = lengthArray
            drn_dict['segment'] = np.ones(shape = (nrow,ncol))
            wr = write_grid_shapefile(fileshp, ml.dis.sr, drn_dict, selectedCell = drnCells)
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_drn'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)

        # HOB
        if 'HOB' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_hob.shp')
            self.shapes_dir = os.path.join(model_ws, 'model_shapefiles')
            obs_data = ml.hob.obs_data
            self.createHobLayer(obs_data)

            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_hob'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)

        # SWI
        print('lista pacchetti', pklist)
        if 'SWI2' in pklist:
            fileshp = os.path.join(model_ws, 'model_shapefiles'  ,  'model_swi.shp')
            swi_dict = {}
            # here we assume that nsfr = 1 (only 1 surface) so that zeta is a listof length1
            # zeta[0] is an array (nlay, nrow, ncol)
            zetaarray = ml.swi2.zeta[0].array
            for l in range(nlay):
                swi_dict['zeta_%i'%(l+1)] = zetaarray[l]
                swi_dict['ssz_%i'%(l+1)] = ml.swi2.ssz.array[l]
                swi_dict['isource_%i'%(l+1)] = ml.swi2.isource.array[l]

            wr = write_grid_shapefile(fileshp, ml.dis.sr, swi_dict)
            #
            if self.chkSqlite.isChecked():
                # temporarly disable the pop-up asking CRS
                settings = QSettings()
                oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
                settings.setValue( "/Projections/defaultBehaviour", "useProject" )
                layerName = 'model_swi'
                wb = QgsVectorLayer(fileshp, layerName, 'ogr')
                srs = QgsCoordinateReferenceSystem(unicode(crs))
                wb.setCrs(srs)
                srid = wb.crs().postgisSrid()
                uploadQgisVectorLayer(dbname, wb, layerName, srid = srid)
                #
                # Retrieve the Spatialite layer and add it to mapp
                uri = QgsDataSourceURI()
                uri.setDatabase(dbname)
                schema = ''
                table = layerName
                geom_column = "Geometry"
                uri.setDataSource(schema, table, geom_column)
                display_name = table
                wlayer = QgsVectorLayer(uri.uri(), display_name, 'spatialite')
                QgsMapLayerRegistry.instance().addMapLayer(wlayer)
                # revert settings to original
                settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #
            val = self.progressBar.value()
            self.progressBar.setValue(val + 2)

        self.progressBar.setValue(100)

        messaggio = 'MODFLOW model coorected imported in ' + model_ws
        Logger.information(None, 'Information', '%s' % (messaggio))

