# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2019 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************
import os
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import QtGui, uic
from qgis.core import *
from PyQt4 import QtCore
from qgis.gui import QgsMessageBar

#matplotlib imports
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
if matplotlib.__version__ >= '1.5.0':
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
else:
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure

# Import the code for the dialog
import numpy as np
import sys, os
#
#
from freewat.freewat_utils import getVectorLayerByName, getVectorLayerNames, getModelsInfoLists, getModelInfoByName, getModelNlayByName, getModelNspecByName, getTransportModelsByName, fileDialog, getModelUnitsByName, getGroupLayerByName, getUniqueValuesArray, pop_message, ComboStyledItemDelegate
from freewat.sqlite_utils import getTableData, getTableArrayData, checkIfTableExists
import freewat.createGrid_utils

FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_plotGrid.ui') )

class PlotGrid(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)

        # 3 Figure objects:
        # 1 for plant vie (XY)
        # 1 for X view (fixed ROW)
        # 1for Y view (fixed COL)
        #
        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self.figure)
        self.mpltoolbar = NavigationToolbar(self.canvas, self.widgetPlant)
        lstActions = self.mpltoolbar.actions()
        self.mpltoolbar.removeAction(lstActions[7])
        self.layoutPlotXY.addWidget(self.canvas)
        self.layoutPlotXY.addWidget(self.mpltoolbar)
        #
        #
        self.figureX = Figure()
        self.axesX = self.figureX.add_subplot(111)
        self.canvasX = FigureCanvas(self.figureX)
        self.mpltoolbarX = NavigationToolbar(self.canvasX, self.widgetPlotX)
        self.mpltoolbarX.removeAction(lstActions[7])
        self.layoutPlotX.addWidget(self.canvasX)
        self.layoutPlotX.addWidget(self.mpltoolbarX)
        #
        self.figureY = Figure()
        self.axesY = self.figureY.add_subplot(111)
        self.canvasY = FigureCanvas(self.figureY)
        self.mpltoolbarY = NavigationToolbar(self.canvasY, self.widgetPlotY)
        self.mpltoolbarY.removeAction(lstActions[7])
        self.layoutPlotY.addWidget(self.canvasY)
        self.layoutPlotY.addWidget(self.mpltoolbarY)
        #
        self.spinRow.valueChanged.connect(self.CreatePlot)
        self.spinCol.valueChanged.connect(self.CreatePlot)

        # create the dictionary with SP as keys and nested dictionary with TS as keys and ranges as values
        # it has to be created here in order to have SP and corresponding TS to populate the comboboxes

        self.cmbModelName.clear()
        layerNameList = getVectorLayerNames()
        layerNameList.sort()

        # Retrieve modelname and pathfile List
        (modelNameList, pathList) =  getModelsInfoLists(layerNameList)
        self.cmbModelName.addItems(modelNameList)
        #
        self.CreatePlot()


    def getGridStructure(self):
        # ------------ Load input MDO  ------------
        modelName = self.cmbModelName.currentText()
        pathfile, _ = getModelInfoByName(modelName)
        self.dbpath = os.path.join(pathfile, '{}.sqlite'.format(modelName))
        # get name of layers
        layerNameList = getVectorLayerNames()
        layNameList = []
        for mName in layerNameList:
            # Retrieve LPF table, and from there model layers and data needed for LPF
            if mName ==  "lpf_"+ modelName:
                lpftable = getVectorLayerByName(mName)
                # Number of layers
                nlay = 0
                # Get layers name from LPF table
                dpLPF = lpftable.dataProvider()
                #
                for ft in lpftable.getFeatures():
                    attrs = ft.attributes()
                    layNameList.append(attrs[0])
                    nlay = nlay + 1
    #
        # Number of rows (along width)
        # new method to get directly nrow and ncol: it works also with rotated grids
        nrow, ncol =  freewat.createGrid_utils.get_row_col(getVectorLayerByName(layNameList[0]))

        # delc, delrow
        delr, delc = freewat.createGrid_utils.get_rgrid_delr_delc(getVectorLayerByName(layNameList[0]))

        # Set TOP and Bottom(s)
        # Get data from DB
        # check if table exists:
        tbl_exists = checkIfTableExists(self.dbpath,layNameList[0], popupNoExists = True)
        if tbl_exists:
            modeltop = getTableArrayData(self.dbpath, layNameList[0] , col = 'TOP', nrow = nrow, ncol = ncol)
        else:
            # TO DO: an Error message here, please !!!
            return
        # --
        botm = np.zeros(shape = (nlay, nrow, ncol))
        for i in range(0,nlay):
            tbl_exists = checkIfTableExists(self.dbpath,layNameList[i], popupNoExists = True)
            if tbl_exists:
                btemp = getTableArrayData(self.dbpath, layNameList[i] , col = 'BOTTOM' , nrow = nrow, ncol = ncol)
                botm[i, : ,: ] = btemp
            else:
                return
        # make self
        self.nrow = nrow
        self.ncol = ncol
        self.nlay = nlay
        self.delr = delr
        self.delc = delc
        self.top = modeltop
        self.bottom = botm
        # set constarint on spin boxes
        self.spinRow.setMaximum(int(self.nrow))
        self.spinCol.setMaximum(int(self.ncol))
        self.spinRow.setMinimum(1)
        self.spinCol.setMinimum(1)


    def CreatePlot(self):
        '''
        method that create the grid plots

        according to ROW and COL selected

        '''
        #get model grid info
        self.getGridStructure()
        # clear the axes for the next plots
        self.axes.clear()
        self.axesX.clear()
        self.axesY.clear()
        #
        # DIS
        if isinstance(self.delc, float): # uniform cols
            X = np.array([int(self.delc) for i in range(self.ncol)])
        else: # non uniform grid in X
            X = np.array([self.delc[0]])
            X = np.append(X,[self.delc[i]+self.delc[i-1] for i in range(1,len(self.delc))])

        if isinstance(self.delc, float): # uniform rows
            Y = np.array([int(self.delr) for i in range(self.nrow)])
        else: # non uniform grid in Y
            Y = np.array([self.delr[0]])
            Y = np.append(Y,[self.delr[i]+self.delr[i-1] for i in range(1,len(self.delr))])

        XX, YY = np.meshgrid(X, Y)

        # --
        # Plot cross sections
        ##
        from matplotlib.figure import Figure
        from matplotlib.ticker import LinearLocator, FormatStrFormatter
        # select
        col = int(self.spinCol.value())
        row = int(self.spinRow.value())
        nlay = self.nlay

        Z = self.top
        # selected row
        self.axesX.plot(Z[row-1,:], label = 'Top')
        for l in range(nlay):
            Z1 = self.bottom[l]
            self.axesX.plot(Z1[row-1,:], label = 'Bottom Layer %i'%(l+1))
        self.axesX.set_xlabel('Columns')
        self.axesX.set_ylabel('Elevation')
        self.axesX.set_title('Layers elevation at ROW = {}'.format(row))
        #plt.title('Row %i Cross-Section - Model Grid'%row)
        self.axesX.legend(loc = 'best').draggable()
        self.axesX.grid(True)
        self.canvasX.draw()

        # selected col
        self.axesY.plot(Z[:,col -1], label = 'Top')
        for l in range(nlay):
            Z1 = self.bottom[l]
            self.axesY.plot(Z1[:,col-1], label = 'Bottom Layer %i'%(l+1))
        self.axesY.set_xlabel('Rows')
        self.axesY.set_ylabel('Elevation')
        self.axesY.set_title('Layers elevation at COL = {}'.format(col))
        self.axesY.legend(loc = 'best').draggable()
        self.axesY.grid(True)
        self.canvasY.draw()

        # plant (XY- plane)
        # plot the total grid as black lines
##        for i in range(XX.shape[1]):
##            self.axes.plot(XX[:,i],YY[:,i], 'k-')
##        for j in range(XX.shape[0]):
##            self.axes.plot(XX[j,:],YY[j,:], 'k-')

        # this row and this col
        self.axes.plot(col*np.ones(self.nrow), [i for i in range(self.nrow) ],'r-'   )
        self.axes.plot( [i for i in range(self.ncol)], row*np.ones(self.ncol),'b-')
        self.axes.set_xlim(1,self.ncol)
        self.axes.set_ylim(1,self.nrow)
        self.axes.set_yticks(XX[0,:], minor=False)
        self.axes.set_xticks(YY[:,0], minor=False)
        #self.axes.grid(True)
        self.axes.grid(color='k', linestyle='-', linewidth=2)
        self.canvas.draw()