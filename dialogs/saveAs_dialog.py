# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2019 - Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************

from PyQt4.QtCore import *
from PyQt4.QtCore import QSettings
from PyQt4.QtGui import *
from qgis.gui import QgsGenericProjectionSelector
import os
from PyQt4 import QtGui, uic
#
import numpy as np
#
# Import from flopy
from flopy.modflow import Modflow
#
# module shapefile is saved under freewat module
from freewat.shapefile import Shape, Writer
from freewat.sqlite_utils import checkIfTableExists, uploadQgisVectorLayer
from freewat.freewat_utils import *
from freewat.freewat_utils import FreewatModel
from freewat.mdoCreate_utils import createModel
import datetime
from pyspatialite import dbapi2 as sqlite3
# ---------------------------------------

#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_saveModelAs.ui') )

#class CreateModelDialog(QDialog, Ui_CreateModelDialog):
class SaveModelDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)

        self.OutFilePath = None
        self.encoding = None
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.saveModel)

        self.btnOldModelDir.clicked.connect(self.modelFile)
        self.btnWorkingDir.clicked.connect(self.workingDir)

    def modelFile(self):
        (self.InputFilePath) = fileDialog(self)
        self.txtModelFile.setText(str(self.InputFilePath))
        if self.InputFilePath is None or self.encoding is None:
            return

    def workingDir(self):
        (self.OutFilePath, self.encoding) = dirDialog(self)
        #see the current working directory in the text box
        self.txtDirectory.setText(self.OutFilePath)
        if self.OutFilePath is None or self.encoding is None:
            return



    def saveModel(self):
        newName = str(self.txtModelName.text())
        newDir = self.txtDirectory.text()
        modelname = os.path.basename(str(self.txtModelFile.text())).replace('.sqlite', '')
        model_ws  = os.path.dirname(str(self.txtModelFile.text()))
        #
        self.progressBar.setMinimum(0)
        # ------------------
        #
        if os.path.isfile(os.path.join(newDir,newName + '.sqlite')):
            pop_message(self.tr('In this Working Folder you halready have a model named:\n {} \n Please change directory or model name'.format(modelName)), self.tr('warning'))
            return
        #
        fwm = FreewatModel(modelname = modelname, model_ws = model_ws)
        fwm.save_as(newName = newName, newDir = newDir)
        # open the new model in QGIS
        newModel = FreewatModel(modelname = newName, model_ws = newDir)
        newModel.open()
        #
        self.progressBar.setValue(100)

        messaggio = 'FREEWAT model saved in ' + model_ws
        Logger.information(None, 'Information', '%s' % (messaggio))

