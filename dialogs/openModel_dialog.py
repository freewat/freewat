# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2019 - Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************

from PyQt4.QtCore import *
from PyQt4.QtCore import QSettings
from PyQt4.QtGui import *
from qgis.gui import QgsGenericProjectionSelector
import os
from PyQt4 import QtGui, uic
#
import numpy as np
#
# Import from flopy
from flopy.modflow import Modflow
#
# module shapefile is saved under freewat module
from freewat.shapefile import Shape, Writer
from freewat.sqlite_utils import checkIfTableExists, uploadQgisVectorLayer
from freewat.freewat_utils import *
from freewat.freewat_utils import FreewatModel
from freewat.mdoCreate_utils import createModel
import datetime
from pyspatialite import dbapi2 as sqlite3
# ---------------------------------------

#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_openModel.ui') )

#class CreateModelDialog(QDialog, Ui_CreateModelDialog):
class OpenModelDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)

        self.OutFilePath = None
        self.encoding = None
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.openModel)
        self.btnModelDir.clicked.connect(self.modelFile)

    def modelFile(self):
        (self.InputFilePath) = fileDialog(self)
        self.txtModelFile.setText(str(self.InputFilePath))
        if self.InputFilePath is None or self.encoding is None:
            return

    def openModel(self):
        modelname = os.path.basename(str(self.txtModelFile.text())).replace('.sqlite', '')
        model_ws  = os.path.dirname(str(self.txtModelFile.text()))
        #
        fwm = FreewatModel(modelname = modelname, model_ws = model_ws)
        fwm.open()
        #

        messaggio = 'FREEWAT model saved in ' + model_ws
        Logger.information(None, 'Information', '%s' % (messaggio))

