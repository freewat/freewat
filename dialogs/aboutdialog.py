# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2014 - 2015 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************


import os
import ConfigParser

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import QtGui, uic

#
#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_aboutdialogbase.ui'))


class AboutDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.setupUi(self)

        self.buttonBox.rejected.connect(self.reject)

        cfg = ConfigParser.SafeConfigParser()
        cfg.read(os.path.join(os.path.dirname(__file__), "../metadata.txt"))
        version = cfg.get("general", "version")

        self.textBrowser.setOpenExternalLinks(True)



    def reject(self):
        QDialog.reject(self)

    def openHelp(self):
        overrideLocale = bool(QSettings().value("locale/overrideFlag", False))
        if not overrideLocale:
            localeFullName = QLocale.system().name()
        else:
            localeFullName = QSettings().value("locale/userLocale", "")

        localeShortName = localeFullName[0:2]
        if localeShortName in ["ru", "uk"]:
            QDesktopServices.openUrl(QUrl("http://www.tea-group.com"))
        else:
            QDesktopServices.openUrl(QUrl("http://www.tea-group.com"))

##    def getAboutText(self):
##        return self.tr('<p>Provides tools for statistical data exploration, stored as  \
##            field of vector layer. </p>\
##            <p><strong>Help/Suggestions</strong>:</p>\
##            <p>Data Explorer is aimed at plotting and/or analysing \
##            data series, with particular enphasis on time series. \
##            The plugin structure has been partially mutuated by the plugin \
##            <b>Statist</b>: the User is encouradged to use also that plugin \
##            to complete the analysis of its data. <br>\
##            With <b>Data Explorer</b> you can: <br>\
##            -Plot data and compare them with their descriptive parameters <br>\
##            -Compare different data using multi-plot or scatter plot.<br>\
##            -Perform Normality tests: Normal q-q plot;\
##            Kolmogorov-Smirnov test.<br>\
##            -Perform Time Series Analysis (autocorrelation, periodogramm, trend test) <br> </p>\
##            <p><strong>Developer</strong>: Iacopo Borsi</p>\
##            <p><strong>Homepage</strong>: <a href="http://www.tea-group.com">http://www.tea-group.com</a></p>'
##            )
