#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''''
from AKFeatureCustomChart import AKFeatureCustomChart
from AKSettings import TABLES
from measurements.AKFeatureMeasurementsChem import AKFeatureMeasurementsChem

class AKFeatureCustomChartChem(AKFeatureCustomChart):
    '''This base class for all features that are run a query of geochemical measurements.'''
    def __init__(self, settings, iface, windowTitle = "", parent=None):
        super(AKFeatureCustomChartChem, self).__init__(settings, iface, windowTitle, parent)
        self.m_measurementsSubFeature = AKFeatureMeasurementsChem(self.m_settings, self.iface, windowTitle, self.parent())                
        self.m_measurementsSubFeature.nextForm.connect(self.onNextForm)
        self.m_measurementsSubFeature.m_parametersAreImposed = True
        self.allParameterForCharts = {"N436MQL" : {"parameterIds": [813, 814],
                                           "factor": 12.1},
                                    "N580MQL" : {"parameterIds": [1046, 1047],
                                         "factor": 23.0},
                                    "N209MQL" : {"parameterIds": [414,415],
                                         "factor": 20.0},
                                    "N247MQL" : {"parameterIds": [479, 480],
                                         "factor": 35.5},
                                    "N582MQL" : {"parameterIds": [1050,1051],
                                         "factor": 48.0},
                                    "N168MQL" : {"parameterIds": [339,340],
                                         "factor": 61.0},
                                    "N592MQL" : {#"label": "TAC (meq/l)",
                                         "parameterIds": [1073],
                                         "factor": 1.22},
                                    "N553MQL" : {"parameterIds": [1007, 1008],
                                         "factor": 39.1},
                                    "N480MQL" : {"parameterIds": [899, 900],
                                         "factor": 62},
                                    "N326UNK" : {"parameterIds": [612, 613],
                                         "factor": 1.0},
                                    "N647UNK" : {"parameterIds": [1172],
                                         "factor": 1.0},
                                    "N124MQL" : {"parameterIds": [256, 257],
                                         "factor": 1.0},
                                    "N595GCG" : {#"label": "Temperature (C)",
                                         "parameterIds": [1077, 1078],
                                         "factor": 1.0},
                                    "N216USC" : {"parameterIds": [431],
                                         "factor": 1.0},
                                    "N540UNK" : {#"label": "pH",
                                         "parameterIds": [985],
                                         "factor": 1.0}
                                    }

    def defineSQLQueries(self):
        self.sqlSelBaseParam =  "SELECT " \
                                      "CP.id, " \
                                      "CP.parameterNumber as BaseParam, " \
                                      "CP.nameEN AS ParameterName, " \
                                      "UNITS.nameEN AS Unit, " \
                                      "UNITS.uomCode AS UnitCode, " \
                                      "UNITS.id AS UnitId " \
                                "FROM " \
                                    + TABLES.CHEMPARAMS + " CP, " \
                                    + TABLES.L_UNITOFMEASUREMENTS + " UNITS " + \
                                "WHERE " \
                                    "CP.uomCodeId = UNITS.id " \
                                    "AND CP.hydrochemicalParameterCode = ? ;"