#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from external.chemPlotLib.SBDPlot import SBDPlot
from AKFeatureResultsPlot import AKFeatureResultsPlot

class AKFeatureResultsSBD(AKFeatureResultsPlot):
    '''
    This feature provides the functionality for the second form to create SBD plots. 
    '''
    def __init__(self, settings, iface, parent=None):
        super(AKFeatureResultsSBD, self).__init__(settings, iface, SBDPlot, windowTitle = "SBD Results", parent=parent)

    def initialize(self):
        self.showData(hideMeasurements=True)
        super(AKFeatureResultsSBD, self).initialize()
        
    def onActionClicked(self):
        model = self.sampleMeasurementsModel
        myChart = SBDPlot(self.iface.mainWindow())
        
        labels = []    
        Ca = []
        Mg = []
        Na = []
        Cl = []    
        SO4 = []
        HCO3 = []

        numOfSamples = model.rowCount()
        for row in range(numOfSamples):
            isValid = model.data(model.index(row, model.COLUMNS["valid"]))
            if(isValid):
                label = model.data(model.index(row, model.COLUMNS["Sample"]))
                labels.append(label)
                columnId = self.paramInfo["N209MQL"]["id"]
                value = model.data(model.index(row, model.COLUMNS[columnId]))
                Ca.append(value)
                columnId = self.paramInfo["N436MQL"]["id"]
                value = model.data(model.index(row, model.COLUMNS[columnId]))
                Mg.append(value)
                columnId = self.paramInfo["N580MQL"]["id"]
                value = model.data(model.index(row, model.COLUMNS[columnId]))
                Na.append(value)
                columnId = self.paramInfo["N247MQL"]["id"]
                value = model.data(model.index(row, model.COLUMNS[columnId]))
                Cl.append(value)
                columnId = self.paramInfo["N582MQL"]["id"]
                value = model.data(model.index(row, model.COLUMNS[columnId]))
                SO4.append(value)
                columnId = self.paramInfo["N168MQL"]["id"]
                value = model.data(model.index(row, model.COLUMNS[columnId]))
                HCO3.append(value)                

        values = {'Ca':Ca, 'Mg':Mg, 'Na':Na, 'Cl':Cl, 'SO4':SO4, 'HCO3':HCO3}   
        myChart.setData(labels, values)
        myChart.set_settings(self.plotSettingsModel.getDictData())        
        myChart.draw()

    def postProcessModelData(self, paramInfo, parameterOrder):
        super(AKFeatureResultsSBD, self).postProcessModelData(paramInfo, parameterOrder)
        self.addValidColumn(paramInfo, parameterOrder)
