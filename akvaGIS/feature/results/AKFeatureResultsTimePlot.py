#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from AKFeatureResultsPlot import AKFeatureResultsPlot
from external.chemPlotLib.TimePlot import TimePlot, TIMEPLOTSETTINGS
from form.AKWidgetTimePlotSettings import AKWidgetTimePlotSettings

class AKFeatureResultsTimePlot(AKFeatureResultsPlot):
    '''
    This feature provides the functionality for the second form to create time plots. 
    '''
    def __init__(self, settings, iface, windowTitle = "TimePlot Results", parent=None):
        super(AKFeatureResultsTimePlot, self).__init__(settings, iface, TimePlot, windowTitle, parent=parent)
        
    def customizeForm(self):
        super(AKFeatureResultsTimePlot, self).customizeForm()
        self.timeSettingsForm = AKWidgetTimePlotSettings(self.iface, self.m_form)    
        self.m_form.customResultsGroup.layout().addWidget(self.timeSettingsForm)
        
        mod = self.plotSettingsModel
        time_automatic_title_id =  mod.columnFromField(TIMEPLOTSETTINGS.time, TIMEPLOTSETTINGS.time_automatic_title_id)         
        self.mapper.addMapping(self.timeSettingsForm.time_automatic_title, time_automatic_title_id);
        
        time_format_id =  mod.columnFromField(TIMEPLOTSETTINGS.time, TIMEPLOTSETTINGS.time_format_id)         
        self.mapper.addMapping(self.timeSettingsForm.time_format, time_format_id);
        
        self.mapper.toFirst()
                         
        currentText = mod.data(mod.index(0,time_format_id))
        comboW = self.timeSettingsForm.time_format
        comboW.setCurrentIndex(comboW.findText(currentText))
        
        self.timeSettingsForm.time_automatic_title.clicked.connect(self.plotSettingsForm.title_label.setDisabled)
        checked = mod.data(mod.index(0, time_automatic_title_id))
        self.timeSettingsForm.time_automatic_title.clicked.emit(checked)
                
        # disable unused things
        self.plotSettingsForm.lineGroup.hide()
           
    def initialize(self):
        self.showData()
        super(AKFeatureResultsTimePlot, self).initialize()
        
    def onActionClicked(self):
        timeSeriesData = self.storeDataInTimeSeries()
        
        counter = 0
        for curPlot in timeSeriesData:        
            myChart = TimePlot(self.iface.mainWindow())
            myChart.setData(timeSeriesData[curPlot])
            myChart.set_settings(self.plotSettingsModel.getDictData())
            myChart.draw(num_generated=counter)
            counter += 1

    def postProcessModelData(self, paramInfo, parameterOrder):
        super(AKFeatureResultsTimePlot, self).postProcessModelData({}, [])
        self.showData()