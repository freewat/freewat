#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
import math

from external.chemPlotLib.SARPlot import SARPlot, SARSETTINGS
from AKFeatureResultsPlot import AKFeatureResultsPlot
from form.AKWidgetSARPlotSettings import AKWidgetSARPlotSettings

class AKFeatureResultsSAR(AKFeatureResultsPlot):
    '''
    This feature provides the functionality for the second form to create SAR plots. 
    '''
    def __init__(self, settings, iface, parent=None):
        super(AKFeatureResultsSAR, self).__init__(settings, iface, SARPlot, windowTitle = "SAR Results", parent=parent)
        
    def customizeForm(self):
        super(AKFeatureResultsSAR, self).customizeForm()
        self.stiffSettingsForm = AKWidgetSARPlotSettings(self.iface, self.m_form)    
        self.m_form.customResultsGroup.layout().addWidget(self.stiffSettingsForm)
        
        mod = self.plotSettingsModel 
        polygon_color_serie_id =  mod.columnFromField(SARSETTINGS.sar, SARSETTINGS.polygon_color_serie_id)         
        self.mapper.addMapping(self.stiffSettingsForm.polygon_color_serie, polygon_color_serie_id) ;

        self.mapper.toFirst()
                         
        currentText = mod.data(mod.index(0,polygon_color_serie_id))
        comboW = self.stiffSettingsForm.polygon_color_serie
        comboW.setCurrentIndex(comboW.findText(currentText))
        
        # disable unused things
        self.plotSettingsForm.lineGroup.hide()

    def initialize(self):
        self.showData(hideMeasurements=True)
        super(AKFeatureResultsSAR, self).initialize()
        
    def onActionClicked(self):        
        model = self.sampleMeasurementsModel
        myChart = SARPlot(self.iface.mainWindow())
        labels = []
        SAR = []
        CE = []
        numOfSamples = model.rowCount()
        for row in range(numOfSamples):
            isValid = model.data(model.index(row, model.COLUMNS["valid"]))
            if(isValid):
                label = model.data(model.index(row, model.COLUMNS["Sample"]))
                labels.append(label)
                columnId = self.paramInfo["N216USC"]["id"]
                valueCE = model.data(model.index(row, model.COLUMNS[columnId]))
                CE.append(valueCE)
                valueSAR = model.data(model.index(row, model.COLUMNS["SAR"]))
                SAR.append(valueSAR)       
        values = {'SAR':SAR, 'CE':CE}  
        myChart.setData(labels, values)
        myChart.set_settings(self.plotSettingsModel.getDictData())
        myChart.draw()

    def postProcessModelData(self, paramInfo, parameterOrder):
        super(AKFeatureResultsSAR, self).postProcessModelData(paramInfo, parameterOrder)

        model = self.sampleMeasurementsModel
        model.addSimpleColumn("SAR")
        self.addValidColumn(paramInfo, parameterOrder)

        numOfSamples = model.rowCount()            
        for row in range(numOfSamples):
            isValid = model.data(model.index(row, model.COLUMNS["valid"]))
            if(isValid):
                columnId = paramInfo["N436MQL"]["id"]
                Mg = model.data(model.index(row, model.COLUMNS[columnId]))
                columnId = paramInfo["N580MQL"]["id"]
                Na = model.data(model.index(row, model.COLUMNS[columnId]))
                columnId = paramInfo["N209MQL"]["id"]
                Ca = model.data(model.index(row, model.COLUMNS[columnId]))
                valueSAR = Na/(math.sqrt((0.5*(Ca+Mg))))
                model.setData(model.index(row, model.COLUMNS["SAR"]), valueSAR)