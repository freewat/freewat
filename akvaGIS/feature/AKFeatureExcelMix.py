#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.QtGui import QIcon

from AKFeatureCustomChartChem import AKFeatureCustomChartChem
from results.AKFeatureResultsExcelMix import AKFeatureResultsExcelMix

class AKFeatureExcelMix(AKFeatureCustomChartChem):
    '''
    Can be used to export the geochemical measurements for the MIX program.
    The user needs to decide which of the samples are end members before running the export.
    '''
    def __init__(self, settings, iface, parent=None):
        super(AKFeatureExcelMix, self).__init__(settings, iface, windowTitle = 'ExcelMix Export Measurements', parent=parent)
        self.m_icon = QIcon(settings.getIconPath('mix.png'))
        self.m_text = u'ExcelMix Export'
        self.m_measurementsSubFeature.m_parametersAreImposed = False
        self.m_plotSubFeature = AKFeatureResultsExcelMix(self.m_settings, self.iface,  parent=self.parent())
