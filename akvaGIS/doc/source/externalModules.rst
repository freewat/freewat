external packages
=================

Description
-----------
The following third party libraries are provided with the plugin distribution:

- ChemPlotLib 1.0: 
   A GPL licensed library that draws the chemical plots provided in the plugin: Stiff diagram, Piper diagrams and SAR plots but also standard plots as 1D line plots. It relies on Matplotlib 1.5 and. ChemPlotLib offers extensive customization of plots, title labels, axis, edges sizes and colours can be chosen by users.
- Openpyxl2.3 
   (https://openpyxl.readthedocs.io): A MIT licensed library for reading and writing Excel 2010 xlsx/xlsm/xltx/xltm files.AkvaGIS uses it to export data to MSExcel spreadsheets format.  
- Odfpy 1.3 
   (https://pypi.python.org/pypi/odfpy): A library to read and write OpenDocument v. 1.2 files. AkvaGIS uses it to export data to ODF spreadsheets format. 
- Pyexcel 0.2 
   (https://pyexcel.readthedocs.io): A BSD licensed Python Wrapper that provides one API for reading, manipulating and writing data in csv, ods, xls, xlsx and xlsm files. AkvaGIS uses it to export data to different spreadsheets formats.   

external module
---------------

.. toctree::
   :maxdepth: 4

   external2