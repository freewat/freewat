#
#coding=utf-8
#Copyright (C) 2015 Barcelona Science
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 Barcelona Science
:authors: L.M. de Vries, A. Nardi
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''

from collections import OrderedDict
from matplotlib.patches import Polygon
from PyQt4 import QtGui
import sys
from matplotlib.lines import Line2D
from StiffPolygon import stiffPolygonLocalCoords, \
    verticalLineCoords, horizontalLinesCoords, labelsInfo, plotRange

from ChemPlot import ChemPlot
from ChemPlot import CPSETTINGS

class STIFFSETTINGS():    
    stiff = "stiff"    
    polygon_color_id = "polygon_color"    
    showLabels_id = "showLabels"
    showVerticalLine_id= "showVerticalLine"
    showHorizontalLines_id = "showHorizontalLines"
    showHorizontalAxe_id = "showHorizontalAxe"
    fontSize_id = "fontSize"
    xRange_id = "xRange"
    yLength_id = "yLength"    
    
    default_settings = OrderedDict({
                    stiff:{
                        polygon_color_id:{
                            CPSETTINGS.value_key:"green",
                            CPSETTINGS.label_key:"Polygon color",
                            CPSETTINGS.type_value_key:"str"
                        },
                        showLabels_id:{
                            CPSETTINGS.value_key: True,
                            CPSETTINGS.label_key:"Show labels",
                            CPSETTINGS.type_value_key:"bool"
                        },
                        showVerticalLine_id:{
                            CPSETTINGS.value_key:True,
                            CPSETTINGS.label_key:"showVerticalLine_id",
                            CPSETTINGS.type_value_key:"bool"
                        },
                        showHorizontalLines_id:{
                            CPSETTINGS.value_key: True,
                            CPSETTINGS.label_key:"showHorizontalLines_id",
                            CPSETTINGS.type_value_key:"bool"
                        },
                        showHorizontalAxe_id:{
                            CPSETTINGS.value_key: True,
                            CPSETTINGS.label_key:"showHorizontalAxe_id",
                            CPSETTINGS.type_value_key:"bool"
                        },  
                        fontSize_id:{
                            CPSETTINGS.value_key: 15,
                            CPSETTINGS.label_key:"Font size",
                            CPSETTINGS.type_value_key:"float"
                        },
                        xRange_id:{
                            CPSETTINGS.value_key: 10.0,
                            CPSETTINGS.label_key:"xRange_id",
                            CPSETTINGS.type_value_key:"float"
                        },
                        yLength_id:{
                            CPSETTINGS.value_key: 10.0,
                            CPSETTINGS.label_key:"yLength_id",
                            CPSETTINGS.type_value_key:"float"
                        }
                    }
                })

class StiffPlot(ChemPlot):
    '''
    This class implements the Stiff Plot.
    '''

    def __init__(self, parent=None):
        super(StiffPlot, self).__init__(parent)
        self.set_setting(CPSETTINGS.title, CPSETTINGS.title_label_id, 'Stiff Plot')        
        self.set_setting(CPSETTINGS.window, CPSETTINGS.window_title_id, 'Stiff')
        self.set_setting(CPSETTINGS.legend, CPSETTINGS.legend_show_id, False)
        self.m_settings.update(STIFFSETTINGS.default_settings)

    def setData(self, labels, values):        
        self.labels = labels  
        self.values = values        
        self.set_setting(CPSETTINGS.title, CPSETTINGS.title_label_id, labels)  
        self.legendPosition = 'tightToXAxis'        
        
    def pre_processing(self):
        yLength = 10
        coords = stiffPolygonLocalCoords(self.values, yLength= yLength) 
        polygon_color = self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.polygon_color_id)        
        polygon = Polygon(coords, True, color= polygon_color)        
        self.ax.add_patch(polygon)
    
    def post_processing(self):
        fontsize = self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.fontSize_id)
        xRange= self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.xRange_id)
        yLength= self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.yLength_id)
        
        pRange = plotRange(xRange, yLength)
        self.ax.axis(pRange)
        
        showVerticalLine = self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.showVerticalLine_id)
        showHorizontalLines = self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.showHorizontalLines_id)
        showLabels = self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.showLabels_id)
        showHorizontalAxe = self.setting(STIFFSETTINGS.stiff, STIFFSETTINGS.showHorizontalAxe_id)
        
        if showVerticalLine:            
            coords = verticalLineCoords(xRange, yLength)
            print(coords)
            self.ax.add_artist(Line2D(coords[0], coords[1], color='black', linewidth=2))
            
        if showHorizontalLines:            
            linesCoords = horizontalLinesCoords(xRange, yLength)

            for position in linesCoords:
                self.ax.add_artist(Line2D(linesCoords[position][0], linesCoords[position][1], color='gray', linewidth=0.5))

        if showLabels:
            info = labelsInfo(xRange, yLength)
            for specie in info:
                horizontalalignment = info[specie]["posHorizontal"]
                verticalalignment = info[specie]["posVertical"]                
                x = info[specie]["x"]
                y = info[specie]["y"] 
                labelToAdd =  info[specie]["label"]
                self.ax.text(x, y, labelToAdd, fontsize=fontsize, horizontalalignment=horizontalalignment, verticalalignment=verticalalignment)         
        
        if showHorizontalAxe:
            xMin, xMax, yMin = pRange[0], pRange[1], pRange[2]
            self.ax.add_artist(Line2D((xMin, xMax), (yMin, yMin), color='black', linewidth=2))
            self.ax.text(0, yMin+yMin*0.1, "meq", fontsize=fontsize, horizontalalignment="center", verticalalignment="top")        
        
        self.ax.set_frame_on(False)
        self.ax.get_xaxis().tick_bottom()
        self.ax.axes.get_yaxis().set_visible(False)
 
def plotStiff1():
    plot = StiffPlot()
    labels = "Well_1"    
    Ca = 3.5
    Mg = 1.5
    Na = 9.5
    Cl = 1.5    
    SO4 = 5.5
    HCO3 = 3.5
    values = {'Ca':Ca, 'Mg':Mg, 'Na':Na, 'Cl':Cl, 'SO4':SO4, 'HCO3':HCO3}  
    plot.setData(labels, values)
    plot.draw()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    plotStiff1()
    sys.exit(app.exec_())        
        