jdcal.py:
https://github.com/phn/jdcal
V1.0
commit: 81298bc9c10c4a39823a5585b9c90e229e1f657b 

texttable.py:
http://foutaise.org/code/texttable/texttable-0.8.3.tar.gz

openpyxl:
V2.2.6
changeset: f7d02b93ed86d345876a3bd2488207b16d774d2b 

pyexcel:
commit: 4da4828309ca0c91f392bc528a6e9c7c75a3bb12 

pyexcel_io: 
commit: 8408f79ddf645b752da669f2eeaed1584146fad3 

pyexcel_xlsx:
commit: 537a5539a69190f56aec1228cc4208e5c0f4a68d 

pyexcel_ods:
commit: 68736d1ecc3cd477eceab61724d0ee017bd2c7f7 

odfpy:
commit: d6d37d0f817f8b2dc41a02dc13d2c15ac3336e0b 
+ custom changes
