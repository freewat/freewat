# -*- coding: utf-8 -*-
# ===============================================================================
#
#
# Copyright (c) 2015 IST-SUPSI (www.supsi.ch/ist)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
# ===============================================================================
from PyQt4.QtGui import QMessageBox

import version
import sys
import os
import imp

winPlatform = True
if 'linux' in sys.platform:
    import installerLinux as inst
    winPlatform = False
elif 'darwin' in sys.platform:
    import installerLinux as inst
    winPlatform = False
else:
    import installerWindows as inst

package = {
    'pip': False,
    'pandas': False,
    'numpy': False,
    'requests': False,
    'isodate': False,
    'flopy': False,
    'xlrd': False,
    'xlwt': False,
    'seaborn': False,
}


def check_install():
    if os.path.isfile(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'install.txt')):
        print "Installation done!!!"

    else:
        # check if which packages are installed, and update the dictionary accordingly
        try:
            imp.find_module('pip')
            imp.find_module('setuptools')
            package['pip'] = True
        except ImportError:
            pass

        try:
            import numpy
            ver = numpy.__version__

            if ver >= version.numpy:
                package['numpy'] = True
            else:
                print "installed: {} required: {}".format(ver, version.numpy)
        except ImportError:
            pass

        try:
            import pandas
            ver = pandas.__version__

            print ver
            if ver >= version.pandas:
                package['pandas'] = True
            else:
                print "installed: {} required: {}".format(ver, version.pandas)
        except ImportError:
            pass

        try:
            imp.find_module('requests')
            package['requests'] = True
        except ImportError:
            pass

        try:
            imp.find_module('isodate')
            package['isodate'] = True
        except ImportError:
            pass

        try:
            import flopy
            ver = flopy.__version__

            if ver == version.flopy:
                package['flopy'] = True
            else:
                print "installed: {} required: {}".format(ver, version.flopy)

        except ImportError:
            pass

        try:
            imp.find_module('xlrd')
            package['xlrd'] = True
        except ImportError:
            pass

        try:
            import xlwt  # imp.find_module() doesn't work for this module (on windows...) :(
            package['xlwt'] = True
        except ImportError:
            pass

        try:
            imp.find_module('seaborn')
            package['seaborn'] = True
        except ImportError:
            pass

        # Check if all packages are ok (in case you are only updating the plugin, no new installation is needed)
        needInstall = False

        for dep, flag in package.items():
            if flag == False:
                needInstall = True

        if needInstall:
            QMessageBox.warning(None, "Installation process",
                   "If you are installing FREEWAT from Plugin Manager, please follow the installation process for missing libraries.\nThen, close QGIS and open it again.\nFREEWAT will be ready to use, after activating it from Plug Manager !")

            # Scan which package needs to be installed
            if package['pip'] == False: inst.install_pip()
            if package['numpy'] == False : inst.install_numpy()
            # --- all the others
            inst.install_missing_dep(package)

        else:
            # Hey man, everything is ok, you need only to write the sentinel file!!
            # -*- coding: utf-8 -*-
            if winPlatform:             #IAC: this works around two different calls to this method, depending on the system in use
                inst.write_file()
            else:
                inst.write_file(package)   #GDF: the string package in brackets didn't exist in previous versions. This caused an installation error to a Linux User
