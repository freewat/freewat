# coding = utf-8

"""
This is the logging function for freewat.

It replaces calls to QMessageBox or other GUI mechanisme to allow for automated testing.
"""


class Logger(object):
    TESTING = False
    @staticmethod
    def warning(parent, title, message):
        if Logger.TESTING:
            raise RuntimeError(
                    "trying to open warning message box with:\n  title: {}\n  message: {}".format(
                        title, message))
        else:
            from PyQt4.QtGui import QMessageBox
            return QMessageBox.warning(parent, title, message)

    @staticmethod
    def information(parent, title, message):
        if Logger.TESTING:
            print("trying to open information message box with:\n  title: {}\n  message: {}".format(
                        title, message))
        else:
            from PyQt4.QtGui import QMessageBox
            return QMessageBox.information(parent, title, message)
            
    @staticmethod
    def critical(parent, title, message):
        if Logger.TESTING:
            raise RuntimeError(
                    "trying to open critical message box with:\n  title: {}\n  message: {}".format(
                        title, message))
        else:
            from PyQt4.QtGui import QMessageBox
            return QMessageBox.critical(parent, title, message)

